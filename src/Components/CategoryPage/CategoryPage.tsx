import React, { useEffect, useContext, useState } from 'react';
import { useLocation } from 'react-router';
import SideBar from '../Common/Sidebar';
import CategoryData from '../Common/CategoryData';
import FoodCard from '../Common/FoodCard';
import { stateContext, itemContext } from '../../GlobalState/Context';
import { Dropdown } from 'react-bootstrap';
import $ from 'jquery';
import BreadCrumb from '../Common/BreadCrumb';
import { Range } from 'rc-slider';
import 'rc-slider/assets/index.css';
import { Helmet } from 'react-helmet';


const CategoryPage: React.FC = () => {
    const { state, dispatch } = useContext(stateContext);
    const {storeItem, setStoreItem} = useContext(itemContext);
    ///const [data, setData] = useState(CategoryData);
    const [items, setItems] = useState(CategoryData);
    const [dropdown, setDropdown] = useState('');
    //const [ clear, setClear ] = useState(false);
    
    //const createSliderWithTooltip = Slider.createSliderWithTooltip;
    //const Range = createSliderWithTooltip(Slider.Range);
    let total = 0;
    const location = useLocation();

    useEffect(() => {
        if (state.counter === 0) {
            $('#wrapper').removeClass('toggled');
        } else {
            $('#wrapper').addClass('toggled');
        }
    }, [state.counter]);

    const sortLowtoHigh = () => {   ///Low to High
        let lowtohigh = storeItem
            .sort((item1:any, item2:any) => {
                return item1.price - item2.price;
            })
            .filter((item:any) => {
                return item;
            })

       // setData(lowtohigh);
        setStoreItem(lowtohigh);
        setDropdown('Price low to high');
    };

    const sortHightoLow = () => {  ///HIGH to Low
        let high = storeItem
            .sort((item1:any, item2:any) => {
                return item2.price - item1.price;
            })
            .filter((item:any) => {
                return item;
            })
        //setData(high);
        setStoreItem(high)
        setDropdown('Price high to low');
    };

    const getProducts = storeItem
        .filter((item:any) => {
            if (item.category === state.category) {
                total = total + 1;
                return item;
            }
        })
        .map((item:any) => {
            return <FoodCard key={item.id} val={item} />
        })

    const handleClear = () => {  ///Best Match
        let clear = items
            .sort(() => Math.random() - 0.5)
            .filter((item) => {
                return item;
            })
        //setData(clear);
        setStoreItem(clear);
        setDropdown('');
    };

    return (
        <React.Fragment>
            <Helmet>
                <title>
                    {location.pathname.split('/')[1]} Archives - Farmer&#039;s
                    Fresh Meat: Houston Meat Market &amp; Butcher Shop
                </title>
            </Helmet>

            <div className="container-fluid category">
                <div className="row">
                    <BreadCrumb currentPage={'Category'} category={true} />
                    <SideBar />

                    <div className="col-12 col-md-10 col-lg-10 col-xl-9 column">
                        <div className="col-12 info-bar">
                            <div className="row">
                                <div className="col-md-6 left">
                                    <small>Showing {total} results</small>
                                </div>

                                <div className="col right">
                                    <span className="float-left">Sort by</span>
                                    <Dropdown>
                                        <Dropdown.Toggle
                                            variant=""
                                            id="dropdown-basic"
                                        >
                                            {dropdown
                                                ? dropdown
                                                : 'Best Match'}
                                        </Dropdown.Toggle>

                                        <Dropdown.Menu>
                                            <Dropdown.Item
                                                onClick={() => handleClear()}
                                            >
                                                Best Match
                                            </Dropdown.Item>
                                            <Dropdown.Item
                                                onClick={() => sortLowtoHigh()}
                                            >
                                                Price low to high
                                            </Dropdown.Item>
                                            <Dropdown.Item
                                                onClick={() => sortHightoLow()}
                                            >
                                                Price high to low
                                            </Dropdown.Item>
                                        </Dropdown.Menu>
                                    </Dropdown>
                                    {/* <small className="float-right" onClick={() => handleClear()}> ClearAll </small> */}
                                </div>
                            </div>
                        </div>
                        <div className="row display-details">{getProducts}</div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
};


export default CategoryPage;
