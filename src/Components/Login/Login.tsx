import React, { useState, useEffect } from 'react';
import GoogleLogin from 'react-google-login';
import FacebookLogin from 'react-facebook-login';
import { NavLink, useHistory } from 'react-router-dom';
import Api from '../../API/api';
import Auth from '../../Auth/index';
import { images } from '../Constant/Constant';
import $ from 'jquery';
import { Helmet } from 'react-helmet';
import Notifications, { notify } from 'react-notify-toast';

const auth = new Auth();
const api = new Api();

const Login: React.FC = () => {
    useEffect(() => {
        $('#wrapper').removeClass('toggled');
        $('.container1').removeClass('open');
    }, []);

    const clientID =
        '282384162289-gp4ve43bm00n3hgu09keva40i6n44au6.apps.googleusercontent.com';

    const history = useHistory();

    const [data, setData] = useState({
        email: '',
        password: '',
    });

    const [ error ,setError ] = useState('');
    const [ loading , setLoading ] = useState(false);

    const InputEvent = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;

        setData((prevVal) => {
            return {
                ...prevVal,
                [name]: value,
            }
        });
    };

    const handleLogin = (e: React.FormEvent) => {
        e.preventDefault();
        setLoading(true);        
        if(data.email===''){
            setError('Enter Email');
            notify.show('Enter Email','error');
        }

        if(data.password===''){
            setError('Enter Password');
            notify.show('Enter Password','error');
        }

        if(error === ''){
           api.login(data).then((res)=>{
              setLoading(false);
              auth.setValue('token', res.data.response.token);
              history.push('/PersonalInformation');
           }).catch((error) => {
             setLoading(false);
             console.log(error.response.data.message); 
           });
        }
            
    };

    const responseGoogleSuccess = (res: any) => {
        console.log(res.profileObj);
    };

    const responseGoogleFailure = (res: any) => {
        console.log(res);
    };

    const componentClicked = (val: any) => {
        console.log('Component Clicked', val);
    };

    const responseFacebook = (val: any) => {
        console.log('Response', val);
    };

    return (
        <React.Fragment>
            <Notifications/>
            <Helmet>
                <title>
                    My Account - Farmer&#039;s Fresh Meat: Houston Meat Market
                    &amp; Butcher Shop
                </title>
            </Helmet>
            <header className="header1"> 
                <div className="bg-white header">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                                <NavLink exact to="/">
                                    <div className="logo-col mx-auto">
                                        <img
                                            src={images.logoImg.default}
                                            className="img-fluid" 
                                            alt="logo"
                                        />
                                    </div>
                                </NavLink> 
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <section className="main-height">
                <div className="container">
                    <div className="row">
                        <div className="col-12 col-md-4 col-lg-4 col-xl-5 login-column mx-auto">
                            <div className="signin-info text-center">
                                <h2>Sign In</h2>
                                <p> Sign in to your existing account</p>
                            </div>
                            <div className="bg-white main-section p-4">
                                <form>
                                    <div className="form-group">
                                        <label>Email address</label>
                                        <input
                                            type="email"
                                            className="form-control info-control"
                                            id="email"
                                            placeholder="Enter here .."
                                            name="email"
                                            value={data.email}
                                            onChange={InputEvent}
                                        />
                                    </div>

                                    <div className="form-group">
                                        <label>Password</label>
                                        <input
                                            type="password"
                                            className="form-control info-control"
                                            id="password"
                                            placeholder="min. 8 characters"
                                            name="password"
                                            value={data.password}
                                            onChange={InputEvent}
                                        />
                                    </div>

                                    <div className="forget-details">
                                        <a href="#">Forgot your password ?</a>
                                    </div>

                                    <div className="custom-check login-check">
                                        <label className="check ">
                                            <input
                                                type="checkbox"
                                                name="is_name1"
                                            />
                                            <span className="checkmark"></span>
                                            Keep me logged in
                                        </label>
                                    </div>

                                    <div
                                        className="update-btn"
                                        onClick={(e) => {
                                            handleLogin(e)
                                        }}
                                    >
                                        <a className="btn btn-cart w-100">
                                            {loading?'Loading...':'Sign in'}
                                        </a>
                                    </div>

                                    <div className="create-account text-center">
                                        <p>
                                            Don’t have an account?
                                            <NavLink exact to="/signup">
                                                Create account
                                            </NavLink>
                                        </p>
                                    </div>

                                    <div className="row kpx_row-sm-offset-3 kpx_loginOr">
                                        <div className="col-xs-12 col-xl-12">
                                            <hr className="kpx_hrOr" />
                                            <span className="kpx_spanOr">
                                                or
                                            </span>
                                        </div>
                                    </div>

                                    <div className="google-sign text-center">
                                        <h6 className="login-title">
                                            Login with
                                        </h6>
                                        <GoogleLogin
                                            clientId={clientID}
                                            render={(renderProps) => (
                                                <div
                                                    className="google-button d-flex justify-content-center"
                                                    onClick={
                                                        renderProps.onClick
                                                    }
                                                >
                                                    <div className="google-icon">
                                                        <img
                                                            src={
                                                                images
                                                                    .googleIcon
                                                                    .default
                                                            }
                                                            alt="Google"
                                                        />
                                                    </div>

                                                    <div className="google-content">
                                                        <h6>
                                                            Sign in with google
                                                        </h6>
                                                    </div>
                                                </div>
                                            )}
                                            onSuccess={responseGoogleSuccess}
                                            onFailure={responseGoogleFailure}
                                            cookiePolicy={'single_host_origin'}
                                        />

                                        <FacebookLogin
                                            appId="5765000220206748"
                                            autoLoad={false}
                                            fields="name,email,picture"
                                            onClick={componentClicked}
                                            callback={responseFacebook}
                                            cssClass="google-button d-flex justify-content-center google-content"
                                            language={'en_US'}
                                            icon={images.fbIcon.default}
                                            textButton={'Sign in with Facebook'}
                                        />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <hr className="mb-0" />
        </React.Fragment>
    )
}

export default Login
