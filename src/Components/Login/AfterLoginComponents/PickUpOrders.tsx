import React from 'react'
import Sidebar from './Sidebar'
import BreadCrumb from '../../Common/BreadCrumb'

const PickUpOrders: React.FC = () => {
    return (
        <React.Fragment>
            <div className="container">
                <div className="row row-margin">
                    <div className="col-12 col-md-12 col-lg-11 col-xl-11 mx-auto">
                        {/* <!---------breadcrumb------> */}

                        <BreadCrumb currentPage="My account" />

                        {/* <!-------/breadcrumb-------> */}
                        {/* <!---------> */}
                        <div className="row">
                            <Sidebar />
                            <div className="col-12 col-md-8 col-lg-8 col-xl-9">
                                <div className="food-details">
                                    <div>
                                        <h2>Pickup orders</h2>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                                        <div className="bg-white main-section p-0">
                                            <div className="pickup-order">
                                                <h6>
                                                    8630 Cullen Blvd, Houston
                                                </h6>
                                                <p>
                                                    <span>
                                                        Jan 21, 2021 Tuesdays
                                                    </span>{' '}
                                                    <span>|</span>{' '}
                                                    <span>
                                                        12:30pm - 1:00pm
                                                    </span>
                                                </p>
                                                <h6 className="pickup-amt">
                                                    <span>
                                                        Amount - $230.00
                                                    </span>{' '}
                                                    <span>
                                                        Quantity - 40 items{' '}
                                                    </span>
                                                </h6>
                                                <div className="pickup-way food-details">
                                                    <div className="payment-method">
                                                        <button className="btn btn-select">
                                                            Details
                                                        </button>
                                                    </div>
                                                    <div className="pickup-card">
                                                        <h6>On the way</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                                        <div className="bg-white main-section p-0">
                                            <div className="pickup-order">
                                                <h6>
                                                    8630 Cullen Blvd, Houston
                                                </h6>
                                                <p>
                                                    <span>
                                                        Jan 21, 2021 Tuesdays
                                                    </span>{' '}
                                                    <span>|</span>{' '}
                                                    <span>
                                                        12:30pm - 1:00pm
                                                    </span>
                                                </p>
                                                <h6 className="pickup-amt">
                                                    <span>
                                                        Amount - $230.00
                                                    </span>{' '}
                                                    <span>
                                                        Quantity - 40 items{' '}
                                                    </span>
                                                </h6>
                                                <div className="pickup-way food-details">
                                                    <div className="payment-method">
                                                        <button className="btn btn-select">
                                                            Details
                                                        </button>
                                                    </div>
                                                    <div className="pickup-card">
                                                        <h6>On the way</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                                        <div className="bg-white main-section p-0">
                                            <div className="pickup-order">
                                                <h6>
                                                    8630 Cullen Blvd, Houston
                                                </h6>
                                                <p>
                                                    <span>
                                                        Jan 21, 2021 Tuesdays
                                                    </span>{' '}
                                                    <span>|</span>{' '}
                                                    <span>
                                                        12:30pm - 1:00pm
                                                    </span>
                                                </p>
                                                <h6 className="pickup-amt">
                                                    <span>
                                                        Amount - $230.00
                                                    </span>{' '}
                                                    <span>
                                                        Quantity - 40 items{' '}
                                                    </span>
                                                </h6>
                                                <div className="pickup-way food-details">
                                                    <div className="payment-method">
                                                        <button className="btn btn-select">
                                                            Details
                                                        </button>
                                                    </div>
                                                    <div className="pickup-card">
                                                        <h6>On the way</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                                        <div className="bg-white main-section p-0">
                                            <div className="pickup-order">
                                                <h6>
                                                    8630 Cullen Blvd, Houston
                                                </h6>
                                                <p>
                                                    <span>
                                                        Jan 21, 2021 Tuesdays
                                                    </span>{' '}
                                                    <span>|</span>{' '}
                                                    <span>
                                                        12:30pm - 1:00pm
                                                    </span>
                                                </p>
                                                <h6 className="pickup-amt">
                                                    <span>
                                                        Amount - $230.00
                                                    </span>{' '}
                                                    <span>
                                                        Quantity - 40 items{' '}
                                                    </span>
                                                </h6>
                                                <div className="pickup-way food-details">
                                                    <div className="payment-method">
                                                        <button className="btn btn-select">
                                                            Details
                                                        </button>
                                                    </div>
                                                    <div className="pickup-card">
                                                        <h6>On the way</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                                        <div className="bg-white main-section p-0">
                                            <div className="pickup-order">
                                                <h6>
                                                    8630 Cullen Blvd, Houston
                                                </h6>
                                                <p>
                                                    <span>
                                                        Jan 21, 2021 Tuesdays
                                                    </span>{' '}
                                                    <span>|</span>{' '}
                                                    <span>
                                                        12:30pm - 1:00pm
                                                    </span>
                                                </p>
                                                <h6 className="pickup-amt">
                                                    <span>
                                                        Amount - $230.00
                                                    </span>{' '}
                                                    <span>
                                                        Quantity - 40 items{' '}
                                                    </span>
                                                </h6>
                                                <div className="pickup-way food-details">
                                                    <div className="payment-method">
                                                        <button className="btn btn-select">
                                                            Details
                                                        </button>
                                                    </div>
                                                    <div className="pickup-card">
                                                        <h6>On the way</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                                        <div className="bg-white main-section p-0">
                                            <div className="pickup-order">
                                                <h6>
                                                    8630 Cullen Blvd, Houston
                                                </h6>
                                                <p>
                                                    <span>
                                                        Jan 21, 2021 Tuesdays
                                                    </span>{' '}
                                                    <span>|</span>{' '}
                                                    <span>
                                                        12:30pm - 1:00pm
                                                    </span>
                                                </p>
                                                <h6 className="pickup-amt">
                                                    <span>
                                                        Amount - $230.00
                                                    </span>{' '}
                                                    <span>
                                                        Quantity - 40 items{' '}
                                                    </span>
                                                </h6>
                                                <div className="pickup-way food-details">
                                                    <div className="payment-method">
                                                        <button className="btn btn-select">
                                                            Details
                                                        </button>
                                                    </div>
                                                    <div className="pickup-card">
                                                        <h6>On the way</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                                        <div className="bg-white main-section p-0">
                                            <div className="pickup-order">
                                                <h6>
                                                    8630 Cullen Blvd, Houston
                                                </h6>
                                                <p>
                                                    <span>
                                                        Jan 21, 2021 Tuesdays
                                                    </span>{' '}
                                                    <span>|</span>{' '}
                                                    <span>
                                                        12:30pm - 1:00pm
                                                    </span>
                                                </p>
                                                <h6 className="pickup-amt">
                                                    <span>
                                                        Amount - $230.00
                                                    </span>{' '}
                                                    <span>
                                                        Quantity - 40 items{' '}
                                                    </span>
                                                </h6>
                                                <div className="pickup-way food-details">
                                                    <div className="payment-method">
                                                        <button className="btn btn-select">
                                                            Details
                                                        </button>
                                                    </div>
                                                    <div className="pickup-card">
                                                        <h6>On the way</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                                        <div className="bg-white main-section p-0">
                                            <div className="pickup-order">
                                                <h6>
                                                    8630 Cullen Blvd, Houston
                                                </h6>
                                                <p>
                                                    <span>
                                                        Jan 21, 2021 Tuesdays
                                                    </span>{' '}
                                                    <span>|</span>{' '}
                                                    <span>
                                                        12:30pm - 1:00pm
                                                    </span>
                                                </p>
                                                <h6 className="pickup-amt">
                                                    <span>
                                                        Amount - $230.00
                                                    </span>{' '}
                                                    <span>
                                                        Quantity - 40 items{' '}
                                                    </span>
                                                </h6>
                                                <div className="pickup-way food-details">
                                                    <div className="payment-method">
                                                        <button className="btn btn-select">
                                                            Details
                                                        </button>
                                                    </div>
                                                    <div className="pickup-card">
                                                        <h6>On the way</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                                        <div className="bg-white main-section p-0">
                                            <div className="pickup-order">
                                                <h6>
                                                    8630 Cullen Blvd, Houston
                                                </h6>
                                                <p>
                                                    <span>
                                                        Jan 21, 2021 Tuesdays
                                                    </span>{' '}
                                                    <span>|</span>{' '}
                                                    <span>
                                                        12:30pm - 1:00pm
                                                    </span>
                                                </p>
                                                <h6 className="pickup-amt">
                                                    <span>
                                                        Amount - $230.00
                                                    </span>{' '}
                                                    <span>
                                                        Quantity - 40 items{' '}
                                                    </span>
                                                </h6>
                                                <div className="pickup-way food-details">
                                                    <div className="payment-method">
                                                        <button className="btn btn-select">
                                                            Details
                                                        </button>
                                                    </div>
                                                    <div className="pickup-card">
                                                        <h6>On the way</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                                        <div className="bg-white main-section p-0">
                                            <div className="pickup-order">
                                                <h6>
                                                    8630 Cullen Blvd, Houston
                                                </h6>
                                                <p>
                                                    <span>
                                                        Jan 21, 2021 Tuesdays
                                                    </span>{' '}
                                                    <span>|</span>{' '}
                                                    <span>
                                                        12:30pm - 1:00pm
                                                    </span>
                                                </p>
                                                <h6 className="pickup-amt">
                                                    <span>
                                                        Amount - $230.00
                                                    </span>{' '}
                                                    <span>
                                                        Quantity - 40 items{' '}
                                                    </span>
                                                </h6>
                                                <div className="pickup-way food-details">
                                                    <div className="payment-method">
                                                        <button className="btn btn-select">
                                                            Details
                                                        </button>
                                                    </div>
                                                    <div className="pickup-card">
                                                        <h6>On the way</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-12 col-md-6 col-lg-6 col-xl-5">
                                        <div className="filter-design bg-white">
                                            <nav aria-label="Page navigation example">
                                                <ul className="pagination justify-content-between mb-0">
                                                    <li className="page-item page-item-navigation mr-2">
                                                        <span className="fa fa-angle-left"></span>
                                                    </li>
                                                    <li className="page-item ">
                                                        <a
                                                            className="page-link"
                                                            href="#"
                                                        >
                                                            1
                                                        </a>
                                                    </li>
                                                    <li className="page-item active">
                                                        <a
                                                            className="page-link"
                                                            href="#"
                                                        >
                                                            2
                                                        </a>
                                                    </li>
                                                    <li className="page-item">
                                                        <a
                                                            className="page-link"
                                                            href="#"
                                                        >
                                                            3
                                                        </a>
                                                    </li>
                                                    <li className="page-item">
                                                        <a
                                                            className="page-link"
                                                            href="#"
                                                        >
                                                            4
                                                        </a>
                                                    </li>
                                                    <li className="page-item">
                                                        <a
                                                            className="page-link"
                                                            href="#"
                                                        >
                                                            5
                                                        </a>
                                                    </li>
                                                    <li className="page-item">
                                                        <a
                                                            className="page-link"
                                                            href="#"
                                                        >
                                                            6
                                                        </a>
                                                    </li>

                                                    <li className="page-item page-item-navigation ml-2">
                                                        <span className="fa fa-angle-right"></span>
                                                    </li>
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {/* <!---------> */}
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default PickUpOrders
