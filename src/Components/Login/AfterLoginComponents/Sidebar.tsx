import React from 'react'
import { NavLink } from 'react-router-dom'

const Sidebar: React.FC = () => {
    return (
        <React.Fragment>
            <div className="col-12 col-md-4 col-lg-4 col-xl-3">
                <div className="bg-white main-section">
                    <div className="account-details">
                        <h6 className="account-title">My account</h6>
                        <ul className="nav flex-column">
                            <li className="nav-item">
                                <NavLink
                                    exact
                                    to="/PersonalInformation"
                                    className="nav-link"
                                >
                                    Personal information
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink
                                    className="nav-link"
                                    exact
                                    to="/CommunicationPrefrences"
                                >
                                    Communication preferences
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink
                                    className="nav-link"
                                    exact
                                    to="/SecuritySetting"
                                >
                                    Security settings
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink
                                    className="nav-link"
                                    exact
                                    to="/AddressDetails"
                                >
                                    Address details
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink
                                    className="nav-link"
                                    exact
                                    to="/PaymentDetails"
                                >
                                    Payment details
                                </NavLink>
                            </li>
                        </ul>
                    </div>
                    <div className="account-details">
                        <h6>Orders Details</h6>
                        <ul className="nav flex-column">
                            <li className="nav-item">
                                <NavLink
                                    className="nav-link"
                                    exact
                                    to="/PickUpOrder"
                                >
                                    Pickup orders
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink
                                    className="nav-link"
                                    to="/DeliveryOrders"
                                >
                                    Delivery orders
                                </NavLink>
                            </li>
                        </ul>
                    </div>
                    <div className="account-details">
                        <h6 className="account-title">Manage</h6>
                        <ul className="nav flex-column">
                            <li className="nav-item">
                                <NavLink
                                    exact
                                    className="nav-link"
                                    to="/DigitalCoupons"
                                >
                                    Digital coupons
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink
                                    exact
                                    className="nav-link"
                                    to="/ItemList"
                                >
                                    Item list
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink
                                    exact
                                    className="nav-link"
                                    to="/TaxExemption"
                                >
                                    Tax exemption
                                </NavLink>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default Sidebar
