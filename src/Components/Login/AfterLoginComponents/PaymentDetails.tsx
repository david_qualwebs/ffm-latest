import React from 'react'
import Sidebar from './Sidebar'
import BreadCrumb from '../../Common/BreadCrumb'
import { images } from '../../Constant/Constant'

const PaymentDetails: React.FC = () => {
    return (
        <React.Fragment>
            <div className="container">
                <div className="row row-margin">
                    <div className="col-12 col-md-12 col-lg-11 col-xl-11 mx-auto">
                        {/* <!---------breadcrumb------> */}
                        <BreadCrumb currentPage="My account" />
                        {/* <!-------/breadcrumb-------> */}
                        {/* <!---------> */}
                        <div className="row">
                            <Sidebar />

                            <div className="col-12 col-md-8 col-lg-8 col-xl-9">
                                <div className="food-details">
                                    <div>
                                        <h2>Saved payment cards</h2>
                                    </div>
                                </div>
                                <div className="row">
                                    {/* <!----------card one-----------> */}
                                    <div className="col-12 col-md-12 col-lg-6 col-xl-6">
                                        <div className="bg-white main-section p-0">
                                            <div className="payment-method payment pay-card">
                                                <input
                                                    type="radio"
                                                    id="myradio1"
                                                    name="radio1"
                                                    checked={true}
                                                    value="fashion"
                                                />
                                                <label htmlFor="myradio1">
                                                    <div className=" visa-card text-right">
                                                        <div>
                                                            <img
                                                                src={
                                                                    images
                                                                        .VisaIcon
                                                                        .default
                                                                }
                                                                alt="Visa"
                                                            />
                                                        </div>
                                                    </div>

                                                    <div className="card-number ch-card-no">
                                                        <span>****</span>
                                                        <span>****</span>
                                                        <span>****</span>
                                                        <span>8014</span>
                                                    </div>

                                                    <div className="food-details chekcout-card">
                                                        <div className="card-holder">
                                                            <p className="crd-title ch-title">
                                                                Cardholder’s
                                                                name
                                                            </p>
                                                            <h6 className="crd-holder-name ch-holder">
                                                                Rashid Hussain
                                                            </h6>
                                                        </div>
                                                        <div className="card-holder chekcout-card">
                                                            <p className="crd-title ch-title">
                                                                Expiry
                                                            </p>
                                                            <h6 className="crd-holder-name ch-holder">
                                                                22-2022
                                                            </h6>
                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-12 col-xl-7">
                                                            <div className="food-details edit-info">
                                                                <div className="edit-content">
                                                                    <a href="#">
                                                                        Edit
                                                                    </a>
                                                                </div>
                                                                <div>
                                                                    <button className="btn btn-select">
                                                                        Select
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    {/* <!----------/card one-----------> */}

                                    {/* <!----------card two-----------> */}
                                    <div className="col-12 col-md-12 col-lg-6 col-xl-6">
                                        <div className="bg-white main-section p-0">
                                            <div className="payment-method payment pay-card">
                                                <input
                                                    type="radio"
                                                    id="myradio1"
                                                    name="radio1"
                                                    checked={true}
                                                    value="fashion"
                                                />
                                                <label htmlFor="myradio1">
                                                    <div className=" visa-card text-right">
                                                        <div>
                                                            <img
                                                                src={
                                                                    images
                                                                        .VisaIcon
                                                                        .default
                                                                }
                                                                alt="Visa"
                                                            />
                                                        </div>
                                                    </div>

                                                    <div className="card-number ch-card-no">
                                                        <span>****</span>
                                                        <span>****</span>
                                                        <span>****</span>
                                                        <span>8014</span>
                                                    </div>

                                                    <div className="food-details chekcout-card">
                                                        <div className="card-holder">
                                                            <p className="crd-title ch-title">
                                                                Cardholder’s
                                                                name
                                                            </p>
                                                            <h6 className="crd-holder-name ch-holder">
                                                                Rashid Hussain
                                                            </h6>
                                                        </div>
                                                        <div className="card-holder chekcout-card">
                                                            <p className="crd-title ch-title">
                                                                Expiry
                                                            </p>
                                                            <h6 className="crd-holder-name ch-holder">
                                                                22-2022
                                                            </h6>
                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-12 col-xl-7">
                                                            <div className="food-details edit-info">
                                                                <div className="edit-content">
                                                                    <a href="#">
                                                                        Edit
                                                                    </a>
                                                                </div>
                                                                <div>
                                                                    <button className="btn btn-select">
                                                                        Select
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    {/* <!----------/card two-----------> */}
                                </div>
                                <div className="address-btn">
                                    <button className="btn btn-address">
                                        + Add new card
                                    </button>
                                </div>
                            </div>
                        </div>

                        {/* <!---------> */}
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default PaymentDetails
