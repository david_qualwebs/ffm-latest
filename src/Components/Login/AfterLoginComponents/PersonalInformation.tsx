import React from 'react'
import BreadCrumb from '../../Common/BreadCrumb'
import Sidebar from './Sidebar'

const PersonalInformation: React.FC = () => {
    return (
        <React.Fragment>
            <div className="container">
                <div className="row row-margin">
                    <div className="col-12 col-md-12 col-lg-11 col-xl-11 mx-auto">
                        {/* <!---------breadcrumb------> */}

                        <BreadCrumb currentPage="My account" />
                        {/* <!-------/breadcrumb-------> */}
                        {/* <!---------> */}
                        <div className="row">
                            {/* <!---------------/tabs------------> */}
                            <Sidebar />
                            {/* <!---------------/tabs------------> */}

                            <div className="col-12 col-md-8 col-lg-8 col-xl-9">
                                <div className="food-details">
                                    <div>
                                        <h2>Personal details</h2>
                                    </div>
                                </div>

                                <div className="bg-white main-section">
                                    <div className="row">
                                        <div className="col-12 col-md-5 col-lg-5 col-xl-5">
                                            <div className="user-personal-details">
                                                <h6>Name</h6>
                                                <p>Alex wassabi</p>
                                            </div>
                                            <div className="user-personal-details">
                                                <h6>Email address</h6>
                                                <p>alex_wassabi@gmail.com</p>
                                            </div>
                                        </div>
                                        <div className="col-12 col-md-5 col-lg-5 col-xl-5">
                                            <div className="user-personal-details">
                                                <h6>Mobile number</h6>
                                                <p>731 569-5623</p>
                                            </div>
                                            <div className="user-personal-details">
                                                <h6>Home contact</h6>
                                                <p>731 569-5623</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-12 col-xl-7 payment-method">
                                            <div className="food-details edit-info mt-0">
                                                <div className="edit-content">
                                                    <a href="#">
                                                        Change password
                                                    </a>
                                                </div>
                                                <div className="edit-button">
                                                    <button className="btn btn-select">
                                                        Edit details
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {/* <!---------> */}
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default PersonalInformation
