import React from 'react'
import Sidebar from './Sidebar'
import BreadCrumb from '../../Common/BreadCrumb'

const AddressDetails: React.FC = () => {
    return (
        <React.Fragment>
            <div className="container">
                <div className="row row-margin">
                    <div className="col-12 col-md-12 col-lg-11 col-xl-11 mx-auto">
                        {/* <!---------breadcrumb------> */}
                        <BreadCrumb currentPage="My account" />
                        {/* <!-------/breadcrumb-------> */}
                        {/* <!---------> */}
                        <div className="row">
                            <Sidebar />
                            <div className="col-12 col-md-8 col-lg-8 col-xl-9">
                                <div className="food-details">
                                    <div>
                                        <h2>Saved address</h2>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-12 col-md-12 col-lg-6 col-xl-6">
                                        <div className="bg-white main-section">
                                            <div className="address-info">
                                                <h6 className="mb-2">
                                                    <span>Alex Wassabi,</span>{' '}
                                                    <span> 713 565 9895</span>
                                                </h6>
                                                <h6 className="address1">
                                                    3500 Cambridge Court
                                                </h6>
                                                <p className="address2">
                                                    Russellville, AR
                                                </p>
                                                <p className="address2">
                                                    72801
                                                </p>
                                            </div>
                                            <div className="row">
                                                <div className="col-12 col-xl-8 payment-method">
                                                    <div className="food-details edit-info mt-0">
                                                        <div className="edit-content">
                                                            <a href="#">
                                                                Delete
                                                            </a>
                                                        </div>
                                                        <div>
                                                            <button className="btn btn-select">
                                                                Edit
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-12 col-md-12 col-lg-6 col-xl-6">
                                        <div className="bg-white main-section">
                                            <div className="address-info">
                                                <h6 className="mb-2">
                                                    <span>Alex Wassabi,</span>{' '}
                                                    <span> 713 565 9895</span>
                                                </h6>
                                                <h6 className="address1">
                                                    3500 Cambridge Court
                                                </h6>
                                                <p className="address2">
                                                    Russellville, AR
                                                </p>
                                                <p className="address2">
                                                    72801
                                                </p>
                                            </div>
                                            <div className="row">
                                                <div className="col-12 col-xl-8 payment-method">
                                                    <div className="food-details edit-info mt-0">
                                                        <div className="edit-content">
                                                            <a href="#">
                                                                Delete
                                                            </a>
                                                        </div>
                                                        <div>
                                                            <button className="btn btn-select">
                                                                Edit
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="address-btn">
                                    <button className="btn btn-address">
                                        + Add new address
                                    </button>
                                </div>
                            </div>
                        </div>

                        {/* <!---------> */}
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default AddressDetails
