import React from 'react'
import Sidebar from './Sidebar'
import BreadCrumb from '../../Common/BreadCrumb'

const CommunicationPreferences: React.FC = () => {
    return (
        <React.Fragment>
            <div className="container">
                <div className="row row-margin">
                    <div className="col-12 col-md-12 col-lg-11 col-xl-11 mx-auto">
                        {/* <!---------breadcrumb------> */}

                        <BreadCrumb currentPage="My account" />
                        {/* <!-------/breadcrumb-------> */}

                        {/* <!---------> */}
                        <div className="row">
                            <Sidebar />
                            <div className="col-12 col-md-8 col-lg-8 col-xl-9">
                                <div className="food-details">
                                    <div>
                                        <h2>Email preferences</h2>
                                    </div>
                                </div>

                                <div className="bg-white main-section">
                                    <div className="billing-details toggle-info">
                                        <ul className="list-group">
                                            <li className="list-details row">
                                                <div className="col-9 col-md-9 col-lg-8 col-xl-9 offer-details">
                                                    <h6>Weekly Deals</h6>
                                                    <p>
                                                        Receive best deals from
                                                        farmer’s fresh kitchen{' '}
                                                    </p>
                                                </div>
                                                <div className="col-3 col-md-3 col-lg-4 col-xl-3 toggle-switch">
                                                    <label className="switch">
                                                        <input
                                                            type="checkbox"
                                                            className="default"
                                                        />
                                                        <span className="slider round"></span>
                                                    </label>
                                                </div>
                                            </li>
                                            <li className="list-details row">
                                                <div className="col-9 col-md-9 col-lg-8 col-xl-9 offer-details">
                                                    <h6>
                                                        Promotions and
                                                        Announcements
                                                    </h6>
                                                    <p>
                                                        Send me exclusive email
                                                        discounts and help me
                                                        discover new products
                                                        and services.
                                                    </p>
                                                </div>
                                                <div className="col-3 col-md-3 col-lg-4 col-xl-3 toggle-switch">
                                                    <label className="switch">
                                                        <input
                                                            type="checkbox"
                                                            className="default"
                                                        />
                                                        <span className="slider round"></span>
                                                    </label>
                                                </div>
                                            </li>
                                            <li className="list-details row">
                                                <div className="col-9 col-md-9 col-lg-8 col-xl-10 offer-details">
                                                    <h6>In the Kitchen</h6>
                                                    <p>
                                                        Dinner time inspiration
                                                        from our chefs,
                                                        featuring our newest
                                                        products and the latest
                                                        food trends.
                                                    </p>
                                                </div>
                                                <div className="col-3 col-md-3 col-lg-4 col-xl-2 toggle-switch">
                                                    <label className="switch">
                                                        <input
                                                            type="checkbox"
                                                            className="default"
                                                        />
                                                        <span className="slider round"></span>
                                                    </label>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>

                                    {/* <!--- <div className="update-btn">
                      <a href="#" className="btn btn-cart">Update details</a>
                    </div>--> */}
                                </div>
                            </div>
                        </div>

                        {/* <!---------> */}
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default CommunicationPreferences
