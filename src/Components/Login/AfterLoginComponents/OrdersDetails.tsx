import React from 'react'
import Road1Img from '../../../assets/images/road-1.png'
import Img4 from '../../../assets/images/img4.png'

const OrdersDetails: React.FC = () => {
    return (
        <React.Fragment>
            {/*<!---------------/main section------------------------------>*/}
            <div className="container">
                <div className="row row-margin">
                    <div className="col-12 col-md-12 col-lg-12 col-xl-11 delivery-page mx-auto">
                        {/*<!--------->*/}
                        <div className="row">
                            <div className="col-12 col-md-4 col-lg-4 col-xl-3 account-card">
                                <div className="card-details">
                                    {/*<!---------------/tabs------------>*/}
                                    <div className="account-details">
                                        <h6 className="account-title">
                                            My account
                                        </h6>
                                        <ul className="nav flex-column">
                                            <li className="nav-item">
                                                <a
                                                    className="nav-link"
                                                    href="http://159.65.142.31/newffm-design/personal-info.html"
                                                >
                                                    Personal information
                                                </a>
                                            </li>
                                            <li className="nav-item">
                                                <a
                                                    className="nav-link"
                                                    href="http://159.65.142.31/newffm-design/communication-prefrences.html"
                                                >
                                                    Communication preferences
                                                </a>
                                            </li>
                                            <li className="nav-item">
                                                <a
                                                    className="nav-link"
                                                    href="http://159.65.142.31/newffm-design/security-setting.html"
                                                >
                                                    Security settings
                                                </a>
                                            </li>
                                            <li className="nav-item">
                                                <a
                                                    className="nav-link"
                                                    href="http://159.65.142.31/newffm-design/address-details.html"
                                                >
                                                    Address details
                                                </a>
                                            </li>
                                            <li className="nav-item">
                                                <a
                                                    className="nav-link"
                                                    href="http://159.65.142.31/newffm-design/payment-details.html"
                                                >
                                                    Payment details
                                                </a>
                                            </li>
                                        </ul>
                                    </div>

                                    <div className="account-details">
                                        <h6 className="account-title">
                                            Orders Details
                                        </h6>
                                        <ul className="nav flex-column">
                                            <li className="nav-item">
                                                <a
                                                    className="nav-link"
                                                    href="#"
                                                >
                                                    Pickup orders
                                                </a>
                                            </li>
                                            <li className="nav-item">
                                                <a
                                                    className="nav-link active"
                                                    href="http://159.65.142.31/newffm-design/delivery-order.html"
                                                >
                                                    Delivery orders
                                                </a>
                                            </li>
                                        </ul>
                                    </div>

                                    <div className="account-details">
                                        <h6 className="account-title">
                                            Manage
                                        </h6>
                                        <ul className="nav flex-column">
                                            <li className="nav-item">
                                                <a
                                                    className="nav-link"
                                                    href="#"
                                                >
                                                    Digital coupons
                                                </a>
                                            </li>
                                            <li className="nav-item">
                                                <a
                                                    className="nav-link"
                                                    href="#"
                                                >
                                                    Item list
                                                </a>
                                            </li>
                                            <li className="nav-item">
                                                <a
                                                    className="nav-link"
                                                    href="#"
                                                >
                                                    Tax exemption
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            {/* <!---------------/tabs------------> */}

                            {/* <!-----------order-details-------------> */}

                            <div className="col-12 col-md-8 col-lg-8 col-xl-9">
                                <div className="order-breadcrumb">
                                    <nav aria-label="breadcrumb">
                                        <ol className="breadcrumb">
                                            <li className="breadcrumb-item">
                                                <a href="#">Delivery orders</a>
                                            </li>
                                            <li
                                                className="breadcrumb-item active"
                                                aria-current="page"
                                            >
                                                Order details
                                            </li>
                                        </ol>
                                    </nav>
                                </div>
                                <div className="card-details">
                                    <h6 className="op-title">
                                        Order placed on - January 10,2021
                                    </h6>
                                    <hr />
                                    <div className="row grid-divider">
                                        <div className="col-12 col-md-8 col-lg-8 col-xl-9">
                                            <div className="order-info">
                                                <div className="row">
                                                    <div className="col-12 col-md-12 col-lg-6 col-xl-6">
                                                        <div className="order-address">
                                                            <div>
                                                                <img
                                                                    src={
                                                                        Road1Img
                                                                    }
                                                                />
                                                            </div>
                                                            <div className="oadd-details">
                                                                <p>Tx 77051</p>
                                                                <p>
                                                                    8630 cullen
                                                                    Blvd,
                                                                    Houuston
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-12 col-md-12 col-lg-6 col-xl-6">
                                                        <div className="order-date">
                                                            <p>
                                                                January 12,2021
                                                            </p>
                                                            <p>
                                                                between 3:00 PM
                                                                and 3:30 PM
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="order-notes">
                                                    <p>
                                                        <span>
                                                            Order notes :{' '}
                                                        </span>{' '}
                                                        Please speak with the
                                                        front desk and the
                                                        concierge will let you
                                                        up.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-12 col-md-4 col-lg-4 col-xl-3 ostatus-col">
                                            <div className="order-status">
                                                <h6 className="ostatus-deliver">
                                                    Delivered
                                                </h6>
                                                <h6>Order status</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {/* <!----------select order details------------------> */}
                                <div className="card-details order-card">
                                    <div className="select-reorder">
                                        <div className="custom-check">
                                            <label className="check ">
                                                <input
                                                    type="checkbox"
                                                    name="is_name1"
                                                />
                                                <span className="checkmark"></span>
                                                Select to reorder
                                            </label>
                                        </div>
                                        <div>
                                            <a
                                                href="#"
                                                className="btn btn-reorder"
                                            >
                                                Reorder
                                            </a>
                                        </div>
                                    </div>
                                    <hr />

                                    <div className="odfood-table">
                                        <h6>Lamb & Goat</h6>
                                        <div className="table-responsive">
                                            <table className="table table-borderless">
                                                <thead>
                                                    <tr>
                                                        <th className="col-one"></th>
                                                        <th className="col-two"></th>
                                                        <th className="col-three"></th>
                                                        <th className="col-three"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td className="food-check">
                                                            <label className="check ">
                                                                <input
                                                                    type="checkbox"
                                                                    name="is_name1"
                                                                />
                                                                <span className="checkmark"></span>
                                                            </label>
                                                        </td>
                                                        <td className="odfood-add">
                                                            <div className="select-add">
                                                                <div>
                                                                    <img
                                                                        src={
                                                                            Img4
                                                                        }
                                                                    />
                                                                </div>
                                                                <div className="shop-add">
                                                                    <p>
                                                                        USDA
                                                                        Primen
                                                                        Angus
                                                                        Boneless
                                                                        New York
                                                                        Strip
                                                                        steak
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td className="odfood-qty">
                                                            Qty - 5
                                                        </td>
                                                        <td className="odfood-qty">
                                                            $13.33
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td className="food-check">
                                                            <label className="check ">
                                                                <input
                                                                    type="checkbox"
                                                                    name="is_name1"
                                                                />
                                                                <span className="checkmark"></span>
                                                            </label>
                                                        </td>
                                                        <td className="odfood-add">
                                                            <div className="select-add">
                                                                <div>
                                                                    <img
                                                                        src={
                                                                            Img4
                                                                        }
                                                                    />
                                                                </div>
                                                                <div className="shop-add">
                                                                    <p>
                                                                        USDA
                                                                        Primen
                                                                        Angus
                                                                        Boneless
                                                                        New York
                                                                        Strip
                                                                        steak
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td className="odfood-qty">
                                                            Qty - 5
                                                        </td>
                                                        <td className="odfood-qty">
                                                            $13.33
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <hr />
                                    <div className="odfood-table">
                                        <h6>Turkey</h6>
                                        <div className="table-responsive">
                                            <table className="table table-borderless">
                                                <thead>
                                                    <tr>
                                                        <th className="col-one"></th>
                                                        <th className="col-two"></th>
                                                        <th className="col-three"></th>
                                                        <th className="col-three"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td className="food-check">
                                                            <label className="check ">
                                                                <input
                                                                    type="checkbox"
                                                                    name="is_name1"
                                                                />
                                                                <span className="checkmark"></span>
                                                            </label>
                                                        </td>
                                                        <td className="odfood-add">
                                                            <div className="select-add">
                                                                <div>
                                                                    <img
                                                                        src={
                                                                            Img4
                                                                        }
                                                                    />
                                                                </div>
                                                                <div className="shop-add">
                                                                    <p>
                                                                        USDA
                                                                        Primen
                                                                        Angus
                                                                        Boneless
                                                                        New York
                                                                        Strip
                                                                        steak
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td className="odfood-qty">
                                                            Qty - 5
                                                        </td>
                                                        <td className="odfood-qty">
                                                            $13.33
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td className="food-check">
                                                            <label className="check ">
                                                                <input
                                                                    type="checkbox"
                                                                    name="is_name1"
                                                                />
                                                                <span className="checkmark"></span>
                                                            </label>
                                                        </td>
                                                        <td className="odfood-add">
                                                            <div className="select-add">
                                                                <div>
                                                                    <img
                                                                        src={
                                                                            Img4
                                                                        }
                                                                    />
                                                                </div>
                                                                <div className="shop-add">
                                                                    <p>
                                                                        USDA
                                                                        Primen
                                                                        Angus
                                                                        Boneless
                                                                        New York
                                                                        Strip
                                                                        steak
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td className="odfood-qty">
                                                            Qty - 5
                                                        </td>
                                                        <td className="odfood-qty">
                                                            $13.33
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <hr />
                                    <div className="odfood-table">
                                        <h6>Chicken</h6>
                                        <div className="table-responsive">
                                            <table className="table table-borderless">
                                                <thead>
                                                    <tr>
                                                        <th className="col-one"></th>
                                                        <th className="col-two"></th>
                                                        <th className="col-three"></th>
                                                        <th className="col-three"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td className="food-check">
                                                            <label className="check ">
                                                                <input
                                                                    type="checkbox"
                                                                    name="is_name1"
                                                                />
                                                                <span className="checkmark"></span>
                                                            </label>
                                                        </td>
                                                        <td className="odfood-add">
                                                            <div className="select-add">
                                                                <div>
                                                                    <img
                                                                        src={
                                                                            Img4
                                                                        }
                                                                    />
                                                                </div>
                                                                <div className="shop-add">
                                                                    <p>
                                                                        USDA
                                                                        Primen
                                                                        Angus
                                                                        Boneless
                                                                        New York
                                                                        Strip
                                                                        steak
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td className="odfood-qty">
                                                            Qty - 5
                                                        </td>
                                                        <td className="odfood-qty">
                                                            $13.33
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td className="food-check">
                                                            <label className="check ">
                                                                <input
                                                                    type="checkbox"
                                                                    name="is_name1"
                                                                />
                                                                <span className="checkmark"></span>
                                                            </label>
                                                        </td>
                                                        <td className="odfood-add">
                                                            <div className="select-add">
                                                                <div>
                                                                    <img
                                                                        src={
                                                                            Img4
                                                                        }
                                                                    />
                                                                </div>
                                                                <div className="shop-add">
                                                                    <p>
                                                                        USDA
                                                                        Primen
                                                                        Angus
                                                                        Boneless
                                                                        New York
                                                                        Strip
                                                                        steak
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td className="odfood-qty">
                                                            Qty - 5
                                                        </td>
                                                        <td className="odfood-qty">
                                                            $13.33
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                {/* <!----------select order details------------------> */}
                            </div>
                        </div>

                        {/* <!-----------/order-details-------------> */}
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default OrdersDetails
