import React, { useState } from 'react'
import Sidebar from './Sidebar'
import BreadCrumb from '../../Common/BreadCrumb'

const SecuritySettings: React.FC = () => {
    const [data, setData] = useState({
        currentPassword: '',
        newPassword: '',
        confirmPassword: '',
    })

    const InputEvent = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target

        setData((prevVal) => {
            return {
                ...prevVal,
                [name]: value,
            }
        })
    }

    const submitForm = (e: React.FormEvent) => {
        //e.preventDefault();
        if (data.currentPassword && data.newPassword && data.newPassword)
            alert('Password Updated')
    }

    return (
        <React.Fragment>
            <div className="container">
                <div className="row row-margin">
                    <div className="col-12 col-md-12 col-lg-11 col-xl-9 mx-auto">
                        {/* <!-BreadCrumb--> */}
                        <BreadCrumb currentPage="My account" />

                        {/**BreadCrumb */}
                        <div className="row">
                            <Sidebar />
                            <div className="col-12 col-md-8 col-lg-8 col-xl-8">
                                <div className="food-details">
                                    <div>
                                        <h2>Change password</h2>
                                    </div>
                                </div>
                                <div className="bg-white main-section">
                                    <form>
                                        <div className="form-group">
                                            <label>Current password</label>
                                            <input
                                                type="password"
                                                className="form-control info-control"
                                                id="current-password"
                                                placeholder="Enter here .."
                                                name="currentPassword"
                                                value={data.currentPassword}
                                                onChange={InputEvent}
                                            />
                                        </div>

                                        <div className="form-group">
                                            <label>Create new password</label>
                                            <input
                                                type="password"
                                                className="form-control info-control"
                                                id="new-password"
                                                placeholder="min. 8 characters"
                                                name="newPassword"
                                                value={data.newPassword}
                                                onChange={InputEvent}
                                            />
                                        </div>

                                        <div className="form-group npassword">
                                            <label>Confirm new password</label>
                                            <input
                                                type="password"
                                                className="form-control info-control"
                                                id="confirm-password"
                                                placeholder="same as new passowrd"
                                                name="confirmPassword"
                                                value={data.confirmPassword}
                                                onChange={InputEvent}
                                            />
                                            <i
                                                className="fa fa-eye-slash"
                                                aria-hidden="true"
                                            ></i>
                                        </div>

                                        <div
                                            className="update-btn"
                                            onClick={submitForm}
                                        >
                                            <a
                                                href="#"
                                                className="btn btn-cart"
                                            >
                                                Update password
                                            </a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        {/* <!---------> */}
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default SecuritySettings
