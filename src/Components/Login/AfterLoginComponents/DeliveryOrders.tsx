import React from 'react'
import Sidebar from './Sidebar'
import { images } from '../../Constant/Constant'
import BreadCrumb from '../../Common/BreadCrumb'

const DeliveryOrders: React.FC = () => {
    return (
        <React.Fragment>
            <div className="container">
                <div className="row row-margin">
                    <div className="col-12 col-md-12 col-lg-11 col-xl-11 mx-auto">
                        {/* <!---------> */}
                        {/* <!---------breadcrumb------> */}
                        <BreadCrumb currentPage="My account" />
                        {/* <!-------/breadcrumb-------> */}
                        <div className="row">
                            <Sidebar />
                            <div className="col-12 col-md-8 col-lg-8 col-xl-9">
                                <div className="food-details">
                                    <div>
                                        <h2>Delivery orders</h2>
                                    </div>
                                </div>
                                <div className="bg-white main-section">
                                    <div className="table-responsive order-table">
                                        <table className="table mb-0">
                                            <thead>
                                                <tr>
                                                    <th scope="col">
                                                        Delivered
                                                    </th>
                                                    <th scope="col">Items</th>
                                                    <th scope="col"> Amount</th>
                                                    <th scope="col">Status</th>
                                                    <th scope="col">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div className="order-address">
                                                            <div>
                                                                <img
                                                                    src={
                                                                        images
                                                                            .roadIcon
                                                                            .default
                                                                    }
                                                                    alt="Icon"
                                                                />
                                                            </div>
                                                            <div className="delivery-details">
                                                                <p>
                                                                    Jan 11, 2021
                                                                </p>
                                                                <p>
                                                                    12:00pm -
                                                                    1:00 pm
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>15</td>
                                                    <td>$243.00</td>
                                                    <td>Delivered</td>
                                                    <td>
                                                        <a
                                                            href="http://159.65.142.31/newffm-design/newffm-latest/orders-detail.html"
                                                            className="btn btn-view"
                                                        >
                                                            View
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div className="order-address">
                                                            <div>
                                                                <img
                                                                    src={
                                                                        images
                                                                            .roadIcon
                                                                            .default
                                                                    }
                                                                    alt="Icon"
                                                                />
                                                            </div>
                                                            <div className="delivery-details">
                                                                <p>
                                                                    Jan 11, 2021
                                                                </p>
                                                                <p>
                                                                    12:00pm -
                                                                    1:00 pm
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>15</td>
                                                    <td>$243.00</td>
                                                    <td>Delivered</td>
                                                    <td>
                                                        <a
                                                            href="http://159.65.142.31/newffm-design/newffm-latest/orders-detail.html"
                                                            className="btn btn-view"
                                                        >
                                                            View
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div className="order-address">
                                                            <div>
                                                                <img
                                                                    src={
                                                                        images
                                                                            .roadIcon
                                                                            .default
                                                                    }
                                                                    alt="Icon"
                                                                />
                                                            </div>
                                                            <div className="delivery-details">
                                                                <p>
                                                                    Jan 11, 2021
                                                                </p>
                                                                <p>
                                                                    12:00pm -
                                                                    1:00 pm
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>15</td>
                                                    <td>$243.00</td>
                                                    <td>Delivered</td>
                                                    <td>
                                                        <a
                                                            href="http://159.65.142.31/newffm-design/newffm-latest/orders-detail.html"
                                                            className="btn btn-view"
                                                        >
                                                            View
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div className="order-address">
                                                            <div>
                                                                <img
                                                                    src={
                                                                        images
                                                                            .roadIcon
                                                                            .default
                                                                    }
                                                                    alt="Icon"
                                                                />
                                                            </div>
                                                            <div className="delivery-details">
                                                                <p>
                                                                    Jan 11, 2021
                                                                </p>
                                                                <p>
                                                                    12:00pm -
                                                                    1:00 pm
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>15</td>
                                                    <td>$243.00</td>
                                                    <td>Delivered</td>
                                                    <td>
                                                        <a
                                                            href="http://159.65.142.31/newffm-design/newffm-latest/orders-detail.html"
                                                            className="btn btn-view"
                                                        >
                                                            View
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div className="order-address">
                                                            <div>
                                                                <img
                                                                    src={
                                                                        images
                                                                            .roadIcon
                                                                            .default
                                                                    }
                                                                    alt="Icon"
                                                                />
                                                            </div>
                                                            <div className="delivery-details">
                                                                <p>
                                                                    Jan 11, 2021
                                                                </p>
                                                                <p>
                                                                    12:00pm -
                                                                    1:00 pm
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>15</td>
                                                    <td>$243.00</td>
                                                    <td>Delivered</td>
                                                    <td>
                                                        <a
                                                            href="http://159.65.142.31/newffm-design/newffm-latest/orders-detail.html"
                                                            className="btn btn-view"
                                                        >
                                                            View
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div className="order-address">
                                                            <div>
                                                                <img
                                                                    src={
                                                                        images
                                                                            .roadIcon
                                                                            .default
                                                                    }
                                                                    alt="Icon"
                                                                />
                                                            </div>
                                                            <div className="delivery-details">
                                                                <p>
                                                                    Jan 11, 2021
                                                                </p>
                                                                <p>
                                                                    12:00pm -
                                                                    1:00 pm
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>15</td>
                                                    <td>$243.00</td>
                                                    <td>Delivered</td>
                                                    <td>
                                                        <a
                                                            href="http://159.65.142.31/newffm-design/newffm-latest/orders-detail.html"
                                                            className="btn btn-view"
                                                        >
                                                            View
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div className="order-address">
                                                            <div>
                                                                <img
                                                                    src={
                                                                        images
                                                                            .roadIcon
                                                                            .default
                                                                    }
                                                                    alt="Icon"
                                                                />
                                                            </div>
                                                            <div className="delivery-details">
                                                                <p>
                                                                    Jan 11, 2021
                                                                </p>
                                                                <p>
                                                                    12:00pm -
                                                                    1:00 pm
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>15</td>
                                                    <td>$243.00</td>
                                                    <td>Delivered</td>
                                                    <td>
                                                        <a
                                                            href="http://159.65.142.31/newffm-design/newffm-latest/orders-detail.html"
                                                            className="btn btn-view"
                                                        >
                                                            View
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div className="order-address">
                                                            <div>
                                                                <img
                                                                    src={
                                                                        images
                                                                            .roadIcon
                                                                            .default
                                                                    }
                                                                    alt="Icon"
                                                                />
                                                            </div>
                                                            <div className="delivery-details">
                                                                <p>
                                                                    Jan 11, 2021
                                                                </p>
                                                                <p>
                                                                    12:00pm -
                                                                    1:00 pm
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>15</td>
                                                    <td>$243.00</td>
                                                    <td>Delivered</td>
                                                    <td>
                                                        <a
                                                            href="http://159.65.142.31/newffm-design/newffm-latest/orders-detail.html"
                                                            className="btn btn-view"
                                                        >
                                                            View
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div className="order-address">
                                                            <div>
                                                                <img
                                                                    src={
                                                                        images
                                                                            .roadIcon
                                                                            .default
                                                                    }
                                                                    alt="Icon"
                                                                />
                                                            </div>
                                                            <div className="delivery-details">
                                                                <p>
                                                                    Jan 11, 2021
                                                                </p>
                                                                <p>
                                                                    12:00pm -
                                                                    1:00 pm
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>15</td>
                                                    <td>$243.00</td>
                                                    <td>Delivered</td>
                                                    <td>
                                                        <a
                                                            href="http://159.65.142.31/newffm-design/newffm-latest/orders-detail.html"
                                                            className="btn btn-view"
                                                        >
                                                            View
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div className="order-address">
                                                            <div>
                                                                <img
                                                                    src={
                                                                        images
                                                                            .roadIcon
                                                                            .default
                                                                    }
                                                                    alt="Icon"
                                                                />
                                                            </div>
                                                            <div className="delivery-details">
                                                                <p>
                                                                    Jan 11, 2021
                                                                </p>
                                                                <p>
                                                                    12:00pm -
                                                                    1:00 pm
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>15</td>
                                                    <td>$243.00</td>
                                                    <td>Delivered</td>
                                                    <td>
                                                        <a
                                                            href="http://159.65.142.31/newffm-design/newffm-latest/orders-detail.html"
                                                            className="btn btn-view"
                                                        >
                                                            View
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div className="order-address">
                                                            <div>
                                                                <img
                                                                    src={
                                                                        images
                                                                            .roadIcon
                                                                            .default
                                                                    }
                                                                    alt="Icon"
                                                                />
                                                            </div>
                                                            <div className="delivery-details">
                                                                <p>
                                                                    Jan 11, 2021
                                                                </p>
                                                                <p>
                                                                    12:00pm -
                                                                    1:00 pm
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>15</td>
                                                    <td>$243.00</td>
                                                    <td>Delivered</td>
                                                    <td>
                                                        <a
                                                            href="http://159.65.142.31/newffm-design/newffm-latest/orders-detail.html"
                                                            className="btn btn-view"
                                                        >
                                                            View
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td> <td></td>{' '}
                                                    <td></td> <td></td>
                                                    <td></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <div className="food-details">
                                            <div className="page-details">
                                                <p>
                                                    Showing results 1-10 out of
                                                    22
                                                </p>
                                            </div>
                                            <div className="chevron-icon">
                                                <i
                                                    className="fa fa-chevron-left"
                                                    aria-hidden="true"
                                                ></i>
                                                <i
                                                    className="fa fa-chevron-right active"
                                                    aria-hidden="true"
                                                ></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {/* <!---------> */}
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default DeliveryOrders
