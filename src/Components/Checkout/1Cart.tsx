import React, { useContext, useEffect, useState } from 'react'
import { images } from '../Constant/Constant'
import OwlCarousel from 'react-owl-carousel'
import 'owl.carousel/dist/assets/owl.carousel.css'
import 'owl.carousel/dist/assets/owl.theme.default.css'
import { DataSlider } from '../Home/childComponents/Data'
import FoodCard from '../Common/FoodCard'
import { stateContext } from '../../GlobalState/Context'
import Calender from './childComponents/Calender'
import { formatNumber } from '../Helpers/utils'
import { Data } from './childComponents/Data'
import { useHistory } from 'react-router-dom'
import CartItems from './childComponents/CartItems'
import { Helmet } from 'react-helmet'
import { Modal } from 'react-bootstrap';

const Cart: React.FC = () => {
    const { state, dispatch } = useContext(stateContext)
    const [timeId, setTimeId] = useState<any>()
    const [ show , setShow ] = useState(false);
    const history = useHistory();

    useEffect(() => {
        localStorage.setItem(
            'deliveryDistance',
            JSON.stringify(state.deliveryDistance)
        )
        localStorage.setItem('PickUpTime', JSON.stringify(state.selectedTime))
        localStorage.setItem('PickUpDate', JSON.stringify(state.selectedDate))
        localStorage.setItem('showTime', JSON.stringify(state.showTime))
        localStorage.setItem('timeId', JSON.stringify(timeId))
    }, [
        state.deliveryPrice,
        state.selectedTime,
        state.selectedDate,
        state.showTime,
        timeId,
    ])

    const handleButton = (val: any) => {
        dispatch({ type: 'ADD_ITEM', payload: val })

        if (val.category === 'Meat') {
            dispatch({ type: 'itemMeat' })
        }
        if (val.category === 'Turkey') {
            dispatch({ type: 'itemTurkey' })
        }
    }

    const decrement = (val: any) => {
        dispatch({ type: 'DECREASE', payload: val })
    }

    const increment = (val: any) => {
        dispatch({ type: 'INCREASE', payload: val })
    }

    const handleRemove = (val: any) => {
        dispatch({ type: 'REMOVE_STATE', payload: val })
    }

    const handleTime = (time: string, Id: string) => {
        dispatch({ type: 'selected_Time', payload: time });
        setTimeId(Id);
    };

    const clearCart = () => {
        state.cartItems.map((val: any) => {
            return dispatch({ type: 'CLEAR_CART', payload: val })
        });
    };
    
    const handleCheckout = (e:React.FormEvent) =>{
        e.preventDefault();
        if(state.selectedDate && state.selectedTime){
            history.push('/checkout');
        }else{
            //alert('Please select the Data & Time');
            setShow(true);
        }
        dispatch({
            type: 'show_Footer',
            payload: false, 
        })

    };

    return (
        <React.Fragment>
            <Helmet>
                <title>
                    Cart - Farmer&#039;s Fresh Meat: Houston Meat Market &amp;
                    Butcher Shop
                </title>
            </Helmet>
            <div className="container">
                <div className="row">
                    {/*<!-------------------left column----------------------->*/}
                    <div className="col-12 col-md-8 col-lg-8 col-xl-9">
                        {/*<!------------review your order section--------------->*/}
                        <div className="cart-heading">
                            <div>
                                <h4>Review your order</h4>
                            </div>
                        </div>
                        <div className="col-12 bg-white main-section mb-m d-inline-block">
                            <div className="cart-pickup d-inline-block">
                                <img src={images.img1.default} alt="icon" />
                                <span>
                                    {' '} 
                                    Pickup from - Farmer’s Fresh Meat
                                    <b> {state.defaultPickUp}</b>
                                </span>
                                <span>
                                    on <b>{state.selectedDate}</b>
                                </span>
                                <span>
                                    at <b>{state.selectedTime}</b>
                                </span>
                            </div>
                            <hr />

                            {/*<!------------for pickup date------------------------->*/}
                            {/*<div className="cart-selection">*/}
                            {/*    <h6>Select pickup date</h6>*/}
                            {/*    <Calender />*/}
                            {/*</div>*/}
                            {/* <!------------/for pickup date------------------------->*/}

                            {/*<!------------for pickup time------------------------->*/}
                            {state.showTime === true ? (
                                <div className="col-12 cart-selection mt-3 d-inline-block p-0">
                                    <h6>Select pickup time</h6>
                                    {Data.map((val) => {
                                        return (
                                            <div
                                                className="pickup-time payment-method "
                                                key={val.id}
                                                onClick={() => {
                                                    handleTime(val.time, val.id)
                                                }}
                                            >
                                                <input
                                                    type="radio"
                                                    id={val.id}
                                                    name="radio2"
                                                />
                                                <label
                                                    htmlFor="myradio2.1" /*onClick={() => { handleTime(val.time) }}*/
                                                >

                                                    {val.time}
                                                </label>
                                            </div>
                                        )
                                    })}
                                </div>
                            ) : null}
                            {/*<!------------/for pickup time------------------------->*/}
                        </div>
                        {/*<!------------/review your order section--------------->*/}

                        {/*<!------------you may also like to order--------------->*/}
                        <div className="row">
                            <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                                <div className="food-details mb-sm feature-details">
                                    <div>
                                        <h2>You may also like to order</h2>
                                    </div>
                                    <div>
                                        <a href="#" className="ml-2">
                                            See more
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-12 col-md-12 col-lg-12 mb-m">
                                <OwlCarousel
                                    className="owl-theme owl-carousel"
                                    loop
                                    margin={10}
                                    nav
                                    items={4}
                                >
                                    {DataSlider.map((val, index) => {
                                        return (
                                            <FoodCard
                                                key={index}
                                                val={val}
                                                change="change"
                                            />
                                        )
                                    })}
                                </OwlCarousel>
                            </div>
                        </div>
                        {/*<!------------/you may also like to order--------------->*/}

                        {/*<!------------items in your cart--------------->*/}
                        <div className="row">
                            <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                                <div className="food-details mb-sm feature-details">
                                    <h2>Items in your cart</h2>
                                        <a  className="more-info"
                                            href="#"
                                            onClick={() => {
                                                clearCart()
                                            }}
                                        >
                                            Clear cart
                                        </a>
                                </div>
                            </div>
                            <CartItems />
                        </div>

                        {/*<!-----------///items in your cart////--------------->*/}
                    </div>
                    {/*<!-------------------/left column----------------------->*/}
                    {/*<!-------------------right column----------------------->*/}
                    <div
                        className="col-12 col-md-4 col-lg-4 col-xl-3"
                        id="right-content"
                    >
                        <div className="bg-white main-section mb-sm main-cart">
                            <div className="row">
                                <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                                    <div className="estimate-order">
                                        <h6>
                                            ${Math.trunc(state.price)}.
                                            <span>
                                                {
                                                    formatNumber(state.price)
                                                        .toString()
                                                        .split('.')[1]
                                                }
                                            </span>
                                        </h6>
                                        <p>Estimated order value</p>
                                        <p>
                                            +{' '}
                                            {formatNumber(
                                                (state.deliveryDistance /
                                                    1000) *
                                                    0.62137 *
                                                    5
                                            )}{' '}
                                            Delivery fee
                                        </p>
                                    </div>
                                    <form>
                                        <div className="promo-code mt-3">
                                            <textarea
                                                className="form-control input-control"
                                                rows={3}
                                                placeholder="Note for this order"
                                            ></textarea>
                                            <hr />
                                            <input
                                                type="text"
                                                className="form-control input-control"
                                                name="promo"
                                                placeholder="Promo code"
                                            />
                                        </div>
                                        {state.counter === 0 ? null : (
                                            <div
                                                className="proceed-button"
                                            >
                                                {/* <NavLink exact to={''}> */}
                                                    <button className="btn btn-proceed" onClick={(e)=>handleCheckout(e)}>
                                                        Proceed to checkout
                                                    </button>
                                                {/* </NavLink> */}
                                            </div>
                                        )}
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div className="bg-white main-section mb-m pr-2">
                            <div className="cart-items">
                                <h6 className="pt-1 pb-3">Most bought</h6>
                            </div>
                            <div className="scrollbar" id="style-4">
                                <div className="force-overflow">
                                    {/*<!------------------------------/product 1---------------------------->*/}
                                    <div className="most-product">
                                        {/******************MAPS*****************/}
                                        {DataSlider.map((val) => {
                                            return (
                                                <div
                                                    className="product-details cart-table"
                                                    key={val.id}
                                                >
                                                    <div className="row">
                                                        <div className="col-4 col-xl-4 cart-img">
                                                            <img
                                                                src={val.image}
                                                                alt="food"
                                                            />
                                                        </div>
                                                        <div className="col-8 col-xl-8 food-content">
                                                            <h6>{val.title}</h6>
                                                            <p className="price">
                                                                <sup>$</sup>
                                                                <span>
                                                                    {Math.trunc(
                                                                        val.price
                                                                    )}
                                                                </span>
                                                                <sup>
                                                                    {
                                                                        val.price
                                                                            .toString()
                                                                            .split(
                                                                                '.'
                                                                            )[1]
                                                                    }
                                                                </sup>
                                                                <del>
                                                                    $14.33
                                                                </del>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div className="row ">
                                                        <div className="col-md-12 col-xl-12 cart-row">
                                                            <div className="buttons">
                                                                {state.cartItems.find(
                                                                    (item: {
                                                                        id: any
                                                                    }) =>
                                                                        item.id ===
                                                                        val.id
                                                                ) ? (
                                                                    <button
                                                                        className={`cart-button ${state.toggled}`}
                                                                    >
                                                                        <>
                                                                            {state.cartItems
                                                                                .filter(
                                                                                    (item: {
                                                                                        id: any
                                                                                    }) =>
                                                                                        item.id ===
                                                                                        val.id
                                                                                )
                                                                                .map(
                                                                                    (
                                                                                        val: any
                                                                                    ) => {
                                                                                        return (
                                                                                            <div
                                                                                                className="added"
                                                                                                key={
                                                                                                    val.id
                                                                                                }
                                                                                            >
                                                                                                <span
                                                                                                    className="minus"
                                                                                                    onClick={() => {
                                                                                                        decrement(
                                                                                                            val
                                                                                                        )
                                                                                                    }}
                                                                                                >
                                                                                                    <i className="fa fa-minus"></i>
                                                                                                </span>
                                                                                                <span>
                                                                                                    {
                                                                                                        val.quantity
                                                                                                    }
                                                                                                </span>
                                                                                                <span
                                                                                                    className="plus"
                                                                                                    onClick={() => {
                                                                                                        increment(
                                                                                                            val
                                                                                                        )
                                                                                                    }}
                                                                                                >
                                                                                                    <i className="fa fa-plus"></i>
                                                                                                </span>
                                                                                                {val.quantity ===
                                                                                                0
                                                                                                    ? handleRemove(
                                                                                                          val
                                                                                                      )
                                                                                                    : null}
                                                                                            </div>
                                                                                        )
                                                                                    }
                                                                                )}
                                                                        </>
                                                                    </button>
                                                                ) : (
                                                                    <button
                                                                        className="cart-button menu-toggle"
                                                                        onClick={() =>
                                                                            handleButton(
                                                                                val
                                                                            )
                                                                        }
                                                                    >
                                                                        <span className="add-to-cart">
                                                                            Add
                                                                            to
                                                                            cart
                                                                        </span>
                                                                    </button>
                                                                )}
                                                            </div>
                                                        </div>
                                                        {/*<div className="col-md-5 col-xl-5 cart-row">*/}
                                                        {/*  <div className="pro-list">Add to list</div>*/}
                                                        {/*</div>*/}
                                                    </div>
                                                </div>
                                            )
                                        })}
                                        {/*<!------------------------------/product 1---------------------------->*/}
                                    </div>

                                    <hr className="my-0" />
                                </div>
                            </div>
                        </div>
                    </div>
                    {/*<!-------------------/right column----------------------->*/}
                </div>
            </div>
            <Modal show={show}>
              <Modal.Body>
                  <p>Please select the Date & Time</p>
                  <button onClick={()=>{setShow(false)}}>Ok</button>
              </Modal.Body>
            </Modal>
        </React.Fragment>
    )
}

export default Cart
