import React, { useState } from 'react'
import Cleave from 'cleave.js/react'

const PaymentCard: React.FC = () => {
    const [data, setData] = useState({
        name: '',
        cardNumber: '',
        expiry: '',
        cvc: '',
    })

    const InputEvent = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target

        setData((prevVal) => {
            return {
                ...prevVal,
                [name]: value,
            }
        })
        console.log(data)
    }

    return (
        <React.Fragment>
            <div className="col-12 form-details">
                <form className="row g-3">
                    <div className="col-md-6 fields">
                        <label
                            htmlFor="exampleInputFirstName"
                            className="form-label"
                        >
                            Card Holder's Name
                        </label>
                        <input
                            type="text"
                            className="form-control"
                            name="name"
                            placeholder=""
                            onChange={InputEvent}
                        />
                    </div>
                    <div className="col-md-6 fields dummy"></div>
                    <div className="col-md-6 fields">
                        <label
                            htmlFor="exampleInputFirstName"
                            className="form-label"
                        >
                            Card Number
                        </label>
                        <input
                            type="text"
                            className="form-control"
                            name="cardNumber"
                            placeholder=""
                            onChange={InputEvent}
                        />
                    </div>
                    <div className="col-md-6 fields dummy"></div>
                    <div className="col-md-3 fields">
                        <label
                            htmlFor="exampleInputFirstName"
                            className="form-label"
                        >
                            Expiry
                        </label>
                        <Cleave
                            placeholder="MM/YY"
                            options={{ date: true, datePattern: ['m', 'd'] }}
                            className="form-control"
                            name="expiry"
                            onChange={InputEvent}
                        />
                    </div>
                    <div className="col-md-3 fields">
                        <label
                            htmlFor="exampleInputFirstName"
                            className="form-label"
                        >
                            CVV/CVC
                        </label>
                        <Cleave
                            placeholder=""
                            options={{ blocks: [3], numericOnly: true }}
                            className="form-control"
                            name="cvc"
                            onChange={InputEvent}
                        />
                    </div>
                </form>
            </div>
        </React.Fragment>
    )
}

export default PaymentCard
