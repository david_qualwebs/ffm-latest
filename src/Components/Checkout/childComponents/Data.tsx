export const Data = [
    {
        time: '7:00AM - 9:00AM',
        id: '1',
    },

    {
        time: '9:00AM - 11:00AM',
        id: '2',
    },

    {
        time: '11:00AM - 1:00PM',
        id: '3',
    },

    {
        time: '1:00PM - 3:00PM',
        id: '4',
    },

    {
        time: '3:00PM - 5:00PM',
        id: '5',
    },

    {
        time: '5:00PM - 7:00PM',
        id: '6',
    },

    {
        time: '7:00PM - 9:00PM',
        id: '7',
    },
]
