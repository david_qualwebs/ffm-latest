import React, { useContext, useState } from 'react'
//import Img1 from "../../../assets/images/img1.1.png";
import { NavLink } from 'react-router-dom'
import { stateContext } from '../../../GlobalState/Context'
import { formatNumber } from '../../Helpers/utils'
import { images } from '../../Constant/Constant'
import PaymentCard from './PaymentCard'
import CartItems from './CartItems'
import { Helmet } from 'react-helmet'

const Checkout: React.FC = () => {
    const { state, dispatch } = useContext(stateContext)
    const [data, setData] = useState({
        fname: '',
        lname: '',
        email: '',
        phone: '',
        city: '',
        state: '',
        zipCode: '',
        Address: '',
    })

    const InputEvent = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target

        setData((prevVal) => {
            return {
                ...prevVal,
                [name]: value,
            }
        })
    }

    const submitForm = (e: React.FormEvent) => {
        e.preventDefault()
        alert(` FirstName=${data.fname}
                LastName=${data.lname}
                Mobile No.${data.phone}
                Email=${data.email}
                City=${data.city}
                State=${data.state}
                ZipCode=${data.zipCode}
                Address=${data.Address}
        `)
    }

    return (
        <React.Fragment>
            <Helmet>
                <title>
                    Checkout - Farmer&#039;s Fresh Meat: Houston Meat Market
                    &amp; Butcher Shop
                </title>
            </Helmet> 
            {/*<!-----------------main section----------------------->*/}

            <div className="container">
                <div className="row">
                    <div className="col-12 col-md-12 col-lg-12">
                        <div className="cart-heading">
                            <div>
                                <h4>Checkout</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12 col-md-12 col-lg-8 col-xl-9">
                        <div className="bg-white main-section mb-m checkout-info">
                            {/*<!------------pickup location section------------>*/}

                            <div className="cart-pickup d-inline-block">
                                <img src={images.img1.default} alt="icon" />
                                <span>
                                    {' '}
                                    Pickup from - Farmer’s Fresh Meat
                                    <b> {state.defaultPickUp}</b>
                                </span>
                                <span>
                                    on &nbsp;<b>{state.selectedDate}</b>
                                </span>
                                <span>
                                    at &nbsp;<b>{state.selectedTime}</b>
                                </span>
                            </div>

                            <NavLink exact to="/cart">
                                <span className="edit-tag">Edit selection</span>
                            </NavLink>
                            <hr />
                            {/*<!------------/pickup location section------------>
      
      
                                <!------------contact information------------>*/}
                            <div className="title">
                                <h3 className="mb-0">Delivery Details</h3>
                            </div>
                            {/*<div className="payment-method payment contact-info">
                                {<!----------address one----------->}
                                <input type="radio" id="myradio3" name="radio1" checked="checked" value="fashion" />
                                <label htmlFor="myradio3">
                                    <div>
                                        <h6 className="user-title">Salman Hussain</h6>
                                        <p className="country-contact">731 325-6584</p>
                                        <div className="add-edit"><a href="#" className="add-edit">Edit</a></div>
                                    </div>
                                </label>
                                {<!----------address one----------->
      
                                    <!----------address-two----------->}
                                <input type="radio" id="myradio4" name="radio1" value="fashion" />
                                <label htmlFor="myradio4">
                                    <div>
                                        <h6 className="user-title">Salman Hussain</h6>
                                        <p className="country-contact">731 325-6584</p>
                                        <div className="add-edit"><a href="#" className="add-edit">Edit</a></div>
                                    </div>
                                </label>
                                {/*<!----------address-two----------->}
                            </div>*/}

                            {/** form */}

                            <div className="col-xl-8 form-details">
                                <form className="row g-3" onSubmit={submitForm}>
                                    <div className="col-md-6 fields">
                                        <label
                                            htmlFor="exampleInputFirstName"
                                            className="form-label"
                                        >
                                            First Name
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="fname"
                                            placeholder=""
                                            value={data.fname}
                                            onChange={InputEvent}
                                        />
                                    </div>

                                    <div className="col-md-6 fields">
                                        <label
                                            htmlFor="exampleInputLastName"
                                            className="form-label"
                                        >
                                            Last Name
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="lname"
                                            placeholder=""
                                            value={data.lname}
                                            onChange={InputEvent}
                                        />
                                    </div>

                                    <div className="col-md-6 fields">
                                        <label
                                            htmlFor="exampleInputEmail"
                                            className="form-label"
                                        >
                                            Email address
                                        </label>
                                        <input
                                            type="email"
                                            className="form-control"
                                            name="email"
                                            placeholder=""
                                            value={data.email}
                                            onChange={InputEvent}
                                        />
                                    </div>

                                    <div className="col-md-6 fields">
                                        <label
                                            htmlFor="exampleInputPhone"
                                            className="form-label"
                                        >
                                            Phone No.
                                        </label>
                                        <input
                                            type="tel"
                                            className="form-control"
                                            name="phone"
                                            placeholder=""
                                            value={data.phone}
                                            onChange={InputEvent}
                                        />
                                    </div>
                                    <div className="col-12 fields">
                                        <label
                                            htmlFor="exampleInputAddress"
                                            className="form-label"
                                        >
                                            Street Address 1
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="Address"
                                            placeholder=""
                                            value={data.Address}
                                            onChange={InputEvent}
                                        />
                                    </div>
                                    <div className="col-12 fields">
                                        <label
                                            htmlFor="exampleInputAddress"
                                            className="form-label"
                                        >
                                            Street Address2 (optional)
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="Address"
                                            placeholder=""
                                            value={data.Address}
                                            onChange={InputEvent}
                                        />
                                    </div>
                                    <div className="col-md-4 fields">
                                        <label
                                            htmlFor="exampleInputCity"
                                            className="form-label"
                                        >
                                            City
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="city"
                                            placeholder=""
                                            value={data.city}
                                            onChange={InputEvent}
                                        />
                                    </div>

                                    <div className="col-md-4 fields">
                                        <label
                                            htmlFor="exampleInputState"
                                            className="form-label"
                                        >
                                            State
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="state"
                                            placeholder=""
                                            value={data.state}
                                            onChange={InputEvent}
                                        />
                                    </div>

                                    <div className="col-md-4 fields">
                                        <label
                                            htmlFor="exampleInputZip"
                                            className="form-label"
                                        >
                                            Zip Code
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="zipCode"
                                            placeholder=""
                                            value={data.zipCode}
                                            onChange={InputEvent}
                                        />
                                    </div>
                                    {/* <button type="submit" className="btn btn-primary" style = {{top:""}}>Submit</button> */}
                                </form>
                            </div>

                            {/** form */}

                            <div className="row">
                                <div className="col-12 px-3">
                                    <p className="cf-subtitle">
                                        *By entering your contact information,
                                        we consider you are ready to receive
                                        messages and calls from farmer’s fresh
                                        kitchen regarding order details and
                                        offers
                                    </p>
                                </div>
                            </div>

                            <hr />
                            {/*<!------------/contact information------------>*/}

                            {/******************* */}
                            {/*<!------------payment method------------>*/}
                            <div className="title">
                                <h3 className="mb-0">Payment Details</h3>
                            </div>
                            <PaymentCard />
                            <div className="payment-method payment">
                                {/*<!----------card one----------->}
                                <input type="radio" id="myradio1" name="radio1" checked="checked" value="fashion" />
                                <label htmlFor="myradio1">

                                    <div className=" visa-card text-right">
                                        <div><img src={VisaLogoImg} /></div>
                                    </div>

                                    <div className="card-number ch-card-no">
                                        <span>****</span>
                                        <span>****</span>
                                        <span>****</span>
                                        <span>8014</span>
                                    </div>

                                    <div className="food-details mb-sm chekcout-card">
                                        <div className="card-holder">
                                            <p className="crd-title ch-title">Cardholder’s name</p>
                                            <h6 className="crd-holder-name ch-holder">Rashid Hussain</h6>
                                        </div>
                                        <div className="card-holder chekcout-card">
                                            <p className="crd-title ch-title">Expiry</p>
                                            <h6 className="crd-holder-name ch-holder">22-2022</h6>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-12 col-xl-7">
                                            <div className="food-details mb-sm edit-info">
                                                <div className="edit-content">
                                                    <a href="#">Edit</a>
                                                </div>
                                                <div>
                                                    <button className="btn btn-select">Select</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </label>
                                {/*<!----------card one----------->*/}

                                {/*<!----------card two----------->}
                                <input type="radio" id="myradio2" name="radio1" value="fashion" />
                                <label htmlFor="myradio2">
                                    <div className="">
                                        <div className="visa-card text-right">
                                            <div><img src={VisaLogoImg} /></div>

                                        </div>

                                        <div className="card-number ch-card-no">
                                            <span>****</span>
                                            <span>****</span>
                                            <span>****</span>
                                            <span>8014</span>
                                        </div>

                                        <div className="food-details mb-sm chekcout-card">
                                            <div className="card-holder">
                                                <p className="crd-title ch-title">Cardholder’s name</p>
                                                <h6 className="crd-holder-name ch-holder">Rashid Hussain</h6>
                                            </div>
                                            <div className="card-holder chekcout-card">
                                                <p className="crd-title ch-title">Expiry</p>
                                                <h6 className="crd-holder-name ch-holder">22-2022</h6>
                                            </div>
                                        </div>

                                        <div className="row">
                                            <div className="col-12 col-xl-7">
                                                <div className="food-details mb-sm edit-info">
                                                    <div className="edit-content">
                                                        <a href="#">Edit</a>
                                                    </div>
                                                    <div>
                                                        <button className="btn btn-select">Select</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </label>
                                {/* <!----------card two-----------> */}
                            </div>

                            {/* <!------------/payment method------------> */}
                        </div>

                        {/* <!-----------------------review cart items---------------> */}

                        <div
                            className="row review-cart"
                            style={{ display: 'flex' }}
                        >
                            <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                                <div className="title">
                                    <h2>Review cart</h2>
                                    <NavLink className="more-info" exact to="/cart">
                                        Edit cart
                                    </NavLink>
                                </div>
                            </div>
                            <CartItems Change="change" />
                        </div>

                        {/* <!----------------turkey products------------------------> */}

                        {/* <!---------------/turkey-products------------------------> */}
                    </div>

                    <div className="col-12 col-md-12 col-lg-4 col-xl-3 account-card ">
                        <div className="bg-white main-section mb-sm checkout-info">
                            <div className="main-cart">
                                <div className="proceed-button">
                                    <button className="btn btn-proceed mt-2 mb-3 w-100">
                                        Proceed to payment
                                    </button>
                                </div>
                                <div className="estimate-order">
                                    <p>Subtotal*<span>{formatNumber(state.price)}</span></p>
                                    <p>Your savings
                                        <span>
                                            {formatNumber(
                                                state.orgPrice - state.price
                                            )}
                                        </span>
                                    </p>
                                    <h6>Est. Total <span>{formatNumber(state.price + 5)}</span></h6>
                                </div>
                                <div className="promo-code mt-3">
                                    <input
                                        type="text"
                                        className="form-control input-control"
                                        name="promo"
                                        placeholder="Promo code"
                                    />
                                </div>
                            </div>

                            <div className="amt-subtitle">
                                <p className="cf-subtitle mt-2">
                                    Estimate amount is not the exact amount you
                                    pay for your order, bank or transaction
                                    charges may apply for making payments by
                                    your bank.
                                </p>
                            </div>
                        </div>
                        {/************Address Details***********/}

                        <div className="col-12 p-0">
                            <div className="col-12 bg-white main-section mb-m">
                                <div className="footer-content store-loc">
                                    <div className="col-12 title p-0">
                                        <h2 className="mb-0">Store Details</h2>
                                    </div>
                                    <div className="contact-details">
                                        <div className="row">
                                            <div className="col-2 col-md-1 col-lg-2 col-xl-1">
                                                <i
                                                    className="fa fa-map-marker"
                                                    aria-hidden="true"
                                                ></i>
                                            </div>
                                            <div className="col-10 col-md-11 col-lg-10 col-xl-10 pl-0">
                                                <div className="hours-details">
                                                    <h6>
                                                        {state.defaultPickUp === 'Mesa'?'9541 Mesa Dr. Houston,TX 77078':
                                                        '8630 Cullen Blvd Houston, TX 77051'}
                                                        {/* <hr className="my-2"></hr> */}
                                                        <br />
                                                    </h6>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row">
                                            <div className="col-2 col-md-1 col-lg-2 col-xl-1">
                                                <i className="fa fa-phone"></i>
                                            </div>
                                            <div className="col-10 col-md-11 col-lg-10 col-xl-10 pl-0">
                                                <div className="hours-details">
                                                    <h6>+1 1234567890</h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-2 col-md-1 col-lg-2 col-xl-1">
                                                <i
                                                    className="fa fa-clock-o"
                                                    aria-hidden="true"
                                                ></i>
                                            </div>
                                            <div className="col-10 col-md-11 col-lg-10 col-xl-10 pl-0">
                                                <div className="hours-details mb-0">
                                                    <h6>Hours of Operations</h6>
                                                    <p className="mt-0">
                                                        Mon - Sat: 7am - 8pm
                                                    </p>
                                                    <p className="mt-0">
                                                        Sun: 7am - 4pm
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {/************Address Details***********/}
                    </div>
                </div>
            </div>

            {/*<hr className="mb-0" />*/}

            {/* <!-------------------/container1------------------------> */}
        </React.Fragment>
    )
}

export default Checkout
