import React, { useState, useContext } from 'react'
import { stateContext } from '../../../GlobalState/Context'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import addDays from 'date-fns/addDays'
import { images } from '../../Constant/Constant'

const Calender: React.FC = () => {
    const currentDate = new Date()
    const [startDate, setStartDate] = useState(new Date())
    const { state, dispatch } = useContext(stateContext)

    let month = new Array()
    month[0] = 'January'
    month[1] = 'February'
    month[2] = 'March'
    month[3] = 'April'
    month[4] = 'May'
    month[5] = 'June'
    month[6] = 'July'
    month[7] = 'August'
    month[8] = 'September'
    month[9] = 'October'
    month[10] = 'November'
    month[11] = 'December'

    let weekday = new Array(7)
    weekday[0] = 'Sunday'
    weekday[1] = 'Monday'
    weekday[2] = 'Tuesday'
    weekday[3] = 'Wednesday'
    weekday[4] = 'Thursday'
    weekday[5] = 'Friday'
    weekday[6] = 'Saturday'

    const dateChange = (selectDate: any) => {
        setStartDate(selectDate)
    }

    const handleChange = (DATE: number, MONTH: string) => {
        dispatch({ type: 'show_Time', payload: true })
        dispatch({ type: 'selected_Date', payload: `${MONTH} ${DATE}` })
    }

    return (
        <React.Fragment>
            <div
                className="pickup-date payment-method"
                onClick={() => {
                    handleChange(
                        startDate.getDate(),
                        month[addDays(new Date(startDate), 0).getMonth()]
                    )
                }}
            >
                <input type="radio" id="myradio1" name="radio1" value="date" />
                <label htmlFor="myradio1">
                    <span>
                        {startDate.getDate() === currentDate.getDate()
                            ? 'Today'
                            : weekday[startDate.getDay()]}{' '}
                    </span>{' '}

                    <b>{startDate.getDate()}</b>{' '}
                    <span>{month[addDays(new Date(startDate), 0).getMonth()]}</span>
                </label>
            </div>

            <div
                className="pickup-date payment-method"
                onClick={() => {
                    handleChange(
                        addDays(new Date(startDate), 1).getDate(),
                        month[addDays(new Date(startDate), 1).getMonth()]
                    )
                }}
            >
                <input
                    type="radio"
                    id="myradio1.1"
                    name="radio1"
                    value="date"
                />
                <label htmlFor="myradio1.1">
                    <span>
                        {' '}
                        {addDays(new Date(startDate), 1).getDate() ===
                        addDays(new Date(currentDate), 1).getDate()
                            ? 'Tomorrow'
                            : weekday[addDays(new Date(startDate), 1).getDay()]}
                    </span>

                    <b>
                        {addDays(new Date(startDate), 1).getDate()}
                    </b>{' '}
                    {' '}
                    <span>{month[addDays(new Date(startDate), 1).getMonth()]}</span>
                </label>
            </div>

            <div
                className="pickup-date payment-method"
                onClick={() => {
                    handleChange(
                        addDays(new Date(startDate), 2).getDate(),
                        month[addDays(new Date(startDate), 2).getMonth()]
                    )
                }}
            >
                <input
                    type="radio"
                    id="myradio1.2"
                    name="radio1"
                    value="date"
                />
                <label htmlFor="myradio1.2">
                    <span>
                        {' '}
                        {weekday[addDays(new Date(startDate), 2).getDay()]}{' '}
                    </span>{' '}

                    <b>
                        {addDays(new Date(startDate), 2).getDate()}
                    </b>{' '}
                    {' '}
                    <span>{month[addDays(new Date(startDate), 2).getMonth()]}</span>
                </label>
            </div>

            <div
                className="pickup-date payment-method"
                onClick={() => {
                    handleChange(
                        addDays(new Date(startDate), 3).getDate(),
                        month[addDays(new Date(startDate), 3).getMonth()]
                    )
                }}
            >
                <input
                    type="radio"
                    id="myradio1.3"
                    name="radio1"
                    value="date"
                />
                <label htmlFor="myradio1.3">
                    <span>
                        {' '}
                        {weekday[addDays(new Date(startDate), 3).getDay()]}{' '}
                    </span>{' '}

                    <b>
                        {addDays(new Date(startDate), 3).getDate()}
                    </b>{' '}
                    {' '}
                    <span>{month[addDays(new Date(startDate), 3).getMonth()]}</span>
                </label>
            </div>

            <div
                className="pickup-date payment-method"
                onClick={() => {
                    handleChange(
                        addDays(new Date(startDate), 4).getDate(),
                        month[addDays(new Date(startDate), 4).getMonth()]
                    )
                }}
            >
                <input
                    type="radio"
                    id="myradio1.4"
                    name="radio1"
                    value="date"
                />
                <label htmlFor="myradio1.4">
                    <span>
                        {' '}
                        {weekday[addDays(new Date(startDate), 4).getDay()]}{' '}
                    </span>{' '}

                    <b>
                        {addDays(new Date(startDate), 4).getDate()}
                    </b>{' '}
                    {' '}
                    <span>{month[addDays(new Date(startDate), 4).getMonth()]}</span>
                </label>
            </div>

            <div
                className="pickup-date payment-method"
                onClick={() => {
                    handleChange(
                        addDays(new Date(startDate), 5).getDate(),
                        month[addDays(new Date(startDate), 5).getMonth()]
                    )
                }}
            >
                <input
                    type="radio"
                    id="myradio1.5"
                    name="radio1"
                    value="date"
                />
                <label htmlFor="myradio1.5">
                    <span>
                        {' '}
                        {weekday[addDays(new Date(startDate), 5).getDay()]}{' '}
                    </span>{' '}

                    <b>
                        {addDays(new Date(startDate), 5).getDate()}
                    </b>{' '}
                    {' '}
                    <span>{month[addDays(new Date(startDate), 5).getMonth()]}</span>
                </label>
            </div>

            <div
                className="pickup-date payment-method"
                onClick={() => {
                    handleChange(
                        addDays(new Date(startDate), 6).getDate(),
                        month[addDays(new Date(startDate), 6).getMonth()]
                    )
                }}
            >
                <input
                    type="radio"
                    id="myradio1.6"
                    name="radio1"
                    value="date"
                />
                <label htmlFor="myradio1.6">
                    <span>
                        {' '}
                        {weekday[addDays(new Date(startDate), 6).getDay()]}{' '}
                    </span>{' '}

                    <b>
                        {addDays(new Date(startDate), 6).getDate()}
                    </b>{' '}
                    {' '}
                    <span>{month[addDays(new Date(startDate), 6).getMonth()]}</span>
                </label>
            </div>
            <div className="custom-date">
                <span>Select date</span>
                <DatePicker
                    selected={startDate}
                    onChange={dateChange}
                    minDate={new Date()}
                />
            </div>
        </React.Fragment>
    )
}

export default Calender
