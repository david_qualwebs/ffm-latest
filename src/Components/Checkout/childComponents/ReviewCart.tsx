import React, { useContext } from 'react'
import { stateContext } from '../../../GlobalState/Context'

const ReviewCart: React.FC = () => {
    const { state, dispatch } = useContext(stateContext)

    return (
        <React.Fragment>
            {state.cartItems.find((val: any) => val.category === 'Meat') ? (
                <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                    <div className="bg-white main-section mb-m">
                        <div className="cart-items">
                            <h6>
                                Meats <span>{/*(3 items)*/}</span>
                            </h6>
                        </div>
                        <div className="table-responsive cart-table">
                            <table className="table mb-0">
                                <tbody>
                                    {state.cartItems
                                        .filter((item: any) => {
                                            if (item.category === 'Meat') {
                                                return item
                                            }
                                        })
                                        .map((item: any) => {
                                            return (
                                                <tr className="border-details">
                                                    <td className="button-width">
                                                        <div className="close-btn">
                                                            <span className="fa fa-times"></span>
                                                        </div>
                                                    </td>
                                                    <td className="product-details product-width">
                                                        <div className="row">
                                                            <div className="col-4 col-md-4 col-xl-2 cart-img">
                                                                <div className="review-img">
                                                                    <img
                                                                        src={
                                                                            item.image
                                                                        }
                                                                        alt="food"
                                                                    />
                                                                </div>
                                                            </div>
                                                            <div className="col-8 col-md-8 col-xl-10 food-content">
                                                                <h6>
                                                                    {item.title}
                                                                </h6>
                                                                <a
                                                                    href="#"
                                                                    className="add-notes"
                                                                >
                                                                    Add notes to
                                                                    this item{' '}
                                                                    <i className="fas fa-pencil-alt"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    
                                                    <td className="product-details cart-width">
                                                        <div className="food-content">
                                                            {' '}
                                                            <p className="price">
                                                                <sup>$</sup>
                                                                <span>
                                                                    {item.price}
                                                                </span>
                                                                <sup></sup>
                                                            </p>
                                                        </div>
                                                    </td>
                                                    <td className="product-details cart-width cart-alignment review-align">
                                                        <div className="pro-qty">
                                                            <span>
                                                                Qty{' '}
                                                                <span>
                                                                    {
                                                                        item.quantity
                                                                    }
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            ) : null}

            {/*<!----------------turkey products------------------------>*/}

            {state.cartItems.find((val: any) => val.category === 'Turkey') ? (
                <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                    <div className="bg-white main-section mb-m">
                        <div className="cart-items">
                            <h6>
                                Turkey <span>{/*(3 items)*/}</span>
                            </h6>
                        </div>

                        <div className="table-responsive cart-table">
                            <table className="table mb-0">
                                <tbody>
                                    {state.cartItems
                                        .filter((item: any) => {
                                            if (item.category === 'Turkey') {
                                                return item
                                            }
                                        })
                                        .map((item: any) => {
                                            return (
                                                <tr className="border-details">
                                                    <td className="button-width">
                                                        <div className="close-btn">
                                                            <span className="fa fa-times"></span>
                                                        </div>
                                                    </td>
                                                    <td className="product-details product-width">
                                                        <div className="row">
                                                            <div className="col-4 col-md-4 col-xl-2 cart-img">
                                                                <div className="review-img">
                                                                    <img
                                                                        src={
                                                                            item.image
                                                                        }
                                                                        alt="food"
                                                                    />
                                                                </div>
                                                            </div>
                                                            <div className="col-8 col-md-8 col-xl-10 food-content">
                                                                <h6>
                                                                    USDA Primen
                                                                    Angus
                                                                    Boneless New
                                                                    York Stripe
                                                                </h6>
                                                                <a
                                                                    href="#"
                                                                    className="add-notes"
                                                                >
                                                                    Add notes to
                                                                    this item{' '}
                                                                    <i className="fas fa-pencil-alt"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    
                                                    <td className="product-details cart-width">
                                                        <div className="food-content">
                                                            {' '}
                                                            <p className="price">
                                                                <sup>$</sup>
                                                                <span>
                                                                    {item.price}
                                                                </span>
                                                                <sup></sup>
                                                            </p>
                                                        </div>
                                                    </td>
                                                    <td className="product-details cart-width cart-alignment review-align">
                                                        <div className="pro-qty">
                                                            <span>
                                                                Qty{' '}
                                                                <span>
                                                                    {
                                                                        item.quantity
                                                                    }
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            ) : null}

            {/*<!---------------/turkey-products------------------------>*/}
        </React.Fragment>
    )
}

export default ReviewCart
