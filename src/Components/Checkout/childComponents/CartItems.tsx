import React, { useContext, useEffect } from 'react'
import { stateContext } from '../../../GlobalState/Context'

interface props {
    Change?: string
}

const CartItems: React.FC<props> = ({ Change }) => {
    const { state, dispatch } = useContext(stateContext)

    useEffect(() => {
        localStorage.setItem('itemTurkey', JSON.stringify(state.itemTurkey))
        localStorage.setItem('itemBeef', JSON.stringify(state.itemBeef))
        localStorage.setItem('itemChicken', JSON.stringify(state.itemChicken))
        localStorage.setItem('itemPork', JSON.stringify(state.itemPork))
        localStorage.setItem('itemLambGoat', JSON.stringify(state.itemLambGoat))
        localStorage.setItem('itemDeli', JSON.stringify(state.itemDeli))
        localStorage.setItem(
            'itemHomemadeSausage',
            JSON.stringify(state.itemHomemadeSausage)
        )
        localStorage.setItem('itemPantry', JSON.stringify(state.itemPantry))
        localStorage.setItem(
            'itemFruitsVegetables',
            JSON.stringify(state.itemFruitsVegetables)
        )
        localStorage.setItem(
            'itemWholesaleBulk',
            JSON.stringify(state.itemWholesaleBulk)
        )
    }, [
        state.itemTurkey,
        state.itemBeef,
        state.itemChicken,
        state.itemPork,
        state.itemLambGoat,
        state.itemDeli,
        state.itemHomemadeSausage,
        state.itemPantry,
        state.itemFruitsVegetables,
    ]);

    const decrement = (val: any) => {
        dispatch({ type: 'DECREASE', payload: val });
    };

    const increment = (val: any) => {
        dispatch({ type: 'INCREASE', payload: val });
    };

    const RemoveProduct = (item: any) => {
        dispatch({ type: 'REMOVE_STATE', payload: item });

        if (item.category === 'Turkey') {
            dispatch({ type: 'itemTurkeyDecrease' });
        }

        if (item.category === 'Beef') {
            dispatch({ type: 'itemBeefDecerease' });
        }

        if (item.category === 'Chicken') {
            dispatch({ type: 'itemChickenDecerease' })
        }

        if (item.category === 'Pork') {
            dispatch({ type: 'itemPorkDecerease' })
        }

        if (item.category === 'Lamb & Goat') {
            dispatch({ type: 'itemLambGoatDecerease' })
        }

        if (item.category === 'Deli') {
            dispatch({ type: 'itemDeliDecerease' })
        }

        if (item.category === 'Homemade Sausage') {
            dispatch({ type: 'itemHomemadeSausageDecerease' })
        }

        if (item.category === 'Pantry') {
            dispatch({ type: 'itemPantryDecerease' })
        }

        if (item.category === 'Fruits & Vegetables') {
            dispatch({ type: 'itemFruitsVegetablesDecerease' })
        }

        if (item.category === 'Wholesale & Bulk') {
            dispatch({ type: 'itemWholesaleBulkDecerease' })
        }
    };

    const handleRemove = (item: any) => {
        if (item.quantity === 1) {
            dispatch({ type: 'REMOVE_ITEM', payload: item })
        } else {
            item.price = item.price * item.quantity
            item.orgPrice = item.orgPrice * item.quantity
            dispatch({ type: 'REMOVE_ITEM', payload: item })
        }

        if (item.category === 'Turkey') {
            dispatch({ type: 'itemTurkeyDecrease' })
        }

        if (item.category === 'Beef') {
            dispatch({ type: 'itemBeefDecerease' })
        }

        if (item.category === 'Chicken') {
            dispatch({ type: 'itemChickenDecerease' })
        }

        if (item.category === 'Pork') {
            dispatch({ type: 'itemPorkDecerease' })
        }

        if (item.category === 'Lamb & Goat') {
            dispatch({ type: 'itemLambGoatDecerease' })
        }

        if (item.category === 'Deli') {
            dispatch({ type: 'itemDeliDecerease' })
        }

        if (item.category === 'Homemade Sausage') {
            dispatch({ type: 'itemHomemadeSausageDecerease' })
        }

        if (item.category === 'Pantry') {
            dispatch({ type: 'itemPantryDecerease' })
        }

        if (item.category === 'Fruits & Vegetables') {
            dispatch({ type: 'itemFruitsVegetablesDecerease' })
        }

        if (item.category === 'Wholesale & Bulk') {
            dispatch({ type: 'itemWholesaleBulkDecerease' })
        }
    }

    /*const RemoveTurkey = (item:any) =>{
     
        dispatch({ type: "REMOVE_STATE", payload: item });
        dispatch({ type: "itemTurkeyDecrease" });
    };*/

    return (
        <React.Fragment>
            {/*<!------------items in your cart--------------->*/}

            {/********* TURKEY ********/}
            {state.cartItems.find((val: any) => val.category === 'Turkey') ? (
                <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                    <div className="bg-white main-section mb-sm">
                        <div className="cart-items">
                            <h6>
                                
                                Turkey <span>{state.itemTurkey}</span>
                            </h6>
                        </div>

                        <div className="table-responsive cart-table">
                            <table className="table mb-0">
                                <tbody>
                                    {state.cartItems
                                        .filter((item: any) => {
                                            if (item.category === 'Turkey') {
                                                return item
                                            }
                                        })
                                        .map((item: any, index: any) => {
                                            return (
                                                <tr
                                                    className="table-row"
                                                    key={item.id}
                                                >
                                                    {Change ? null : (
                                                        <td className="remove">
                                                            <div
                                                                className="close-btn"
                                                                onClick={() => {
                                                                    handleRemove(
                                                                        item
                                                                    )
                                                                }}
                                                            >
                                                                <h6>
                                                                    <i className="fa fa-times"></i>
                                                                </h6>
                                                            </div>
                                                        </td>
                                                    )}
                                                    <td className="image">
                                                        <div className="contain">
                                                            <img
                                                                src={
                                                                    item.image
                                                                }
                                                                alt="food"
                                                            />
                                                        </div>
                                                    </td>
                                                    <td className="name">
                                                        <div className="contain">
                                                            <h6>
                                                                {item.title}
                                                            </h6>
                                                            <div className="contain">
                                                                {' '}
                                                                <p className="text-left">
                                                                    3.97lb / each
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td className="note">
                                                        {Change ? null : (
                                                            <a
                                                                href="#"
                                                                className="add-notes"
                                                            >
                                                                Add notes (optional)
                                                                <i className="fas fa-pencil-alt"></i>
                                                            </a>
                                                        )}
                                                    </td>
                                                    {Change ? (
                                                        <td className="quantity">
                                                            <div className="contain pro-qty">
                                                                <span>
                                                                    Qty{' '}
                                                                    <span>
                                                                        {
                                                                            item.quantity
                                                                        }
                                                                    </span>
                                                                </span>
                                                            </div>
                                                        </td>
                                                    ) : (
                                                        <td className="quantity">
                                                            <div className="num-block skin-5">
                                                                <div className="num-in">
                                                                        <span className="minus dis decrement" onClick={() => { decrement( item ) }}>
                                                                            <i className="fa fa-minus"></i>
                                                                        </span>
                                                                    <input type="text" className="in-num" value={item.quantity} name="qty"/>
                                                                    <span className="plus increment" onClick={() => {increment(item)}}>
                                                                            <i className="fa fa-plus"></i>
                                                                        </span>
                                                                </div>
                                                                {item.quantity ===0? RemoveProduct(item): null}
                                                            </div>
                                                        </td>
                                                    )}

                                                    <td className="price-details">
                                                        <div className="food-content contain">
                                                            {' '}
                                                            <p className="price">
                                                                <sup>$</sup>
                                                                <span>13.</span>
                                                                <sup>33</sup>
                                                            </p>
                                                            <span className="each-price">$6.50 each</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            ) : null}

            {/********* Beef ****** */}
            {state.cartItems.find((val: any) => val.category === 'Beef') ? (
                <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                    <div className="bg-white main-section mb-sm">
                        <div className="cart-items">
                            <h6>
                                Beef <span>{state.itemBeef}</span>
                            </h6>
                        </div>

                        <div className="table-responsive cart-table">
                            <table className="table mb-0">
                                <tbody>
                                    {/*MAP*/}
                                    {state.cartItems
                                        .filter((item: any) => {
                                            if (item.category === 'Beef') {
                                                return item
                                            }
                                        })
                                        .map((item: any, index: number) => {
                                            return (
                                                <tr
                                                    className="table-row"
                                                    key={item.id}
                                                >
                                                    {Change ? null : (
                                                        <td className="remove">
                                                            <div
                                                                className="close-btn"
                                                                onClick={() => {
                                                                    handleRemove(
                                                                        item
                                                                    )
                                                                }}
                                                            >
                                                                <h6>
                                                                    <i className="fa fa-times"></i>
                                                                </h6>
                                                            </div>
                                                        </td>
                                                    )}
                                                    <td className="image">
                                                        <div className="contain">
                                                            <img
                                                                src={
                                                                    item.image
                                                                }
                                                                alt="food"
                                                            />
                                                        </div>
                                                    </td>
                                                    <td className="name">
                                                        <div className="contain">
                                                            <h6>
                                                                {item.title}
                                                            </h6>
                                                            <div className="contain">
                                                                {' '}
                                                                <p className="text-left">
                                                                    3.97lb / each
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td className="note">
                                                        {Change ? null : (
                                                            <a
                                                                href="#"
                                                                className="add-notes"
                                                            >
                                                                Add notes (optional)
                                                                <i className="fas fa-pencil-alt"></i>
                                                            </a>
                                                        )}
                                                    </td>
                                                    {Change ? (
                                                        <td className="quantity">
                                                            <div className="contain pro-qty">
                                                                <span>
                                                                    Qty{' '}
                                                                    <span>
                                                                        {
                                                                            item.quantity
                                                                        }
                                                                    </span>
                                                                </span>
                                                            </div>
                                                        </td>
                                                    ) : (
                                                        <td className="quantity">
                                                            <div className="num-block skin-5">
                                                                <div className="num-in">
                                                                        <span className="minus dis decrement" onClick={() => { decrement( item ) }}>
                                                                            <i className="fa fa-minus"></i>
                                                                        </span>
                                                                    <input type="text" className="in-num" value={item.quantity} name="qty"/>
                                                                    <span className="plus increment" onClick={() => {increment(item)}}>
                                                                            <i className="fa fa-plus"></i>
                                                                        </span>
                                                                </div>
                                                                {item.quantity ===0? RemoveProduct(item): null}
                                                            </div>
                                                        </td>
                                                    )}

                                                    <td className="price-details">
                                                        <div className="food-content contain">
                                                            {' '}
                                                            <p className="price">
                                                                <sup>$</sup>
                                                                <span>13.</span>
                                                                <sup>33</sup>
                                                            </p>
                                                            <span className="each-price">$6.50 each</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            ) : null}

            {/********* /Beef ********/}

            {/********* Chicken ****** */}
            {state.cartItems.find((val: any) => val.category === 'Chicken') ? (
                <div className="col-12 col-md-12 col-lg-12 col-xl-12 ">
                    <div className="bg-white main-section mb-sm">
                        <div className="cart-items">
                            <h6>
                                Chicken <span>{state.itemChicken}</span>
                            </h6>
                        </div>

                        <div className="table-responsive cart-table">
                            <table className="table mb-0">
                                <tbody>
                                    {/*MAP*/}
                                    {state.cartItems
                                        .filter((item: any) => {
                                            if (item.category === 'Chicken') {
                                                return item
                                            }
                                        })
                                        .map((item: any, index: number) => {
                                            return (
                                                <tr
                                                    className="table-row"
                                                    key={item.id}
                                                >
                                                    {Change ? null : (
                                                        <td className="remove">
                                                            <div
                                                                className="close-btn"
                                                                onClick={() => {
                                                                    handleRemove(
                                                                        item
                                                                    )
                                                                }}
                                                            >
                                                                <h6>
                                                                    <i className="fa fa-times"></i>
                                                                </h6>
                                                            </div>
                                                        </td>
                                                    )}
                                                    <td className="image">
                                                        <div className="contain">
                                                            <img
                                                                src={
                                                                    item.image
                                                                }
                                                                alt="food"
                                                            />
                                                        </div>
                                                    </td>
                                                    <td className="name">
                                                        <div className="contain">
                                                            <h6>
                                                                {item.title}
                                                            </h6>
                                                            <div className="contain">
                                                                {' '}
                                                                <p className="text-left">
                                                                    3.97lb / each
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td className="note">
                                                        {Change ? null : (
                                                            <a
                                                                href="#"
                                                                className="add-notes"
                                                            >
                                                                Add notes (optional)
                                                                <i className="fas fa-pencil-alt"></i>
                                                            </a>
                                                        )}
                                                    </td>
                                                    {Change ? (
                                                        <td className="quantity">
                                                            <div className="contain pro-qty">
                                                                <span>
                                                                    Qty{' '}
                                                                    <span>
                                                                        {
                                                                            item.quantity
                                                                        }
                                                                    </span>
                                                                </span>
                                                            </div>
                                                        </td>
                                                    ) : (
                                                        <td className="quantity">
                                                            <div className="num-block skin-5">
                                                                <div className="num-in">
                                                                        <span className="minus dis decrement" onClick={() => { decrement( item ) }}>
                                                                            <i className="fa fa-minus"></i>
                                                                        </span>
                                                                    <input type="text" className="in-num" value={item.quantity} name="qty"/>
                                                                    <span className="plus increment" onClick={() => {increment(item)}}>
                                                                            <i className="fa fa-plus"></i>
                                                                        </span>
                                                                </div>
                                                                {item.quantity ===0? RemoveProduct(item): null}
                                                            </div>
                                                        </td>
                                                    )}

                                                    <td className="price-details">
                                                        <div className="food-content contain">
                                                            {' '}
                                                            <p className="price">
                                                                <sup>$</sup>
                                                                <span>13.</span>
                                                                <sup>33</sup>
                                                            </p>
                                                            <span className="each-price">$6.50 each</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            ) : null}

            {/********* /Chicken ********/}

            {/********* Pork ****** */}
            {state.cartItems.find((val: any) => val.category === 'Pork') ? (
                <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                    <div className="bg-white main-section mb-sm">
                        <div className="cart-items">
                            <h6>
                                Pork <span>{state.itemPork}</span>
                            </h6>
                        </div>

                        <div className="table-responsive cart-table">
                            <table className="table mb-0">
                                <tbody>
                                    {/*MAP*/}
                                    {state.cartItems
                                        .filter((item: any) => {
                                            if (item.category === 'Pork') {
                                                return item
                                            }
                                        })
                                        .map((item: any, index: number) => {
                                            return (
                                                <tr
                                                    className="table-row"
                                                    key={item.id}
                                                >
                                                    {Change ? null : (
                                                        <td className="remove">
                                                            <div
                                                                className="close-btn"
                                                                onClick={() => {
                                                                    handleRemove(
                                                                        item
                                                                    )
                                                                }}
                                                            >
                                                                <h6>
                                                                    <i className="fa fa-times"></i>
                                                                </h6>
                                                            </div>
                                                        </td>
                                                    )}
                                                    <td className="image">
                                                        <div className="contain">
                                                            <img
                                                                src={
                                                                    item.image
                                                                }
                                                                alt="food"
                                                            />
                                                        </div>
                                                    </td>
                                                    <td className="name">
                                                        <div className="contain">
                                                            <h6>
                                                                {item.title}
                                                            </h6>
                                                            <div className="contain">
                                                                {' '}
                                                                <p className="text-left">
                                                                    3.97lb / each
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td className="note">
                                                        {Change ? null : (
                                                            <a
                                                                href="#"
                                                                className="add-notes"
                                                            >
                                                                Add notes (optional)
                                                                <i className="fas fa-pencil-alt"></i>
                                                            </a>
                                                        )}
                                                    </td>
                                                    {Change ? (
                                                        <td className="quantity">
                                                            <div className="contain pro-qty">
                                                                <span>
                                                                    Qty{' '}
                                                                    <span>
                                                                        {
                                                                            item.quantity
                                                                        }
                                                                    </span>
                                                                </span>
                                                            </div>
                                                        </td>
                                                    ) : (
                                                        <td className="quantity">
                                                            <div className="num-block skin-5">
                                                                <div className="num-in">
                                                                        <span className="minus dis decrement" onClick={() => { decrement( item ) }}>
                                                                            <i className="fa fa-minus"></i>
                                                                        </span>
                                                                    <input type="text" className="in-num" value={item.quantity} name="qty"/>
                                                                    <span className="plus increment" onClick={() => {increment(item)}}>
                                                                            <i className="fa fa-plus"></i>
                                                                        </span>
                                                                </div>
                                                                {item.quantity ===0? RemoveProduct(item): null}
                                                            </div>
                                                        </td>
                                                    )}

                                                    <td className="price-details">
                                                        <div className="food-content contain">
                                                            {' '}
                                                            <p className="price">
                                                                <sup>$</sup>
                                                                <span>13.</span>
                                                                <sup>33</sup>
                                                            </p>
                                                            <span className="each-price">$6.50 each</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            ) : null}

            {/********* /Pork ********/}

            {/********* Lamb & Goat ****** */}
            {state.cartItems.find(
                (val: any) => val.category === 'Lamb & Goat'
            ) ? (
                <div className="col-12 col-md-12 col-lg-12 col-xl-12 ">
                    <div className="bg-white main-section mb-sm">
                        <div className="cart-items">
                            <h6>
                                Lamb & Goat <span>{state.itemLambGoat}</span>
                            </h6>
                        </div>

                        <div className="table-responsive cart-table">
                            <table className="table mb-0">
                                <tbody>
                                    {/*MAP*/}
                                    {state.cartItems
                                        .filter((item: any) => {
                                            if (
                                                item.category === 'Lamb & Goat'
                                            ) {
                                                return item
                                            }
                                        })
                                        .map((item: any, index: number) => {
                                            return (
                                                <tr
                                                    className="table-row"
                                                    key={item.id}
                                                >
                                                    {Change ? null : (
                                                        <td className="remove">
                                                            <div
                                                                className="close-btn"
                                                                onClick={() => {
                                                                    handleRemove(
                                                                        item
                                                                    )
                                                                }}
                                                            >
                                                                <h6>
                                                                    <i className="fa fa-times"></i>
                                                                </h6>
                                                            </div>
                                                        </td>
                                                    )}
                                                    <td className="image">
                                                        <div className="contain">
                                                            <img
                                                                src={
                                                                    item.image
                                                                }
                                                                alt="food"
                                                            />
                                                        </div>
                                                    </td>
                                                    <td className="name">
                                                        <div className="contain">
                                                            <h6>
                                                                {item.title}
                                                            </h6>
                                                            <div className="contain">
                                                                {' '}
                                                                <p className="text-left">
                                                                    3.97lb / each
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td className="note">
                                                        {Change ? null : (
                                                            <a
                                                                href="#"
                                                                className="add-notes"
                                                            >
                                                                Add notes (optional)
                                                                <i className="fas fa-pencil-alt"></i>
                                                            </a>
                                                        )}
                                                    </td>
                                                    {Change ? (
                                                        <td className="quantity">
                                                            <div className="contain pro-qty">
                                                                <span>
                                                                    Qty{' '}
                                                                    <span>
                                                                        {
                                                                            item.quantity
                                                                        }
                                                                    </span>
                                                                </span>
                                                            </div>
                                                        </td>
                                                    ) : (
                                                        <td className="quantity">
                                                            <div className="num-block skin-5">
                                                                <div className="num-in">
                                                                        <span className="minus dis decrement" onClick={() => { decrement( item ) }}>
                                                                            <i className="fa fa-minus"></i>
                                                                        </span>
                                                                    <input type="text" className="in-num" value={item.quantity} name="qty"/>
                                                                    <span className="plus increment" onClick={() => {increment(item)}}>
                                                                            <i className="fa fa-plus"></i>
                                                                        </span>
                                                                </div>
                                                                {item.quantity ===0? RemoveProduct(item): null}
                                                            </div>
                                                        </td>
                                                    )}

                                                    <td className="price-details">
                                                        <div className="food-content contain">
                                                            {' '}
                                                            <p className="price">
                                                                <sup>$</sup>
                                                                <span>13.</span>
                                                                <sup>33</sup>
                                                            </p>
                                                            <span className="each-price">$6.50 each</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            ) : null}

            {/********* /Lamb & Goat ********/}

            {/********* Deli ****** */}
            {state.cartItems.find((val: any) => val.category === 'Deli') ? (
                <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                    <div className="bg-white main-section mb-sm">
                        <div className="cart-items">
                            <h6>
                                Deli <span>{state.itemDeli}</span>
                            </h6>
                        </div>

                        <div className="table-responsive cart-table">
                            <table className="table mb-0">
                                <tbody>
                                    {/*MAP*/}
                                    {state.cartItems
                                        .filter((item: any) => {
                                            if (item.category === 'Deli') {
                                                return item
                                            }
                                        })
                                        .map((item: any, index: number) => {
                                            return (
                                                <tr
                                                    className="table-row"
                                                    key={item.id}
                                                >
                                                    {Change ? null : (
                                                        <td className="remove">
                                                            <div
                                                                className="close-btn"
                                                                onClick={() => {
                                                                    handleRemove(
                                                                        item
                                                                    )
                                                                }}
                                                            >
                                                                <h6>
                                                                    <i className="fa fa-times"></i>
                                                                </h6>
                                                            </div>
                                                        </td>
                                                    )}
                                                    <td className="image">
                                                        <div className="contain">
                                                            <img
                                                                src={
                                                                    item.image
                                                                }
                                                                alt="food"
                                                            />
                                                        </div>
                                                    </td>
                                                    <td className="name">
                                                        <div className="contain">
                                                            <h6>
                                                                {item.title}
                                                            </h6>
                                                            <div className="contain">
                                                                {' '}
                                                                <p className="text-left">
                                                                    3.97lb / each
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td className="note">
                                                        {Change ? null : (
                                                            <a
                                                                href="#"
                                                                className="add-notes"
                                                            >
                                                                Add notes (optional)
                                                                <i className="fas fa-pencil-alt"></i>
                                                            </a>
                                                        )}
                                                    </td>
                                                    {Change ? (
                                                        <td className="quantity">
                                                            <div className="contain pro-qty">
                                                                <span>
                                                                    Qty{' '}
                                                                    <span>
                                                                        {
                                                                            item.quantity
                                                                        }
                                                                    </span>
                                                                </span>
                                                            </div>
                                                        </td>
                                                    ) : (
                                                        <td className="quantity">
                                                            <div className="num-block skin-5">
                                                                <div className="num-in">
                                                                        <span className="minus dis decrement" onClick={() => { decrement( item ) }}>
                                                                            <i className="fa fa-minus"></i>
                                                                        </span>
                                                                    <input type="text" className="in-num" value={item.quantity} name="qty"/>
                                                                    <span className="plus increment" onClick={() => {increment(item)}}>
                                                                            <i className="fa fa-plus"></i>
                                                                        </span>
                                                                </div>
                                                                {item.quantity ===0? RemoveProduct(item): null}
                                                            </div>
                                                        </td>
                                                    )}

                                                    <td className="price-details">
                                                        <div className="food-content contain">
                                                            {' '}
                                                            <p className="price">
                                                                <sup>$</sup>
                                                                <span>13.</span>
                                                                <sup>33</sup>
                                                            </p>
                                                            <span className="each-price">$6.50 each</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            ) : null}

            {/********* /Deli ********/}

            {/********* Homemade Sausage ****** */}
            {state.cartItems.find(
                (val: any) => val.category === 'Homemade Sausage'
            ) ? (
                <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                    <div className="bg-white main-section mb-sm">
                        <div className="cart-items">
                            <h6>
                                Homemade Sausage{' '}
                                <span>{state.itemHomemadeSausage}</span>
                            </h6>
                        </div>

                        <div className="table-responsive cart-table">
                            <table className="table mb-0">
                                <tbody>
                                    {/*MAP*/}
                                    {state.cartItems
                                        .filter((item: any) => {
                                            if (
                                                item.category ===
                                                'Homemade Sausage'
                                            ) {
                                                return item
                                            }
                                        })
                                        .map((item: any, index: number) => {
                                            return (
                                                <tr
                                                    className="table-row"
                                                    key={item.id}
                                                >
                                                    {Change ? null : (
                                                        <td className="remove">
                                                            <div
                                                                className="close-btn"
                                                                onClick={() => {
                                                                    handleRemove(
                                                                        item
                                                                    )
                                                                }}
                                                            >
                                                                <h6>
                                                                    <i className="fa fa-times"></i>
                                                                </h6>
                                                            </div>
                                                        </td>
                                                    )}
                                                    <td className="image">
                                                        <div className="contain">
                                                            <img
                                                                src={
                                                                    item.image
                                                                }
                                                                alt="food"
                                                            />
                                                        </div>
                                                    </td>
                                                    <td className="name">
                                                        <div className="contain">
                                                            <h6>
                                                                {item.title}
                                                            </h6>
                                                            <div className="contain">
                                                                {' '}
                                                                <p className="text-left">
                                                                    3.97lb / each
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td className="note">
                                                        {Change ? null : (
                                                            <a
                                                                href="#"
                                                                className="add-notes"
                                                            >
                                                                Add notes (optional)
                                                                <i className="fas fa-pencil-alt"></i>
                                                            </a>
                                                        )}
                                                    </td>
                                                    {Change ? (
                                                        <td className="quantity">
                                                            <div className="contain pro-qty">
                                                                <span>
                                                                    Qty{' '}
                                                                    <span>
                                                                        {
                                                                            item.quantity
                                                                        }
                                                                    </span>
                                                                </span>
                                                            </div>
                                                        </td>
                                                    ) : (
                                                        <td className="quantity">
                                                            <div className="num-block skin-5">
                                                                <div className="num-in">
                                                                        <span className="minus dis decrement" onClick={() => { decrement( item ) }}>
                                                                            <i className="fa fa-minus"></i>
                                                                        </span>
                                                                    <input type="text" className="in-num" value={item.quantity} name="qty"/>
                                                                    <span className="plus increment" onClick={() => {increment(item)}}>
                                                                            <i className="fa fa-plus"></i>
                                                                        </span>
                                                                </div>
                                                                {item.quantity ===0? RemoveProduct(item): null}
                                                            </div>
                                                        </td>
                                                    )}

                                                    <td className="price-details">
                                                        <div className="food-content contain">
                                                            {' '}
                                                            <p className="price">
                                                                <sup>$</sup>
                                                                <span>13.</span>
                                                                <sup>33</sup>
                                                            </p>
                                                            <span className="each-price">$6.50 each</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            ) : null}

            {/********* /Homemade Sausage ********/}

            {/********* Pantry ****** */}
            {state.cartItems.find((val: any) => val.category === 'Pantry') ? (
                <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                    <div className="bg-white main-section mb-sm">
                        <div className="cart-items">
                            <h6>
                                Pantry <span>{state.itemPantry}</span>
                            </h6>
                        </div>

                        <div className="table-responsive cart-table">
                            <table className="table mb-0">
                                <tbody>
                                    {/*MAP*/}
                                    {state.cartItems
                                        .filter((item: any) => {
                                            if (item.category === 'Pantry') {
                                                return item
                                            }
                                        })
                                        .map((item: any, index: number) => {
                                            return (
                                                <tr
                                                    className="table-row"
                                                    key={item.id}
                                                >
                                                    {Change ? null : (
                                                        <td className="remove">
                                                            <div
                                                                className="close-btn"
                                                                onClick={() => {
                                                                    handleRemove(
                                                                        item
                                                                    )
                                                                }}
                                                            >
                                                                <h6>
                                                                    <i className="fa fa-times"></i>
                                                                </h6>
                                                            </div>
                                                        </td>
                                                    )}
                                                    <td className="image">
                                                        <div className="contain">
                                                            <img
                                                                src={
                                                                    item.image
                                                                }
                                                                alt="food"
                                                            />
                                                        </div>
                                                    </td>
                                                    <td className="name">
                                                        <div className="contain">
                                                            <h6>
                                                                {item.title}
                                                            </h6>
                                                            <div className="contain">
                                                                {' '}
                                                                <p className="text-left">
                                                                    3.97lb / each
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td className="note">
                                                        {Change ? null : (
                                                            <a
                                                                href="#"
                                                                className="add-notes"
                                                            >
                                                                Add notes (optional)
                                                                <i className="fas fa-pencil-alt"></i>
                                                            </a>
                                                        )}
                                                    </td>
                                                    {Change ? (
                                                        <td className="quantity">
                                                            <div className="contain pro-qty">
                                                                <span>
                                                                    Qty{' '}
                                                                    <span>
                                                                        {
                                                                            item.quantity
                                                                        }
                                                                    </span>
                                                                </span>
                                                            </div>
                                                        </td>
                                                    ) : (
                                                        <td className="quantity">
                                                            <div className="num-block skin-5">
                                                                <div className="num-in">
                                                                        <span className="minus dis decrement" onClick={() => { decrement( item ) }}>
                                                                            <i className="fa fa-minus"></i>
                                                                        </span>
                                                                    <input type="text" className="in-num" value={item.quantity} name="qty"/>
                                                                    <span className="plus increment" onClick={() => {increment(item)}}>
                                                                            <i className="fa fa-plus"></i>
                                                                        </span>
                                                                </div>
                                                                {item.quantity ===0? RemoveProduct(item): null}
                                                            </div>
                                                        </td>
                                                    )}

                                                    <td className="price-details">
                                                        <div className="food-content contain">
                                                            {' '}
                                                            <p className="price">
                                                                <sup>$</sup>
                                                                <span>13.</span>
                                                                <sup>33</sup>
                                                            </p>
                                                            <span className="each-price">$6.50 each</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            ) : null}

            {/********* /Pantry ********/}

            {/********* Fruits & Vegetables ****** */}
            {state.cartItems.find(
                (val: any) => val.category === 'Fruits & Vegetables'
            ) ? (
                <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                    <div className="bg-white main-section mb-sm">
                        <div className="cart-items">
                            <h6>
                                Fruits & Vegetables{' '}
                                <span>{state.itemFruitsVegetables}</span>
                            </h6>
                        </div>

                        <div className="table-responsive cart-table">
                            <table className="table mb-0">
                                <tbody>
                                    {/*MAP*/}
                                    {state.cartItems
                                        .filter((item: any) => {
                                            if (
                                                item.category ===
                                                'Fruits & Vegetables'
                                            ) {
                                                return item
                                            }
                                        })
                                        .map((item: any, index: number) => {
                                            return (
                                                <tr
                                                    className="table-row"
                                                    key={item.id}
                                                >
                                                    {Change ? null : (
                                                        <td className="remove">
                                                            <div
                                                                className="close-btn"
                                                                onClick={() => {
                                                                    handleRemove(
                                                                        item
                                                                    )
                                                                }}
                                                            >
                                                                <h6>
                                                                    <i className="fa fa-times"></i>
                                                                </h6>
                                                            </div>
                                                        </td>
                                                    )}
                                                    <td className="image">
                                                        <div className="contain">
                                                            <img
                                                                src={
                                                                    item.image
                                                                }
                                                                alt="food"
                                                            />
                                                        </div>
                                                    </td>
                                                    <td className="name">
                                                        <div className="contain">
                                                            <h6>
                                                                {item.title}
                                                            </h6>
                                                            <div className="contain">
                                                                {' '}
                                                                <p className="text-left">
                                                                    3.97lb / each
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td className="note">
                                                        {Change ? null : (
                                                            <a
                                                                href="#"
                                                                className="add-notes"
                                                            >
                                                                Add notes (optional)
                                                                <i className="fas fa-pencil-alt"></i>
                                                            </a>
                                                        )}
                                                    </td>
                                                    {Change ? (
                                                        <td className="quantity">
                                                            <div className="contain pro-qty">
                                                                <span>
                                                                    Qty{' '}
                                                                    <span>
                                                                        {
                                                                            item.quantity
                                                                        }
                                                                    </span>
                                                                </span>
                                                            </div>
                                                        </td>
                                                    ) : (
                                                        <td className="quantity">
                                                            <div className="num-block skin-5">
                                                                <div className="num-in">
                                                                        <span className="minus dis decrement" onClick={() => { decrement( item ) }}>
                                                                            <i className="fa fa-minus"></i>
                                                                        </span>
                                                                    <input type="text" className="in-num" value={item.quantity} name="qty"/>
                                                                    <span className="plus increment" onClick={() => {increment(item)}}>
                                                                            <i className="fa fa-plus"></i>
                                                                        </span>
                                                                </div>
                                                                {item.quantity ===0? RemoveProduct(item): null}
                                                            </div>
                                                        </td>
                                                    )}

                                                    <td className="price-details">
                                                        <div className="food-content contain">
                                                            {' '}
                                                            <p className="price">
                                                                <sup>$</sup>
                                                                <span>13.</span>
                                                                <sup>33</sup>
                                                            </p>
                                                            <span className="each-price">$6.50 each</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            ) : null}

            {/********* /Fruits & Vegetables ********/}

            {/********* Wholesale & Bulk ****** */}
            {state.cartItems.find(
                (val: any) => val.category === 'Wholesale & Bulk'
            ) ? (
                <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                    <div className="bg-white main-section mb-sm">
                        <div className="cart-items">
                            <h6>
                                Wholesale & Bulk{' '}
                                <span>{state.itemWholesaleBulk}</span>
                            </h6>
                        </div>

                        <div className="table-responsive cart-table">
                            <table className="table mb-0">
                                <tbody>
                                    {/*MAP*/}
                                    {state.cartItems
                                        .filter((item: any) => {
                                            if (
                                                item.category ===
                                                'Wholesale & Bulk'
                                            ) {
                                                return item
                                            }
                                        })
                                        .map((item: any, index: number) => {
                                            return (
                                                <tr
                                                    className="table-row"
                                                    key={item.id}
                                                >
                                                    {Change ? null : (
                                                        <td className="remove">
                                                            <div
                                                                className="close-btn"
                                                                onClick={() => {
                                                                    handleRemove(
                                                                        item
                                                                    )
                                                                }}
                                                            >
                                                                <h6>
                                                                    <i className="fa fa-times"></i>
                                                                </h6>
                                                            </div>
                                                        </td>
                                                    )}
                                                    <td className="image">
                                                        <div className="contain">
                                                            <img
                                                                src={
                                                                    item.image
                                                                }
                                                                alt="food"
                                                            />
                                                        </div>
                                                    </td>
                                                    <td className="name">
                                                        <div className="contain">
                                                            <h6>
                                                                {item.title}
                                                            </h6>
                                                            <div className="contain">
                                                                {' '}
                                                                <p className="text-left">
                                                                    3.97lb / each
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td className="note">
                                                        {Change ? null : (
                                                            <a
                                                                href="#"
                                                                className="add-notes"
                                                            >
                                                                Add notes (optional)
                                                                <i className="fas fa-pencil-alt"></i>
                                                            </a>
                                                        )}
                                                    </td>
                                                    {Change ? (
                                                        <td className="quantity">
                                                            <div className="contain pro-qty">
                                                                <span>
                                                                    Qty{' '}
                                                                    <span>
                                                                        {
                                                                            item.quantity
                                                                        }
                                                                    </span>
                                                                </span>
                                                            </div>
                                                        </td>
                                                    ) : (
                                                        <td className="quantity">
                                                            <div className="num-block skin-5">
                                                                <div className="num-in">
                                                                        <span className="minus dis decrement" onClick={() => { decrement( item ) }}>
                                                                            <i className="fa fa-minus"></i>
                                                                        </span>
                                                                    <input type="text" className="in-num" value={item.quantity} name="qty"/>
                                                                    <span className="plus increment" onClick={() => {increment(item)}}>
                                                                            <i className="fa fa-plus"></i>
                                                                        </span>
                                                                </div>
                                                                {item.quantity ===0? RemoveProduct(item): null}
                                                            </div>
                                                        </td>
                                                    )}

                                                    <td className="price-details">
                                                        <div className="food-content contain">
                                                            {' '}
                                                            <p className="price">
                                                                <sup>$</sup>
                                                                <span>13.</span>
                                                                <sup>33</sup>
                                                            </p>
                                                            <span className="each-price">$6.50 each</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            ) : null}

            {/********* /Wholesale & Bulk ********/}
            {/*<!------------/items in your cart--------------->*/}
        </React.Fragment>
    )
}

export default CartItems
