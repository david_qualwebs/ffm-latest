import React, { useContext, useEffect, useState } from 'react'
import Sidebar from '../Common/Sidebar'
import Banner from '../Common/Banner'
import FirstSection from './childComponents/FirstSection'
import Featured from './childComponents/Featured'
import MostPopular from './childComponents/MostPopular'
import HomeBanner from './childComponents/HomeBanner'
import NewMeat from './childComponents/NewMeat'
import TopSelling from './childComponents/TopSelling'
import Recommended from './childComponents/Recommended'
import ShopSelection from './childComponents/ShopSelection'
import Subscribe from '../Common/Subscribe'
import useGeolocation from './childComponents/useGeolocation'
import { stateContext } from '../../GlobalState/Context'
import { Modal } from 'react-bootstrap'

const Home: React.FC = () => {
    const destination = new google.maps.LatLng(
        29.85250139999999,
        -95.25277410000001
    )
    const destination1 = new google.maps.LatLng(29.6672782, -95.35597050000001)
    const { state, dispatch } = useContext(stateContext)
    const location = useGeolocation()
    const [mod, setMod] = useState(false)

    useEffect(() => {
        if (state.defaultPickUp === '') {
            if (location.loaded) {
                const src = new google.maps.LatLng(
                    location.coordinates.lat,
                    location.coordinates.lng
                )
                const dis =
                    google.maps.geometry.spherical.computeDistanceBetween(
                        src,
                        destination
                    )
                const dis1 =
                    google.maps.geometry.spherical.computeDistanceBetween(
                        src,
                        destination1
                    )

                if (dis > dis1) {
                    dispatch({ type: 'defaultPickUp', payload: 'Cullen' })
                    //dispatch({type:"delivery_Price",payload:((dis1/1000)*0.62137)});

                    if (state.modShow === 0) {
                        setMod(true)
                    }
                } else {
                    dispatch({ type: 'defaultPickUp', payload: 'Mesa' })
                    // dispatch({type:"delivery_Price",payload:((dis/1000)*0.62137)});

                    if (state.modShow === 0) {
                        setMod(true)
                    }
                }
            }
        }
        if (state.counter === 0) {
            $('#wrapper').removeClass('toggled')
        } else {
            $('#wrapper').addClass('toggled')
        }

        localStorage.setItem('Modal', JSON.stringify(state.modShow))
        localStorage.setItem('PickUp', JSON.stringify(state.defaultPickUp))
    }, [location.coordinates, state.modShow, state.defaultPickUp])

    return (
        <React.Fragment>
            <div className="container-fluid">
                <div className="row">
                    <div className="spacer-home"></div>
                    <Sidebar />
                    <div className="col-12 col-md-10 col-lg-10 col-xl-9 column">
                        <Banner />
                        <FirstSection />
                        <Featured />
                        <MostPopular />
                        <HomeBanner />
                        <NewMeat />
                        <TopSelling />
                        <HomeBanner />
                        <Recommended />
                    </div>
                </div>
            </div>
            <div className="container-fluid">
                <div className="bg-white shop-section">
                    <div className="row">
                        <ShopSelection />
                    </div>
                </div>
            </div>
            <Subscribe />
            <Modal className="storeModal" show={mod} /*className="modalSize"*/>
                <Modal.Body>
                    <div className="col-12 p-0">
                        <h2 className="w-100 mt-1 mb-3"><b>Shopping in Mesa</b></h2>
                        <p className="w-100 mb-0">
                            Let's confirm your shopping store before you start. Is <b>Farmer's Fresh Meat at{' '}
                            {state.defaultPickUp}</b> the right store ?
                        </p>
                    </div>
                    <hr></hr>
                    <div>
                        <button
                            className="btn btn-s proceed-button"
                            onClick={() => {
                                setMod(false)
                                dispatch({ type: 'modShow' })
                            }}
                        >
                            Shop at Mesa
                        </button>
                        <button
                            className="btn btn-s cart-button"
                            onClick={() => {
                                setMod(false)
                                dispatch({ type: 'modShow' })
                                dispatch({ type: 'MODAL', payload: true })
                            }}
                        >
                            Change store
                        </button>
                    </div>
                </Modal.Body>
            </Modal>
        </React.Fragment>
    )
}

export default Home
