import FoodImg from '../../../assets/images/new-img2.png'
import Img3 from '../../../assets/images/new-img3.png'
import Img5 from '../../../assets/images/new-img5.png'
import SliderImg from '../../../assets/images/slider-img.png'
import NewImg3 from '../../../assets/images/img3.png'
import avgImg from '../../../assets/images/avg.png'
import NewImg8 from '../../../assets/images/new-img8.png'
import Img2 from '../../../assets/images/img2.png'
import homeImg4 from '../../../assets/images/home-img4.png'
import NewImg7 from '../../../assets/images/new-img7.png'

const Data = [
    {
        image: FoodImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 1,
        title: 'Boneless Chicken Breast',
        category: 'Beef',
        onSale: true,
    },

    {
        image: SliderImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 2,
        title: 'Leg Quarters',
        category: 'Turkey',
    },

    {
        image: NewImg3,
        price: 13.33,
        orgPrice: 14.33,
        id: 3,
        title: 'First Cut Pork Chop',
        category: 'Pork',
    },

    {
        image: Img2,
        price: 13.33,
        orgPrice: 14.33,
        id: 4,
        title: 'Fresh Turkey Wings',
        category: 'Turkey',
        onSale: true,
    },

    {
        image: NewImg8,
        price: 13.33,
        orgPrice: 14.33,
        id: 5,
        title: 'Beef Shank',
        category: 'Beef',
    },
]

const CardData = [
    {
        image: Img3,
    },

    {
        image: Img3,
    },

    {
        image: Img3,
    },
]

const ShopData = [
    {
        image: homeImg4,
        price: 13.33,
        orgPrice: 14.33,
        id: 101,
        title: 'Boneless Chicken Breast',
        category: 'Chicken',
    },

    {
        image: Img5,
        price: 13.33,
        orgPrice: 14.33,
        id: 102,
        title: 'Leg Quarters',
        category: 'Turkey',
    },

    {
        image: Img2,
        price: 13.33,
        orgPrice: 14.33,
        id: 103,
        title: 'First Cut Pork Chop',
        category: 'Pork',
    },
]

const ShopData1 = [
    {
        image: Img5,
        price: 13.33,
        orgPrice: 14.33,
        id: 104,
        title: 'Boneless Chicken Breast',
        category: 'Chicken',
    },

    {
        image: NewImg8,
        price: 13.33,
        orgPrice: 14.33,
        id: 105,
        title: 'Leg Quarters',
        category: 'Turkey',
    },

    {
        image: NewImg7,
        price: 13.33,
        orgPrice: 14.33,
        id: 106,
        title: 'First Cut Pork Chop',
        category: 'Pork',
    },
]

const ShopData2 = [
    {
        image: Img5,
        price: 13.33,
        orgPrice: 14.33,
        id: 107,
        title: 'Lamb Chops',
        category: 'Beef',
    },

    {
        image: avgImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 108,
        title: 'Leg Quarters',
        category: 'Turkey',
    },

    {
        image: NewImg3,
        price: 13.33,
        orgPrice: 14.33,
        id: 109,
        title: 'Short Ribs',
        category: 'Turkey',
    },
]

const DataSlider = [
    {
        image: avgImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 11,
        title: 'Lamb Chops',
        category: 'beef',
    },

    {
        image: NewImg8,
        price: 13.33,
        orgPrice: 14.33,
        id: 12,
        title: 'Leg Quarters',
        category: 'Turkey',
        onSale: true,
    },
    {
        image: NewImg7,
        price: 13.33,
        orgPrice: 14.33,
        id: 13,
        title: 'Short Ribs',
        category: 'Beef',
    },

    {
        image: NewImg3,
        price: 13.33,
        orgPrice: 14.33,
        id: 14,
        title: 'Fresh Turkey Wings',
        category: 'Turkey',
        onSale: true,
    },

    {
        image: FoodImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 15,
        title: 'Beef Shank',
        category: 'Beef',
    },

    {
        image: avgImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 16,
        title: 'Lamb Chops',
        category: 'Lamb & Goat',
    },

    {
        image: NewImg8,
        price: 13.33,
        orgPrice: 14.33,
        id: 17,
        title: 'Leg Quarters',
        category: 'Turkey',
        onSale: true,
    },

    {
        image: NewImg7,
        price: 13.33,
        orgPrice: 14.33,
        id: 18,
        title: 'Short Ribs',
        category: 'Beef',
    },

    {
        image: NewImg3,
        price: 13.33,
        orgPrice: 14.33,
        id: 19,
        title: 'Fresh Turkey Wings',
        category: 'Turkey',
    },

    {
        image: FoodImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 20,
        title: 'Beef Shank',
        category: 'Beef',
        onSale: true,
    },

    /*{
        image:avgImg,
        price:13.33,
        orgPrice:14.33,
        id:21,
        title:"Lamb Chops",
        category:"Lamb & Goat"
    },

    {
        image:NewImg8,
        price:13.33,
        orgPrice:14.33,
        id:22,
        title:"Leg Quarters",
        category:"Turkey",
        onSale:true
    },

    {
        image:NewImg7,
        price:13.33,
        orgPrice:14.33,
        id:23,
        title:"Short Ribs",
        category:"Beef"
    },

    {
        image:NewImg3,
        price:13.33,
        orgPrice:14.33,
        id:24,
        title:"Fresh Turkey Wings",
        category:"Turkey",
        onSale:true
    },*/
]

const MostPopularData = [
    {
        image: FoodImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 25,
        title: 'Chuck Steak',
        category: 'Pork',
        onSale: true,
    },

    {
        image: NewImg3,
        price: 13.33,
        orgPrice: 14.33,
        id: 26,
        title: 'Skirt Steak',
        category: 'Turkey',
    },
    {
        image: NewImg7,
        price: 13.33,
        orgPrice: 14.33,
        id: 27,
        title: 'First Cut Pork Chop',
        category: 'Pork',
    },

    {
        image: NewImg8,
        price: 13.33,
        orgPrice: 14.33,
        id: 28,
        title: 'Fresh Turkey Wings',
        category: 'Turkey',
        onSale: true,
    },

    {
        image: avgImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 29,
        title: 'Beef Shank',
        category: 'Beef',
    },

    {
        image: FoodImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 30,
        title: 'Chuck Steak',
        category: 'Chicken',
    },

    {
        image: NewImg3,
        price: 13.33,
        orgPrice: 14.33,
        id: 31,
        title: 'Skirt Steak',
        category: 'Turkey',
        onSale: true,
    },

    {
        image: NewImg7,
        price: 13.33,
        orgPrice: 14.33,
        id: 32,
        title: 'First Cut Pork Chop',
        category: 'Pork',
    },

    {
        image: NewImg8,
        price: 13.33,
        orgPrice: 14.33,
        id: 33,
        title: 'Fresh Turkey Wings',
        category: 'Turkey',
    },

    {
        image: avgImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 34,
        title: 'Beef Shank',
        category: 'Beef',
        onSale: true,
    },

    {
        image: FoodImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 35,
        title: 'Chuck Steak',
        category: 'Chicken',
    },

    {
        image: NewImg3,
        price: 13.33,
        orgPrice: 14.33,
        id: 36,
        title: 'Skirt Steak',
        category: 'Turkey',
    },

    {
        image: NewImg7,
        price: 13.33,
        orgPrice: 14.33,
        id: 37,
        title: 'First Cut Pork Chop',
        category: 'Pork',
        onSale: true,
    },

    {
        image: NewImg8,
        price: 13.33,
        orgPrice: 14.33,
        id: 38,
        title: 'Fresh Turkey Wings',
        category: 'Turkey',
    },
]

const NewMeatData = [
    {
        image: FoodImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 39,
        title: 'Chicken Wings Case',
        category: 'Chicken',
    },

    {
        image: NewImg8,
        price: 13.33,
        orgPrice: 14.33,
        id: 40,
        title: 'Ground Chicken Gizzards',
        category: 'Turkey',
        onSale: true,
    },
    {
        image: NewImg7,
        price: 13.33,
        orgPrice: 14.33,
        id: 41,
        title: 'First Cut Pork Chop',
        category: 'Pork',
    },

    {
        image: NewImg3,
        price: 13.33,
        orgPrice: 14.33,
        id: 42,
        title: 'Fresh Turkey Wings',
        category: 'Turkey',
    },

    {
        image: avgImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 43,
        title: 'Beef Shank',
        category: 'Beef',
        onSale: true,
    },

    {
        image: FoodImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 44,
        title: 'Chicken Wings Case',
        category: 'Chicken',
    },

    {
        image: NewImg8,
        price: 13.33,
        orgPrice: 14.33,
        id: 45,
        title: 'Ground Chicken Gizzards',
        category: 'Turkey',
        onSale: true,
    },

    {
        image: NewImg7,
        price: 13.33,
        orgPrice: 14.33,
        id: 46,
        title: 'First Cut Pork Chop',
        category: 'Pork',
    },

    {
        image: NewImg3,
        price: 13.33,
        orgPrice: 14.33,
        id: 47,
        title: 'Fresh Turkey Wings',
        category: 'Turkey',
    },

    {
        image: avgImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 48,
        title: 'Beef Shank',
        category: 'Beef',
        onSale: true,
    },

    {
        image: FoodImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 49,
        title: 'Chicken Wings Case',
        category: 'Chicken',
    },

    {
        image: NewImg8,
        price: 13.33,
        orgPrice: 14.33,
        id: 50,
        title: 'Ground Chicken Gizzards',
        category: 'Turkey',
    },

    {
        image: NewImg7,
        price: 13.33,
        orgPrice: 14.33,
        id: 51,
        title: 'First Cut Pork Chop',
        category: 'Pork',
        onSale: true,
    },

    {
        image: NewImg3,
        price: 13.33,
        orgPrice: 14.33,
        id: 52,
        title: 'Fresh Turkey Wings',
        category: 'Turkey',
    },
]

const TopSellingData = [
    {
        image: FoodImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 53,
        title: 'Boneless Chicken Breast',
        category: 'Chicken',
        onSale: true,
    },

    {
        image: avgImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 54,
        title: 'Leg Quarters',
        category: 'Turkey',
    },
    {
        image: NewImg3,
        price: 13.33,
        orgPrice: 14.33,
        id: 55,
        title: 'First Cut Pork Chop',
        category: 'Pork',
    },

    {
        image: NewImg7,
        price: 13.33,
        orgPrice: 14.33,
        id: 56,
        title: 'Fresh Turkey Wings',
        category: 'Turkey',
        onSale: true,
    },

    {
        image: NewImg8,
        price: 13.33,
        orgPrice: 14.33,
        id: 57,
        title: 'Beef Shank',
        category: 'Beef',
    },

    {
        image: FoodImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 58,
        title: 'Boneless Chicken Breast',
        category: 'Chicken',
        onSale: true,
    },

    {
        image: avgImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 59,
        title: 'Leg Quarters',
        category: 'Turkey',
    },

    {
        image: NewImg3,
        price: 13.33,
        orgPrice: 14.33,
        id: 60,
        title: 'First Cut Pork Chop',
        category: 'Pork',
        onSale: true,
    },

    {
        image: NewImg7,
        price: 13.33,
        orgPrice: 14.33,
        id: 61,
        title: 'Fresh Turkey Wings',
        category: 'Turkey',
    },

    {
        image: NewImg8,
        price: 13.33,
        orgPrice: 14.33,
        id: 62,
        title: 'Beef Shank',
        category: 'Beef',
    },

    {
        image: FoodImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 63,
        title: 'Boneless Chicken Breast',
        category: 'Chicken',
        onSale: true,
    },

    {
        image: avgImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 64,
        title: 'Leg Quarters',
        category: 'Turkey',
    },

    {
        image: NewImg3,
        price: 13.33,
        orgPrice: 14.33,
        id: 65,
        title: 'First Cut Pork Chop',
        category: 'Pork',
        onSale: true,
    },

    {
        image: NewImg7,
        price: 13.33,
        orgPrice: 14.33,
        id: 66,
        title: 'Fresh Turkey Wings',
        category: 'Turkey',
    },
]

const RecommendedData = [
    {
        image: FoodImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 67,
        title: 'Boneless Chicken Breast',
        category: 'Chicken',
        onSale: true,
    },

    {
        image: NewImg8,
        price: 13.33,
        orgPrice: 14.33,
        id: 68,
        title: 'Leg Quarters',
        category: 'Turkey',
    },
    {
        image: NewImg7,
        price: 13.33,
        orgPrice: 14.33,
        id: 69,
        title: 'First Cut Pork Chop',
        category: 'Pork',
    },

    {
        image: NewImg3,
        price: 13.33,
        orgPrice: 14.33,
        id: 70,
        title: 'Fresh Turkey Wings',
        category: 'Turkey',
        onSale: true,
    },

    {
        image: avgImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 71,
        title: 'Beef Shank',
        category: 'Beef',
    },

    {
        image: FoodImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 72,
        title: 'Boneless Chicken Breast',
        category: 'Chicken',
        onSale: true,
    },

    {
        image: NewImg8,
        price: 13.33,
        orgPrice: 14.33,
        id: 73,
        title: 'Leg Quarters',
        category: 'Turkey',
    },

    {
        image: NewImg7,
        price: 13.33,
        orgPrice: 14.33,
        id: 74,
        title: 'First Cut Pork Chop',
        category: 'Pork',
    },

    {
        image: NewImg3,
        price: 13.33,
        orgPrice: 14.33,
        id: 75,
        title: 'Fresh Turkey Wings',
        category: 'Turkey',
        onSale: true,
    },

    {
        image: avgImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 76,
        title: 'Beef Shank',
        category: 'Beef',
    },

    {
        image: FoodImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 77,
        title: 'Boneless Chicken Breast',
        category: 'Chicken',
    },

    {
        image: NewImg8,
        price: 13.33,
        orgPrice: 14.33,
        id: 78,
        title: 'Leg Quarters',
        category: 'Turkey',
        onSale: true,
    },

    {
        image: NewImg7,
        price: 13.33,
        orgPrice: 14.33,
        id: 79,
        title: 'First Cut Pork Chop',
        category: 'Pork',
    },

    {
        image: NewImg3,
        price: 13.33,
        orgPrice: 14.33,
        id: 80,
        title: 'Fresh Turkey Wings',
        category: 'Turkey',
    },
]
export default Data
export {
    CardData,
    ShopData,
    ShopData1,
    ShopData2,
    DataSlider,
    MostPopularData,
    NewMeatData,
    TopSellingData,
    RecommendedData,
}
