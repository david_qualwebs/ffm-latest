import React from 'react'
import { NavLink } from 'react-router-dom'
import OwlCarousel from 'react-owl-carousel'
import 'owl.carousel/dist/assets/owl.carousel.css'
import 'owl.carousel/dist/assets/owl.theme.default.css'
import { DataSlider } from './Data'
import FoodCard from '../../Common/FoodCard'

const Featured: React.FC = () => {
    return (
        <React.Fragment>
            <div className="col-12 col-md-12 col-lg-12 col-xl-12 p-0">
                <div className="title">
                    <h2>Featured Favorites</h2>
                    <NavLink exact to="/AllProduct" className="more-info">
                        See more{' '}
                    </NavLink>
                </div>
            </div>
            <div className="col-12 col-md-12 col-lg-12 mb-m p-0">
                <OwlCarousel
                    className="owl-theme owl-carousel"
                    /*loop*/ margin={10}
                    nav={true}
                    items={5}
                >
                    {DataSlider.map((val, index) => {
                        return <FoodCard key={index} val={val} />
                    })}
                </OwlCarousel>
            </div>
        </React.Fragment>
    )
}

export default Featured
