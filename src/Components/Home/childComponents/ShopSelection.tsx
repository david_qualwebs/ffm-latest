import React from 'react'
import { ShopData, ShopData1, ShopData2 } from './Data'
import ShopCard from '../../Common/ShopCard'

const ShopSelection = () => {
    return (
        <React.Fragment>
            <div className="col-12 col-md-6 col-lg-4 col-xl-4">
                <div className="shop-details">
                    <h2>Shop Selection</h2>

                    {/**********Food Details************/}

                    {ShopData.map((val, index) => {
                        return <ShopCard key={index} val={val} />
                    })}
                    {/**********Food Details************/}
                </div>
                <div className="feature-details m-0">
                    <a href="#" className="more-info">
                        See more{' '}
                    </a>
                </div>
            </div>
            <div className="col-12 col-md-6 col-lg-4 col-xl-4">
                <div className="shop-details">
                    <h2>Frequent Bought</h2>
                    {/**********Food Details************/}

                    {ShopData1.map((val, index) => {
                        return <ShopCard key={index} val={val} />
                    })}
                    {/**********Food Details************/}
                </div>
                <div className="feature-details m-0">
                    <a href="#" className="more-info">
                        See more{' '}
                    </a>
                </div>
            </div>
            <div className="col-12 col-md-6 col-lg-4 col-xl-4">
                <div className="shop-details">
                    <h2>Delicious Deals</h2>
                    {ShopData2.map((val, index) => {
                        return <ShopCard key={index} val={val} />
                    })}
                </div>
                <div className="feature-details m-0">
                    <a href="#" className="more-info">
                        See more{' '}
                    </a>
                </div>
            </div>
        </React.Fragment>
    )
}

export default ShopSelection
