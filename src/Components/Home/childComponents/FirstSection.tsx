import React from 'react'
import Data from './Data'
import FoodCard from '../../Common/FoodCard'
import { NavLink } from 'react-router-dom'

const FirstSection: React.FC = () => {
    return (
        <React.Fragment>
            <div className="row display-details mb-m">
                {Data.map((val, index) => {
                    return <FoodCard key={index} val={val} />
                })}
            </div>
        </React.Fragment>
    )
}

export default FirstSection
