import React from 'react'
import OwlCarousel from 'react-owl-carousel'
import 'owl.carousel/dist/assets/owl.carousel.css'
import 'owl.carousel/dist/assets/owl.theme.default.css'
import { TopSellingData } from './Data'
import FoodCard from '../../Common/FoodCard'
import { NavLink } from 'react-router-dom'

const TopSelling: React.FC = () => {
    return (
        <React.Fragment>
            <div className="row">
                <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                    <div className="title">
                        <h2>Top Selling This Week</h2>
                        <NavLink exact to="/AllProduct" className="more-info">
                            See more{' '}
                        </NavLink>
                    </div>
                </div>
                <div className="col-12 col-md-12 col-lg-12 mb-l">
                    <OwlCarousel
                        className="owl-theme owl-carousel"
                        loop={false}
                        margin={10}
                        nav
                        items={5}
                    >
                        {TopSellingData.map((val, index) => {
                            return <FoodCard key={index} val={val} />
                        })}
                    </OwlCarousel>
                </div>
            </div>
        </React.Fragment>
    )
}

export default TopSelling
