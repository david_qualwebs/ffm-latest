import React, { useState, useEffect } from 'react'

const useGeoLocation = () => {
    const [location, setLocation] = useState<any>({
        loaded: false,
        coordinates: { lat: null, lng: null },
    })

    const onSuccess = (location: {
        coords: { latitude: any; longitude: any }
    }) => {
        setLocation({
            loaded: true,
            coordinates: {
                lat: location.coords.latitude,
                lng: location.coords.longitude,
            },
        })
    }

    const onError = (error: { code: any; message: any }) => {
        setLocation({
            loaded: false,
            error: {
                code: error.code,
                message: error.message,
            },
        })
    }

    useEffect(() => {
        if (!('geolocation' in navigator)) {
            onError({
                code: 0,
                message: 'Geolocation not supported',
            })
        }

        navigator.geolocation.getCurrentPosition(onSuccess, onError)
    }, [])

    return location
}

export default useGeoLocation
