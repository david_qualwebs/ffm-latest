import React, { useContext } from 'react'
import { stateContext } from '../../../GlobalState/Context'
import '../../../assets/css/List.css'

const AddToList: React.FC = () => {
    const { state, dispatch } = useContext(stateContext)

    const handleButton: Function = () => {
        state.listItems.map((val: any) => {
            return dispatch({ type: 'ADD_ITEM', payload: val })
        })
    }

    const RemoveButton: Function = () => {
        state.listItems.map((val: any) => {
            return dispatch({ type: 'REMOVE_LIST', payload: val })
        })
        dispatch({ type: 'isAdded', payload: 0 })
        dispatch({ type: 'isEmpty', payload: 0 })
        dispatch({ type: 'listOpen', payload: 0 })
    }

    return (
        <React.Fragment>
            <i
                className="fas fa-window-close"
                onClick={() => {
                    dispatch({ type: 'listOpen', payload: 0 })
                }}
                style={{ marginLeft: '90%' }}
            ></i>
            {state.isEmpty === 1 ? (
                state.listItems.map((val: any) => {
                    return (
                        <>
                            <div className="main_List" key={val.id}>
                                <div className="imgList">
                                    <img
                                        src={val.image}
                                        className="img-fluid"
                                        alt="Not Found"
                                    />
                                </div>
                                <div className="">
                                    <h6 className="listText">
                                        USDA Primen Angus Boneless New York
                                        Stripe
                                    </h6>
                                </div>
                            </div>
                        </>
                    )
                })
            ) : (
                <h1>List is Empty</h1>
            )}

            {state.isEmpty === 1 ? (
                <div className="buttons">
                    {state.counter === 0 ? (
                        <button
                            className="cart-button"
                            onClick={() => handleButton()}
                        >
                            <span className="add-to-cart">Add to cart</span>
                        </button>
                    ) : (
                        <button className="cart-button">
                            <span className="add-to-cart">Added</span>
                        </button>
                    )}
                    <button
                        className="cart-button"
                        onClick={() => RemoveButton()}
                    >
                        <span className="add-to-cart">Remove All</span>
                    </button>
                </div>
            ) : null}
        </React.Fragment>
    )
}

export default AddToList
