import React from 'react'
import { NavLink } from 'react-router-dom'
import OwlCarousel from 'react-owl-carousel'
import 'owl.carousel/dist/assets/owl.carousel.css'
import 'owl.carousel/dist/assets/owl.theme.default.css'
import { MostPopularData } from './Data'
import FoodCard from '../../Common/FoodCard'

const MostPopular = () => {
    return (
        <React.Fragment>
            <div className="row">
                <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                    <div className="title">
                        <h2>Most Popular at Meat & Grocery</h2>
                        <NavLink exact to="/AllProduct" className="more-info">
                            See more{' '}
                        </NavLink>
                    </div>
                </div>
                <div className="col-12 col-md-12 col-lg-12 mb-l">
                    <OwlCarousel
                        className="owl-theme owl-carousel"
                        loop={false}
                        margin={10}
                        nav
                        items={5}
                    >
                        {MostPopularData.map((val, index) => {
                            return <FoodCard key={index} val={val} />
                        })}
                    </OwlCarousel>
                </div>
            </div>
        </React.Fragment>
    )
}

export default MostPopular
