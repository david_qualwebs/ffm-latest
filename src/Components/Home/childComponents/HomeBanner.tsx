import React from 'react'
import BannerCard from '../../Common/BannerCard'
import { CardData } from './Data'

const HomeBanner: React.FC = () => {
    return (
        <React.Fragment>
            <div className="row">
                {CardData.map((val, index) => {
                    return <BannerCard key={index} image={val.image} />
                })}
            </div>
        </React.Fragment>
    )
}

export default HomeBanner
