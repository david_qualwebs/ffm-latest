export const images = {
    imgP: require('../../assets/images/img5.png'),
    imgB: require('../../assets/images/img7.png'),
    dateIcon: require('../../assets/images/calendar.png'),
    img1: require('../../assets/images/img1.1.png'),
    logoImg: require('../../assets/images/logo2.png'),
    homeImg: require('../../assets/images/home.png'),
    userIcon: require('../../assets/images/user.png'),
    cartIcon: require('../../assets/images/cart.png'),
    googleIcon: require('../../assets/images/google.png'),
    VisaIcon: require('../../assets/images/visa1.png'),
    roadIcon: require('../../assets/images/road-1.png'),
    fbIcon: require('../../assets/images/fb.png'),
}

export const excludeFooter = ['/cart', '/checkout']

export const excludeHeader = ['/login', '/signup']
