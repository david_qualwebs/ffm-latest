export const formatNumber = (number: any) => {
    return new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
    }).format(number)
}

export const formatDistance = (number: any) => {
    return new Intl.NumberFormat('en-US').format(number)
}
