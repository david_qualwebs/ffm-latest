import React, { useContext } from 'react'
import SideBar from '../Common/Sidebar'
import FoodCard from '../Common/FoodCard'
import { stateContext } from '../../GlobalState/Context'
import CategoryData from '../Common/CategoryData'
import { Helmet } from 'react-helmet'

const SearchItem: React.FC = () => {
    const { state } = useContext(stateContext)

    return (
        <React.Fragment>
            <Helmet>
                <title>
                    {state.search} - Farmer&#039;s Fresh Meat: Houston Meat
                    Market &amp; Butcher Shop
                </title>
            </Helmet>

            <div className="container-fluid">
                <div className="row">
                    <SideBar />
                    <div className="col-12 col-md-10 col-lg-10 col-xl-9 column">
                        <div className="row display-details">
                            {CategoryData.filter((item: any) => {
                                if (state.search === '') {
                                    return item
                                } else if (
                                    item.title
                                        .toLowerCase()
                                        .includes(state.search.toLowerCase())
                                ) {
                                    return item
                                } else if (
                                    item.category
                                        .toLowerCase()
                                        .includes(state.search.toLowerCase())
                                ) {
                                    return item
                                }
                            }).map((item: any, index: number) => {
                                return <FoodCard key={index} val={item} />
                            })}
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default SearchItem
