import React, { useContext, useState, useEffect } from 'react';
import { images } from './Constant/Constant';
import Featured from './Home/childComponents/Featured';
import { stateContext } from '../GlobalState/Context';
import Notifications, { notify } from 'react-notify-toast';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import { Carousel } from 'react-responsive-carousel';
import Subscribe from './Common/Subscribe';
import BreadCrumb from './Common/BreadCrumb';
import Popover from '@material-ui/core/Popover';
import { Modal } from 'react-bootstrap'
import Tippy from "@tippyjs/react";
import OwlCarousel from "react-owl-carousel";
import {DataSlider} from "./Home/childComponents/Data";
import FoodCard from "./Common/FoodCard";

const SingleProduct: React.FC = () => {
    //const [btnVal, setBtnVal] = useState(0);
    const { state, dispatch } = useContext(stateContext);
    const [mod, setMod] = useState(0);
    const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);
    const [ show , setShow ] = useState(false);
    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;


    const handleButton = (val: any) => {  //Add to Cart
        dispatch({ type: 'ADD_ITEM', payload: val });

        if (val.category === 'Meat') {
            dispatch({ type: 'itemMeat' });
        }

        if (val.category === 'Turkey') {
            dispatch({ type: 'itemTurkey' });
        }
    };

    const decrement = (val: any) => {  // Decrement Function
        dispatch({ type: 'DECREASE', payload: val });
    };

    const increment = (val: any) => {  // Increment Function
        dispatch({ type: 'INCREASE', payload: val });
    };

    const handleList = (val: object) => { // Add to List
        notify.show('Added to List!', 'success');
        dispatch({ type: 'isAdded', payload: 1 });
        setMod(1);
        dispatch({ type: 'ADD_TO_LIST', payload: val });
        dispatch({ type: 'isEmpty', payload: 1 });
    };

    const copy = () => { // Share Url
        notify.show('Copied to clipboard!', 'success');
        const el = document.createElement('input');
        el.value = window.location.href;
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
    };

    const handleRemove = (val: any) => { //Remove Item
        dispatch({ type: 'REMOVE_STATE', payload: val });
    };

    useEffect(() => {
        localStorage.setItem(
            'singleProduct',
            JSON.stringify(state.singleProduct)
        )
    }, [state.singleProduct]);

    return (
        <React.Fragment>
            <Notifications />
            <div className="container container-margin">
                <div className="col-12">
                    {/*<SideBar />*/}

                    {/**************************/}
                    {state.singleProduct.map((val: any) => {
                        return (
                            <>
                                <div className="row">
                                    <BreadCrumb currentPage="SingleProduct" show={true} />
                                    <div className="col-12 col-md-7 col-lg-8 col-xl-9 mb-m">
                                        {/*************1*************/}
                                        <div className="bg-white mt-0 p-0 single-product main-section mb-0">
                                            <div
                                                className="product-details sale-banner"
                                                key={val.id}
                                            >
                                                <div className="onsale-details">
                                                    <span>On sale</span>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="image col-12 col-md-12 col-lg-5 col-xl-5">
                                                    <div
                                                        id="myCarousel"
                                                        className="carousel slide"
                                                    >
                                                        {/*<!-- main slider carousel items -->*/}

                                                        <div className="carousel-inner">
                                                            <Carousel>
                                                                <div
                                                                    className="active carousel-item"
                                                                    data-slide-number="0"
                                                                >
                                                                    <img
                                                                        src={
                                                                            val.image
                                                                        }
                                                                        className="img-fluid"
                                                                    />
                                                                </div>
                                                                <div /* className="carousel-item" data-slide-number="1" */
                                                                >
                                                                    <img
                                                                        src={
                                                                            val.image
                                                                        }
                                                                        className="img-fluid"
                                                                    />
                                                                </div>
                                                                <div /*className="carousel-item" data-slide-number="2"*/
                                                                >
                                                                    <img
                                                                        src={
                                                                            val.image
                                                                        }
                                                                        className="img-fluid"
                                                                    />
                                                                </div>
                                                            </Carousel>
                                                        </div>
                                                        {/*<!-- main slider carousel nav controls -->*/}

                                                        {/*<ul className="carousel-indicators list-inline mx-auto product-slider">
                                                            <li className={`list-inline-item ${active}`}>
                                                                <a id="carousel-selector-0" className="selected" data-slide-to="0" data-target="#myCarousel">
                                                                    <img src={val.image} />
                                                                </a>
                                                            </li>
                                                            <li className={`list-inline-item ${active}`} onClick={() => { handleClick(val.image1) }}>
                                                                <a id="carousel-selector-1" data-slide-to="1" data-target="#myCarousel">
                                                                    <img src={val.image1} />
                                                                </a>
                                                            </li>
                                                            <li className="list-inline-item">
                                                                <a id="carousel-selector-2" data-slide-to="2" data-target="#myCarousel">
                                                                    <img src={val.image} />
                                                                </a>
                                                            </li>
                                                            <li className="list-inline-item">
                                                                <a id="carousel-selector-2" data-slide-to="3" data-target="#myCarousel">
                                                                    <img src={val.image} />
                                                                </a>
                                                            </li>
                                                        </ul>
                                                          */}
                                                    </div>
                                                </div>
                                                <div className="right col-12 col-md-12 col-lg-7 col-xl-7">
                                                    <div className="single-pro product-details">
                                                        <div className="food-maintitle">
                                                            <h1>{val.title}</h1>
                                                        </div>
                                                        <div className="food-content">
                                                            <p className="weight text-left">
                                                                Weight: 3lb
                                                            </p>
                                                            <div className="label-img">
                                                                <Tippy content="Prime item">
                                                                    <img
                                                                        className="p"
                                                                        src={images.imgP.default}
                                                                        alt="Not Found"
                                                                    />
                                                                </Tippy>
                                                                <Tippy content="Bulk item">
                                                                    <img
                                                                        className="b"
                                                                        src={images.imgB.default}
                                                                        alt="Not Found"
                                                                    />
                                                                </Tippy>
                                                                {/*<img*/}
                                                                {/*    src={*/}
                                                                {/*        images*/}
                                                                {/*            .imgP*/}
                                                                {/*            .default*/}
                                                                {/*    }*/}
                                                                {/*    alt="Not Found"*/}
                                                                {/*/>*/}
                                                                {/*<img*/}
                                                                {/*    src={*/}
                                                                {/*        images*/}
                                                                {/*            .imgB*/}
                                                                {/*            .default*/}
                                                                {/*    }*/}
                                                                {/*    alt="Not Found"*/}
                                                                {/*/>*/}
                                                                <a className="icon-details-click" onClick={() => setShow(true)}>Icon details</a>
                                                            </div>
                                                            {/* <p className="price"><sup>$</sup><span>13</span><sup>33</sup> <del>$14.33</del></p> */}
                                                        </div>
                                                        <div className="row">
                                                            {/** */}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 block">
                                                    <h3>Description</h3>
                                                    <p className="description">
                                                        Ever heard of
                                                        the Beef Dino
                                                        Rib? Well it’s
                                                        cut from this
                                                        delicious
                                                        category of beef
                                                        ribs. This
                                                        succulent piece
                                                        of beef is a
                                                        must have at
                                                        every outdoor
                                                        BBQ event!
                                                        Meaty, Juicy,
                                                        and tender to
                                                        the bone! To
                                                        order entire
                                                        slab, make sure
                                                        to order at
                                                        least 5lbs as
                                                        each slab weighs
                                                        between 5 and 6
                                                        pounds.
                                                    </p>
                                                </div>
                                                <hr />
                                                <div className="col-12 block">
                                                    <h3>Category</h3>
                                                    <p>
                                                                <span className="tags">
                                                                    Best Sellers
                                                                </span>
                                                        <span className="tags">
                                                                    Turkey
                                                                </span>
                                                        <span className="tags">
                                                                    Wholesale
                                                                </span>
                                                    </p>
                                                </div>
                                                <div className="col-12 block">
                                                    <h3>Tags</h3>
                                                    <p>
                                                                <span className="tags">
                                                                    bulk
                                                                </span>
                                                        <span className="tags">
                                                                    Case
                                                                </span>
                                                        <span className="tags">
                                                                    fresh
                                                                </span>
                                                        <span className="tags">
                                                                    raw
                                                                </span>
                                                        <span className="tags">
                                                                    Turkey
                                                                </span>
                                                        <span className="tags">
                                                                    uncooked
                                                                </span>
                                                        <span className="tags">
                                                                    wholesale
                                                                </span>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        {/**************1111************/}
                                        {/* <Featured /> */}
                                    </div>
                                    {/*****RIGHT****/}
                                    <div className="col-12 col-md-5 col-lg-4 col-xl-3 single-pro-right-main">
                                        <div className="col-12 bg-white main-section single-pro-right">
                                            <p className="price">
                                                <sup>$</sup>
                                                <span>13.</span>
                                                <sup>33</sup> <del>$14.33</del>
                                            </p>
                                            <h6 className="sub-text">Estimated package weight: 1.25 lbs each</h6>
                                            <div className="col-12 btn-div">
                                                <div className="buttons text-center">
                                                    {state.cartItems.find(
                                                        (item: { id: any }) =>
                                                            item.id === val.id
                                                    ) ? (
                                                        <button className="btn btn-m cart-button">
                                                            <>
                                                                {state.cartItems
                                                                    .filter(
                                                                        (item: {
                                                                            id: any
                                                                        }) =>
                                                                            item.id ===
                                                                            val.id
                                                                    )
                                                                    .map(
                                                                        (
                                                                            val: any
                                                                        ) => {
                                                                            return (
                                                                                <div
                                                                                    className="added"
                                                                                    key={
                                                                                        val.id
                                                                                    }
                                                                                >
                                                                                    <span
                                                                                        className="minus"
                                                                                        onClick={() => {
                                                                                            decrement(
                                                                                                val
                                                                                            )
                                                                                        }}
                                                                                    >
                                                                                        <i className="fa fa-minus"></i>
                                                                                    </span>
                                                                                    <span>
                                                                                        {
                                                                                            val.quantity
                                                                                        }
                                                                                        &nbsp;added
                                                                                    </span>
                                                                                    <span
                                                                                        className="plus"
                                                                                        onClick={() => {
                                                                                            increment(
                                                                                                val
                                                                                            )
                                                                                        }}
                                                                                    >
                                                                                        <i className="fa fa-plus"></i>
                                                                                    </span>
                                                                                    {val.quantity ===
                                                                                    0
                                                                                        ? handleRemove(
                                                                                              val
                                                                                          )
                                                                                        : null}
                                                                                </div>
                                                                            )
                                                                        }
                                                                    )}
                                                            </>
                                                        </button>
                                                    ) : (
                                                        <button
                                                            className="btn btn-m cart-button"
                                                            onClick={() =>
                                                                handleButton(
                                                                    val
                                                                )
                                                            }
                                                        >
                                                            <span className="add-to-cart cart1">
                                                                Add to cart
                                                            </span>
                                                        </button>
                                                    )}
                                                </div>
                                            </div>
                                            <div className="col-12 mt-3 mb-2 btn-div text-center">
                                                {mod === 0 ||
                                                state.isAdded === 0 ? (
                                                    <>
                                                    <button
                                                        className="btn btn-m btn-list"
                                                        onClick={(e) =>
                                                            handleClick(e)
                                                        }
                                                        aria-describedby={id}
                                                    >
                                                        Add to list
                                                    </button>
                                                        <Popover
                                                            id={id}
                                                            open={open}
                                                            anchorEl={anchorEl}
                                                            onClose={handleClose}
                                                            anchorOrigin={{
                                                                vertical: 'bottom',
                                                                horizontal: 'center',
                                                            }}
                                                            transformOrigin={{
                                                                vertical: 'top',
                                                                horizontal: 'center',
                                                            }}
                                                        >
                                                            <ul>
                                                                <li>My List</li>
                                                                <li onClick={() => handleList(val)}>Wish List</li>
                                                                <li>Manage My List</li>
                                                            </ul>
                                                        </Popover>
                                                    </>
                                                ) : (
                                                    <button className="btn btn-m btn-list">
                                                        Added
                                                    </button>
                                                )}
                                            </div>
                                            {/*<div className="col-12 mt-3 text-center">*/}
                                            {/*    <div className="shared-details"*/}
                                            {/*        onClick={() => {*/}
                                            {/*            copy()*/}
                                            {/*        }}*/}
                                            {/*    >*/}
                                            {/*        <h2><i className="fa fa-share-alt"></i> Share</h2>*/}
                                            {/*    </div>*/}
                                            {/*</div>*/}
                                        </div>
                                        <ul className="single-pro-options bg-white main-section">
                                            <li>
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                    <path fill="currentColor" d="M176 216h160c8.84 0 16-7.16 16-16v-16c0-8.84-7.16-16-16-16H176c-8.84 0-16 7.16-16 16v16c0 8.84 7.16 16 16 16zm-16 80c0 8.84 7.16 16 16 16h160c8.84 0 16-7.16 16-16v-16c0-8.84-7.16-16-16-16H176c-8.84 0-16 7.16-16 16v16zm96 121.13c-16.42 0-32.84-5.06-46.86-15.19L0 250.86V464c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V250.86L302.86 401.94c-14.02 10.12-30.44 15.19-46.86 15.19zm237.61-254.18c-8.85-6.94-17.24-13.47-29.61-22.81V96c0-26.51-21.49-48-48-48h-77.55c-3.04-2.2-5.87-4.26-9.04-6.56C312.6 29.17 279.2-.35 256 0c-23.2-.35-56.59 29.17-73.41 41.44-3.17 2.3-6 4.36-9.04 6.56H96c-26.51 0-48 21.49-48 48v44.14c-12.37 9.33-20.76 15.87-29.61 22.81A47.995 47.995 0 0 0 0 200.72v10.65l96 69.35V96h320v184.72l96-69.35v-10.65c0-14.74-6.78-28.67-18.39-37.77z"
                                                          className=""></path>
                                                </svg>
                                                <span>Email</span>
                                            </li>
                                            <li>
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                    <path fill="currentColor" d="M448 192V77.25c0-8.49-3.37-16.62-9.37-22.63L393.37 9.37c-6-6-14.14-9.37-22.63-9.37H96C78.33 0 64 14.33 64 32v160c-35.35 0-64 28.65-64 64v112c0 8.84 7.16 16 16 16h48v96c0 17.67 14.33 32 32 32h320c17.67 0 32-14.33 32-32v-96h48c8.84 0 16-7.16 16-16V256c0-35.35-28.65-64-64-64zm-64 256H128v-96h256v96zm0-224H128V64h192v48c0 8.84 7.16 16 16 16h48v96zm48 72c-13.25 0-24-10.75-24-24 0-13.26 10.75-24 24-24s24 10.74 24 24c0 13.25-10.75 24-24 24z"
                                                          className=""></path>
                                                </svg>
                                                <span>Print</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                {/*****RIGHT****/}
                            </>
                        )
                    })}
                    <Featured />
                    {/**************************/}
                    {/*<!------------you may also like to order--------------->*/}
                    <div className="row">
                        <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                            <div className="title">
                                <h2>Recently viewed items</h2>
                                <a className="more-info" href="#">
                                    See more
                                </a>
                            </div>
                        </div>
                        <div className="col-12 col-md-12 col-lg-12 mb-m complete-order">
                            <OwlCarousel
                                className="owl-theme owl-carousel"
                                loop={false}
                                margin={10}
                                nav
                                items={4}
                            >
                                {DataSlider.map((val, index) => {
                                    return (
                                        <FoodCard
                                            key={index}
                                            val={val}
                                            change="change"
                                        />
                                    )
                                })}
                            </OwlCarousel>
                        </div>
                    </div>
                    {/*<!------------/you may also like to order--------------->*/}
                </div>
                <Modal className="modal-info" show={show}>
                    <Modal.Body>
                        <div className="drop-modal">
                            <div className="order-prefrence"><h4>Icon details</h4></div>
                            <ul>
                                <li className="w-100">
                                    <img
                                        src={
                                            images
                                                .imgP
                                                .default
                                        }
                                        alt="Not Found"
                                    />
                                    <h6 className="text-uppercase">Prime
                                        <span>This item is available for bulk order.</span>
                                    </h6>
                                </li>
                                <li className="w-100">
                                    <img
                                        src={
                                            images
                                                .imgB
                                                .default
                                        }
                                        alt="Not Found"
                                    />
                                    <h6 className="text-uppercase">Bulk
                                        <span>This item is available for bulk order.</span>
                                    </h6>
                                </li>
                            </ul>
                            {/*<button onClick={()=>{setShow(false)}}>Ok</button>*/}
                            <div className="exit-details text-center">
                                <p onClick={()=>{setShow(false)}}><i className="fa fa-times"></i></p>
                            </div>
                        </div>
                    </Modal.Body>
                </Modal>
            </div>
            {/*<!-------------------container3------------------------>*/}
            <Subscribe />
            {/*<!-------------------/container3------------------------>*/}
        </React.Fragment>
    )
}

export default SingleProduct
