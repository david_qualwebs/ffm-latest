import React from 'react'

const Subscribe: React.FC = () => {
    return (
        <React.Fragment>

                <div
                    className="subscribe-info"
                    data-aos="zoom-in"
                    data-aos-easing="linear"
                >
                    <div className="container">
                        <div className="row">
                        <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                            <div className="col-12 subscribe-details">
                                <div className="row">
                                    <div className="col-md-5">
                                        <h4>Subscribe For Special Offers</h4>
                                        <p>‘Subscribe now and get offers on your first order’</p>
                                    </div>
                                    <form className="col-md-7 action">
                                        <div className="form-group">
                                            <input
                                                type="text"
                                                className="form-control"
                                                placeholder="Enter valid email address"
                                            />
                                        </div>
                                        <button className="btn btn-m btn-order">
                                            Subscribe now
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default Subscribe
