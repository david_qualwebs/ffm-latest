import React, { useState } from 'react'
import ReactPaginate from 'react-paginate'
import SideBar from './Sidebar'
import CategoryData from './CategoryData'
import FoodCard from './FoodCard'
import { Dropdown } from 'react-bootstrap'
import '../../assets/css/pagenation.css'
import BreadCrumb from './BreadCrumb'
import { Range } from 'rc-slider'
import 'rc-slider/assets/index.css'
import { Helmet } from 'react-helmet'

const AllProduct: React.FC = () => {
    const [data, setData] = useState(CategoryData)
    const [pageNumber, setPageNumber] = useState<any>(0)
    const [dropdown, setDropdown] = useState('')
    const [items, setItems] = useState(CategoryData)
    const [value, setValue] = useState([0, 50])
    //const createSliderWithTooltip = Slider.createSliderWithTooltip
    //const Range = createSliderWithTooltip(Slider.Range)
    let total = 0;

    const usersPerPage = 15;
    const pagesVisited = pageNumber * usersPerPage;

    const displayProduct = data
        .slice(pagesVisited, pagesVisited + usersPerPage)
        .map((item) => {
            return <FoodCard key={item.id} val={item} />
        })

    const pageCount = Math.ceil(data.length / usersPerPage);

    const changePage = ({ selected }: any) => {
        setPageNumber(selected);
    };

    const sortLowtoHigh = () => {
        let lowtohigh = data
            .sort((item1, item2) => {
                return item1.price - item2.price;
            })
            .filter((item) => {
                return item;
            })

        setData(lowtohigh);
        setDropdown('Price low to high');
    };

    const sortHightoLow = () => {
        let high = data
            .sort((item1, item2) => {
                return item2.price - item1.price
            })
            .filter((item) => {
                return item;
            })
        setData(high);
        setDropdown('Price high to low');
    };

    const handleClear = () => {
        let clear = items
            .sort(() => Math.random() - 0.5)
            .filter((item) => {
                return item;
            })
        setData(clear);
        setDropdown('');
    };

    const updateRange = (value: any) => {
        setValue(value);

        let range = items.filter((item) => {
            if (item.price <= value[1]) {
                total = total + 1;
                return item;
            }
        })
        setData(range);
    };

    const handleWays = (value:string) =>{  //Ways to Shop Delivery/PickUp 
        
        let selected = items.filter((item) => {
            if (item.shop === value) {
                total = total + 1;
                return item;
            }
        })
        setData(selected);
        
    };

    const handleBulkOrPacked = (value:string) =>{
        
        let selected = items.filter((item) => {
            if (item.type === value) {
                total = total + 1;
                return item;
            }
        })
        setData(selected);
    }; 

    return (
        <React.Fragment>
            <Helmet>
                <title>
                    Products Archive - Farmer&#039;s Fresh Meat: Houston Meat
                    Market &amp; Butcher Shop
                </title>
            </Helmet>

            <div className="container-fluid">
                <BreadCrumb currentPage={''} category={true} />
                <div className="row">
                    <SideBar />

                    <div className="col-12 col-md-10 col-lg-10 col-xl-9 column">
                        <div className="col-12 info-bar">
                            <div className="row">
                                <div className="col-md-6 left">
                                    <small>
                                        Showing {data.length} results
                                    </small>
                                </div>

                                <div className="col right">
                                    <span className="float-left">Sort by</span>
                                    <Dropdown>
                                        <Dropdown.Toggle
                                            variant=""
                                            id="dropdown-basic"
                                        >
                                            {dropdown
                                                ? dropdown
                                                : 'Default sorting'}
                                        </Dropdown.Toggle>

                                        <Dropdown.Menu>
                                            <Dropdown.Item
                                                onClick={() => handleClear()}
                                            >
                                                Default sorting
                                            </Dropdown.Item>
                                            <Dropdown.Item
                                                onClick={() => sortLowtoHigh()}
                                            >
                                                Price low to high
                                            </Dropdown.Item>
                                            <Dropdown.Item
                                                onClick={() => sortHightoLow()}
                                            >
                                                Price high to low
                                            </Dropdown.Item>
                                        </Dropdown.Menu>
                                    </Dropdown>
                                    {/*<small className="float-right" onClick={()=>handleClear()}> ClearAll </small>*/}
                                </div>
                            </div>
                        </div>
                        {/*<div style={{ width: '10vw' }} className="column1">
                            <h2>Price Range</h2>
                            <p>${value[0]}</p>
                            <p>${value[1]}</p>
                            <Range
                                min={0}
                                max={50}
                                onChange={updateRange}
                                defaultValue={[0, 50]}
                                value={value}
                            />
                        </div>*/}
                        <div className="row display-details">
                            {displayProduct}
                        </div>
                        {/*******Ways to shop********/}
                        {/* 
                        <div style={{ cursor: 'pointer', float: 'left' }}>
                            <h2> Ways to shop </h2>
                            <p onClick={() => handleWays('Delivery')}>Delivery</p>
                            <p onClick={() => handleWays('Pickup')}>Pick Up</p>
                            <p onClick={() => handleBulkOrPacked('Packed Item')}>Packed Items</p>
                            <p onClick={() => handleBulkOrPacked('Bulk Item')}>Bulk Items</p>
                        </div>
                        */}
                        {/******Ways to shop ***** */}
                    </div>
                </div>
                {/*<div className="pagination">*/}
                {/*    <ReactPaginate*/}
                {/*        previousLabel={'Previous'}*/}
                {/*        nextLabel={'Next'}*/}
                {/*        pageCount={pageCount}*/}
                {/*        onPageChange={changePage}*/}
                {/*        pageRangeDisplayed={pageCount}*/}
                {/*        marginPagesDisplayed={0}*/}
                {/*        containerClassName={'paginationBttns'}*/}
                {/*        previousLinkClassName={'previousBttn'}*/}
                {/*        nextLinkClassName={'nextBttn'}*/}
                {/*        disabledClassName={'paginationDisabled'}*/}
                {/*        activeClassName={'paginationActive'}*/}
                {/*    />*/}
                {/*</div>*/}
            </div>
        </React.Fragment>
    )
}

export default AllProduct
