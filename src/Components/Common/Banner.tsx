import React from 'react'

const Banner: React.FC = () => {
    return (
        <React.Fragment>
            <div
                className="banner-details"
                data-aos="zoom-in"
                data-aos-easing="linear"
            >
                <h1>Shop fresh at Fresh Meat & Grocery</h1>
                <p>Voted Houston’s Best Meat Market & Butcher Shop</p>
            </div>
        </React.Fragment>
    )
}

export default Banner
