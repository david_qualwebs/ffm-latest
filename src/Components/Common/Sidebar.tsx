import React, { useContext, useEffect, useState } from 'react';
import { images } from '../Constant/Constant';
import { NavLink, useLocation } from 'react-router-dom';
import CategoryData from './CategoryData';
import { stateContext,itemContext } from '../../GlobalState/Context';
import { Range } from 'rc-slider';
import 'rc-slider/assets/index.css';

const Sidebar: React.FC = () => {
    const { state, dispatch } = useContext(stateContext);
    const {storeItem, setStoreItem} = useContext(itemContext);
    const [value, setValue] = useState([0, 50]);
    const [data, setData] = useState(CategoryData);
    const [items, setItems] = useState(CategoryData);
    const location = useLocation();
    const exclude =['/'];
    //const checkedData:any = {};
    let beef = 0,
        Turkey = 0,
        chicken = 0,
        Pork = 0,
        LambGoat = 0,
        Deli = 0,
        HomemadeSausage = 0,
        Pantry = 0,
        FruitsVegetables = 0,
        WholesaleBulk = 0,
        total=0;

    {
        CategoryData.filter((val: any) => {
            if (val.category === 'Beef') {
                beef = beef + 1;
            }

            if (val.category === 'Chicken') {
                chicken = chicken + 1;
            }

            if (val.category === 'Pork') {
                Pork = Pork + 1;
            }

            if (val.category === 'Turkey') {
                Turkey = Turkey + 1;
            }

            if (val.category === 'Fruits & Vegetables') {
                FruitsVegetables = FruitsVegetables + 1;
            }

            if (val.category === 'Wholesale & Bulk') {
                WholesaleBulk = WholesaleBulk + 1;
            }

            if (val.category === 'Lamb & Goat') {
                LambGoat = LambGoat + 1;
            }

            if (val.category === 'Homemade Sausage') {
                HomemadeSausage = HomemadeSausage + 1;
            }

            if (val.category === 'Deli') {
                Deli = Deli + 1;
            }

            if (val.category === 'Pantry') {
                Pantry = Pantry + 1;
            }
        })
    }

    useEffect(() => {
        localStorage.setItem('category', JSON.stringify(state.category));
    }, [state.category]);

    const handleNavClick = (value: string) => {
        dispatch({
            type: 'category',
            payload: value,
        });
        dispatch({ type: 'prevPath', payload: location.pathname });
        dispatch({ type: 'showValue', payload: (state.showValue+1) });
    };


    const updateRange = (value: any) => {  ///RANGE FILTER FUNCTION
        setValue(value);

        let range = items.filter((item) => {
            if (item.price <= value[1] && item.price>=value[0]) {
                return item;
            }
        })
        setStoreItem(range);
        setData(range);
    };

    /*const handleWays = (value:string) =>{  //Ways to Shop Delivery/PickUp 
        
        let selected = items.filter((item) => {
            if (item.shop === value) {
                total = total + 1;
                return item;
            }
        })
        setData(selected);
        setStoreItem(selected);
    };*/

    /*const handleBulkOrPacked = (value:string) =>{
        
        let selected = items.filter((item) => {
            if (item.type === value) {
                total = total + 1;
                return item;
            }
        })
        setData(selected);
        setStoreItem(selected);
    };*/
    
    const handleDelivery = (e:any) =>{
        
        if(e.target.checked===true){
            let selected = items.filter((item) => {
                if (item.shop === 'Delivery') {
                    total = total + 1;
                    return item;
                }
            })
            console.log(selected);
            //checkedData.push(selected);
            //setCheckedData((prev:any)=>[...prev,selected]);
            setStoreItem((prev:any)=>[...prev,selected]);
        }else{
            setStoreItem(items);   
        }
    };

    const handlePickUp = (e:any) =>{
        if(e.target.checked===true){
            let selected = items.filter((item) => {
                if (item.shop === 'Pickup') {
                    total = total + 1;
                    return item;
                }
            })
            setStoreItem(selected);
        }else{
            setStoreItem(items);   
        }
    };

    const handlePacked = (e:any) =>{
        if(e.target.checked===true){
            let selected = items.filter((item) => {
                if (item.type === 'Packed Item') {
                    total = total + 1;
                    return item;
                }
            })
            setStoreItem(selected);
        }else{
            setStoreItem(items);   
        }
    };

    const handleBulk = (e:any) =>{
        if(e.target.checked===true){
            let selected = items.filter((item) => {
                if (item.type === 'Bulk Item') {
                    total = total + 1;
                    return item;
                }
            })
            setStoreItem(selected);
        }else{
            setStoreItem(items);   
        }
    };


    return (
        <React.Fragment>
            <div className="col-12 col-md-2 col-lg-2 col-xl-3 column1">
                <div className="sidebar d-md-none d-lg-block d-xl-block bg-white">
                    <h2>Category</h2>
                    <ul>
                        <li>
                            <NavLink
                                exact
                                to="/Beef"
                                activeStyle={{}}
                                onClick={() => {
                                    handleNavClick('Beef')
                                }}
                            >
                                Beef<span>{beef}</span>
                            </NavLink>
                        </li>
                        <li>
                            <NavLink
                                exact
                                to="/Chicken"
                                activeStyle={{}}
                                onClick={() => {
                                    handleNavClick('Chicken')
                                }}
                            >
                                Chicken<span>{chicken}</span>
                            </NavLink>
                        </li>
                        <li>
                            <NavLink
                                exact
                                to="/Pork"
                                activeStyle={{}}
                                onClick={() => {
                                    handleNavClick('Pork')
                                }}
                            >
                                Pork <span>{Pork}</span>
                            </NavLink>
                        </li>
                        <li>
                            <NavLink
                                exact
                                to="/Turkey"
                                activeStyle={{}}
                                onClick={() => {
                                    handleNavClick('Turkey')
                                }}
                            >
                                Turkey <span>{Turkey}</span>
                            </NavLink>
                        </li>
                        <li>
                            <NavLink
                                exact
                                to="/Lamb & Goat"
                                activeStyle={{}}
                                onClick={() => {
                                    handleNavClick('Lamb & Goat')
                                }}
                            >
                                Lamb & Goat<span>{LambGoat}</span>
                            </NavLink>
                        </li>

                        <li>
                            <NavLink
                                exact
                                to="/Deli"
                                activeStyle={{}}
                                onClick={() => {
                                    handleNavClick('Deli')
                                }}
                            >
                                Deli <span>{Deli}</span>
                            </NavLink>
                        </li>

                        <li>
                            <NavLink
                                exact
                                to="/Homemade Sausage"
                                activeStyle={{}}
                                onClick={() => {
                                    handleNavClick('Homemade Sausage')
                                }}
                            >
                                Homemade Sausage <span>{HomemadeSausage}</span>
                            </NavLink>
                        </li>

                        <li>
                            <NavLink
                                exact
                                to="/Pantry"
                                activeStyle={{}}
                                onClick={() => {
                                    handleNavClick('Pantry')
                                }}
                            >
                                Pantry <span>{Pantry}</span>
                            </NavLink>
                        </li>
                        <li>
                            <NavLink
                                exact
                                to="/Fruits & Vegetables"
                                activeStyle={{}}
                                onClick={() => {
                                    handleNavClick('Fruits & Vegetables')
                                }}
                            >
                                Fruits & Vegetables{' '}
                                <span>{FruitsVegetables}</span>
                            </NavLink>
                        </li>
                        <li>
                            <NavLink
                                exact
                                to="/Wholesale & Bulk"
                                activeStyle={{}}
                                onClick={() => {
                                    handleNavClick('Wholesale & Bulk')
                                }}
                            >
                                Wholesale & Bulk <span>{WholesaleBulk}</span>
                            </NavLink>
                        </li>
                    </ul>
                    {exclude.find((item)=>location.pathname===item)?null:
                        <>
                            <div className="col-md-12 filter" >
                                <h2>Price Range</h2>
                                <div className="range-slider">
                                    <p>${value[0]}</p>
                                    <p>${value[1]}</p>
                                </div>

                                <Range
                                    min={0}
                                    max={50}
                                    onChange={updateRange}
                                    defaultValue={[0, 50]}
                                    value={value}
                                />
                            </div>
                            <div className="col-md-12 filter">
                                <h2> Ways to shop </h2>
                                <h6 /*onClick={() => handleWays('Delivery')}*/>
                                    <label className="control control--checkbox">
                                        <img
                                            src={
                                                images
                                                    .imgB
                                                    .default
                                            }
                                            alt="Not Found"
                                        />
                                        <b> Delivery</b>
                                        <input type="checkbox" name="Delivery" id="1" onChange={(e)=>{handleDelivery(e)}}/>
                                        <div className="control__indicator"></div>
                                        <span>5</span>
                                    </label>
                                </h6>
                                <h6 /*onClick={() => handleWays('Pickup')}*/>
                                    <label className="control control--checkbox">
                                        <img
                                            src={
                                                images
                                                    .imgP
                                                    .default
                                            }
                                            alt="Not Found"
                                        />
                                        <b> Pick Up</b>
                                        <input type="checkbox" name="PickUp" id="2" onChange={(e)=>{handlePickUp(e)}} />
                                        <div className="control__indicator"></div>
                                        <span>5</span>
                                    </label>
                                </h6>
                                <h6 /*onClick={() => handleBulkOrPacked('Packed Item')}*/>
                                    <label className="control control--checkbox">
                                        <img
                                            src={
                                                images
                                                    .imgP
                                                    .default
                                            }
                                            alt="Not Found"
                                        />
                                        <b> Prime Items</b>
                                        <input type="checkbox" name="PackedItems" id="3" onChange={(e)=>{handlePacked(e)}} />
                                        <div className="control__indicator"></div>
                                        <span>5</span>
                                    </label>
                                </h6>
                                <h6 /*onClick={() => handleBulkOrPacked('Bulk Item')}*/>
                                    <label className="control control--checkbox">
                                        <img
                                            src={
                                                images
                                                    .imgB
                                                    .default
                                            }
                                            alt="Not Found"
                                        />
                                        <b> Bulk Items</b>
                                        <input type="checkbox" name="BulkItems" id="4" onChange={(e)=>{handleBulk(e)}} />
                                        <div className="control__indicator"></div>
                                        <span>5</span>
                                    </label>
                                </h6>
                            </div>
                        </>
                    }
                </div>
            </div>
        </React.Fragment>
    );
};

export default Sidebar;
