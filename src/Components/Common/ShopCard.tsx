import React, { useContext,useEffect } from 'react';
import { stateContext } from '../../GlobalState/Context';

interface props {
    val: {
        image: string
        price: number
        title: string
        category: string
        id: number
    }
}

const ShopCard: React.FC<props> = ({ val }) => {
    const { state, dispatch } = useContext(stateContext);
   
    useEffect(() => {
        localStorage.setItem('itemTurkey', JSON.stringify(state.itemTurkey));
        localStorage.setItem('itemBeef', JSON.stringify(state.itemBeef));
        localStorage.setItem('itemChicken', JSON.stringify(state.itemChicken));
        localStorage.setItem('itemPork', JSON.stringify(state.itemPork));
        localStorage.setItem('itemLambGoat', JSON.stringify(state.itemLambGoat));
        localStorage.setItem('itemDeli', JSON.stringify(state.itemDeli));
        localStorage.setItem('itemHomemadeSausage',JSON.stringify(state.itemHomemadeSausage));
        localStorage.setItem('itemPantry', JSON.stringify(state.itemPantry));
        localStorage.setItem('itemFruitsVegetables',JSON.stringify(state.itemFruitsVegetables));
        localStorage.setItem('itemWholesaleBulk',JSON.stringify(state.itemWholesaleBulk)); 
    }, [state.itemTurkey,
        state.itemBeef,
        state.itemChicken,
        state.itemPork,
        state.itemLambGoat,
        state.itemDeli,
        state.itemHomemadeSausage,
        state.itemPantry,
        state.itemFruitsVegetables]);

    const handleButton = (val: any) => {
        dispatch({ type: 'ADD_ITEM', payload: val });

        if (val.category === 'Meat') {
            dispatch({ type: 'itemMeat' });
        }

        if (val.category === 'Turkey') {
            dispatch({ type: 'itemTurkey' });
        }
 
        if (val.category === 'Beef') {
            dispatch({ type: 'itemBeef' });
        }

        if (val.category === 'Chicken') {
            dispatch({ type: 'itemChicken' });
        }

        if (val.category === 'Pork') {
            dispatch({ type: 'itemPork' });
        }

        if (val.category === 'Lamb & Goat') {
            dispatch({ type: 'itemLambGoat' });
        }

        if (val.category === 'Deli') {
            dispatch({ type: 'itemDeli' });
        }

        if (val.category === 'Homemade Sausage') {
            dispatch({ type: 'itemHomemadeSausage' });
        }

        if (val.category === 'Pantry') {
            dispatch({ type: 'itemPantry' });
        }

        if (val.category === 'Fruits & Vegetables') {
            dispatch({ type: 'itemFruitsVegetables' });
        }

        if (val.category === 'Wholesale & Bulk') {
            dispatch({ type: 'itemWholesaleBulk' });
        }
    }

    const decrement = (val: any) => {
        dispatch({ type: 'DECREASE', payload: val });
    }

    const increment = (val: any) => {
        dispatch({ type: 'INCREASE', payload: val });
    }

    const handleRemove = (val: any) => {
        dispatch({ type: 'REMOVE_STATE', payload: val });
        if (val.category === 'Turkey') {
            dispatch({ type: 'itemTurkeyDecrease' });
        }

        if (val.category === 'Beef') {
            dispatch({ type: 'itemBeefDecerease' });
        }

        if (val.category === 'Chicken') {
            dispatch({ type: 'itemChickenDecerease' });
        }

        if (val.category === 'Pork') {
            dispatch({ type: 'itemPorkDecerease' });
        }

        if (val.category === 'Lamb & Goat') {
            dispatch({ type: 'itemLambGoatDecerease' });
        }

        if (val.category === 'Deli') {
            dispatch({ type: 'itemDeliDecerease' });
        }

        if (val.category === 'Homemade Sausage') {
            dispatch({ type: 'itemHomemadeSausageDecerease' });
        }

        if (val.category === 'Pantry') {
            dispatch({ type: 'itemPantryDecerease' });
        }

        if (val.category === 'Fruits & Vegetables') {
            dispatch({ type: 'itemFruitsVegetablesDecerease' });
        }

        if (val.category === 'Wholesale & Bulk') {
            dispatch({ type: 'itemWholesaleBulkDecerease' });
        }
    }

    return (
        <React.Fragment>
            <div className="shop-border food-details mb-sm">
                <div className="shop-img">
                    <img src={val.image} alt="Not Found" />
                </div>
                <div className="shop-inner-details">
                    <a>
                        <h6>{val.title} (2lb)</h6>
                    </a>
                    <div className="row">
                        <div className="col-4 col-md-5 col-lg-5 col-xl-7 food-col">
                            <div className="food-price">
                                <p className="price"><sup></sup><span>$13.</span><sup>33 </sup>each
                                    <del>$14.33</del>
                                </p>
                            </div>
                        </div>

                        <div className="col-8 col-md-7 col-lg-7 col-xl-5 cart-col">
                            <div className="buttons">
                                {state.cartItems.find(
                                    (item: { id: any }) => item.id === val.id
                                ) ? (
                                    <button className="btn btn-sm cart-button cart1">
                                            {' '}
                                            {state.cartItems
                                                .filter(
                                                    (item: { id: any }) =>
                                                        item.id === val.id
                                                )
                                                .map((val: any) => {
                                                    return (
                                                        <span
                                                            className="added"
                                                            key={val.id}
                                                        >
                                                            <div className="num-block skin-5">
                                                                <div className="num-in">
                                                                    <span
                                                                        className="minus decrement dis"
                                                                        onClick={() => {
                                                                            decrement(
                                                                                val
                                                                            )
                                                                        }}
                                                                    >
                                                                        <i className="fa fa-minus"></i>
                                                                    </span>
                                                                    <input
                                                                        type="text"
                                                                        className="in-num"
                                                                        readOnly
                                                                        value={
                                                                            val.quantity
                                                                        }
                                                                        name="qty"
                                                                    />
                                                                    <span
                                                                        className="plus increment"
                                                                        onClick={() => {
                                                                            increment(
                                                                                val
                                                                            )
                                                                        }}
                                                                    >
                                                                        <i className="fa fa-plus"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            {val.quantity === 0
                                                                ? handleRemove(
                                                                      val
                                                                  )
                                                                : null}
                                                        </span>
                                                    )
                                                })}
                                    </button>
                                ) : (
                                    <button className="btn btn-sm cart-button cart1">
                                        <span
                                            className="add-to-cart"
                                            onClick={() => {
                                                handleButton(val)
                                            }}
                                        >Add to cart</span>
                                    </button>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default ShopCard;
