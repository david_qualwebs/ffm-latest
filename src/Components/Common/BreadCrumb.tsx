import React, { useContext, useState, useEffect } from 'react';
import { useLocation, NavLink, useHistory } from 'react-router-dom';
import { stateContext } from '../../GlobalState/Context';

interface Props {
    currentPage?: string
    show?: boolean
    category?:boolean
}

const BreadCrumb: React.FC<Props> = (props) => {
    const location = useLocation();
    const { state, dispatch } = useContext(stateContext);
    //const [val,setVal] = useState(state.showValue);
    // const location = useLocation();
    // const { state , dispatch } = useContext(stateContext);
    
    useEffect(() => {
        
    }, [state.showValue,state.prevPath]);

    return (
        <React.Fragment>
            <div className="col-md-12 ">
                <div className="breadcrumb-details">
                    <nav aria-label="breadcrumb">
                        {/*<h4>{location.pathname.split('/')[1]}</h4>*/}
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item">
                                <NavLink exact to="/" onClick={()=>{dispatch({type: 'prevPath',payload: '',})}}>
                                    {/*<i className="fas fa-home"></i>*/}
                                    Home
                                </NavLink>
                            </li>
                            {props.currentPage?<li className="breadcrumb-item"><a href="#">{props.currentPage}</a></li>:null}  
                            {state.prevPath === '' ?null:(
                                <li className="breadcrumb-item">
                                    <NavLink
                                        exact
                                        to={state.prevPath}
                                        onClick={() => {
                                            dispatch({
                                                type: 'prevPath',
                                                payload: '',
                                            })
                                        }}
                                    >
                                        {state.prevPath.split('/')[1]}
                                    </NavLink>
                                </li>
                            )}

                          {/* {props.currentPage?<li className="breadcrumb-item"><a href="#">{props.currentPage}</a></li>:null} */}
                            {props.show ? (
                                <li
                                    className="breadcrumb-item active"
                                    aria-current="page"
                                >
                                    <a>{state.link}</a>
                                </li>
                                ) : (
                                <li
                                    className="breadcrumb-item active"
                                    aria-current="page"
                                >
                                    <a>{location.pathname.split('/')[1]}</a>
                                </li>
                            )}
                        </ol>
                    </nav>
                </div>
            </div>
        </React.Fragment>
    );
};

export default BreadCrumb;
