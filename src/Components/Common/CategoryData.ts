import FoodImg from '../../assets/images/new-img2.png'
import Img3 from '../../../assets/images/new-img3.png'
import Img5 from '../../assets/images/new-img5.png'
import SliderImg from '../../assets/images/slider-img.png'
import NewImg3 from '../../assets/images/img3.png'
import avgImg from '../../assets/images/avg.png'
import NewImg8 from '../../assets/images/new-img8.png'
import Img2 from '../../assets/images/img2.png'
import homeImg4 from '../../../assets/images/home-img4.png'
import NewImg7 from '../../assets/images/new-img7.png'

const CategoryData = [
    {
        image: FoodImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 201,
        title: 'Boneless Chicken Breast',
        category: 'Beef',
        onSale: true,
        shop: 'Delivery',
        type:'Packed Item'
    },

    {
        image: SliderImg,
        price: 14.33,
        orgPrice: 15.33,
        id: 202,
        title: 'Leg Quarters',
        category: 'Pantry',
        shop: 'Pickup',
        type:'Bulk Item'
    },

    {
        image: NewImg3,
        price: 15.33,
        orgPrice: 16.33,
        id: 203,
        title: 'First Cut Pork Chop',
        category: 'Beef',
        shop: 'Delivery',
        type:'Packed Item'
    },

    {
        image: Img2,
        price: 16.33,
        orgPrice: 17.33,
        id: 204,
        title: 'Fresh Turkey Wings',
        category: 'Turkey',
        onSale: true,
        shop: 'Pickup',
        type:'Bulk Item'
    },

    {
        image: NewImg8,
        price: 17.33,
        orgPrice: 18.33,
        id: 205,
        title: 'Beef Shank',
        category: 'Chicken',
        shop: 'Delivery',
        type:'Packed Item'
    },

    {
        image: avgImg,
        price: 18.33,
        orgPrice: 19.33,
        id: 206,
        title: 'Lamb Chops',
        category: 'Chicken',
        shop: 'Pickup',
        type:'Packed Item'
    },

    {
        image: NewImg8,
        price: 19.33,
        orgPrice: 20.33,
        id: 207,
        title: 'Leg Quarters',
        category: 'Pantry',
        onSale: true,
        shop: 'Delivery',
        type:'Bulk Item'
    },
    {
        image: NewImg7,
        price: 15.33,
        orgPrice: 16.33,
        id: 501,
        title: 'Short Ribs',
        category: 'Beef',
        shop: 'Pickup',
        type:'Packed Item'
    },

    {
        image: NewImg3,
        price: 13.33,
        orgPrice: 14.33,
        id: 208,
        title: 'Fresh Turkey Wings',
        category: 'Turkey',
        onSale: true,
        shop: 'Delivery',
        type:'Packed Item'
    },

    {
        image: FoodImg,
        price: 17.33,
        orgPrice: 18.33,
        id: 209,
        title: 'Beef Shank',
        category: 'Pork',
        shop: 'Pickup',
        type:'Bulk Item'
    },

    {
        image: avgImg,
        price: 16.33,
        orgPrice: 17.33,
        id: 210,
        title: 'Lamb Chops',
        category: 'Beef',
        shop: 'Delivery',
        type:'Packed Item'
    },

    {
        image: NewImg8,
        price: 14.33,
        orgPrice: 15.33,
        id: 211,
        title: 'Leg Quarters',
        category: 'Wholesale & Bulk',
        onSale: true,
        shop: 'Pickup',
        type:'Bulk Item'
    },

    {
        image: NewImg7,
        price: 13.33,
        orgPrice: 14.33,
        id: 212,
        title: 'Short Ribs',
        category: 'Chicken',
        shop: 'Delivery',
        type:'Packed Item'
    },

    {
        image: NewImg3,
        price: 18.33,
        orgPrice: 19.33,
        id: 213,
        title: 'Fresh Turkey Wings',
        category: 'Turkey',
        shop: 'Pickup',
        type:'Packed Item'
    },

    {
        image: FoodImg,
        price: 17.33,
        orgPrice: 18.33,
        id: 214,
        title: 'Beef Shank',
        category: 'Beef',
        onSale: true,
        shop: 'Delivery',
        type:'Bulk Item'
    },

    {
        image: avgImg,
        price: 16.33,
        orgPrice: 17.33,
        id: 215,
        title: 'Lamb Chops',
        category: 'Chicken',
        shop: 'Pickup',
        type:'Packed Item'
    },

    {
        image: NewImg8,
        price: 13.33,
        orgPrice: 14.33,
        id: 216,
        title: 'Leg Quarters',
        category: 'Wholesale & Bulk',
        onSale: true,
        shop: 'Delivery',
        type:'Bulk Item'
    },

    {
        image: NewImg7,
        price: 18.33,
        orgPrice: 19.33,
        id: 217,
        title: 'Short Ribs',
        category: 'Beef',
        shop: 'Pickup',
        type:'Packed Item'
    },

    {
        image: NewImg3,
        price: 13.33,
        orgPrice: 14.33,
        id: 218,
        title: 'Fresh Turkey Wings',
        category: 'Turkey',
        onSale: true,
        shop: 'Delivery',
        type:'Bulk Item'
    },

    {
        image: FoodImg,
        price: 16.33,
        orgPrice: 17.33,
        id: 219,
        title: 'Chuck Steak',
        category: 'Lamb & Goat',
        onSale: true,
        shop: 'Pickup',
        type:'Packed Item'
    },

    {
        image: NewImg3,
        price: 13.33,
        orgPrice: 14.33,
        id: 220,
        title: 'Skirt Steak',
        category: 'Wholesale & Bulk',
        shop: 'Delivery',
        type:'Bulk Item'
    },
    {
        image: NewImg7,
        price: 14.33,
        orgPrice: 15.33,
        id: 221,
        title: 'First Cut Pork Chop',
        category: 'Pork',
        shop: 'Pickup',
        type:'Packed Item'
    },

    {
        image: NewImg8,
        price: 17.33,
        orgPrice: 18.33,
        id: 222,
        title: 'Fresh Turkey Wings',
        category: 'Turkey',
        onSale: true,
        shop: 'Delivery',
        type:'Packed Item'
    },

    {
        image: avgImg,
        price: 16.33,
        orgPrice: 17.33,
        id: 223,
        title: 'Beef Shank',
        category: 'Deli',
        shop: 'Pickup',
        type:'Bulk Item'
    },

    {
        image: FoodImg,
        price: 19.33,
        orgPrice: 20.33,
        id: 224,
        title: 'Chuck Steak',
        category: 'Beef',
        shop: 'Delivery',
        type:'Packed Item'
    },

    {
        image: NewImg3,
        price: 13.33,
        orgPrice: 14.33,
        id: 225,
        title: 'Skirt Steak',
        category: 'Wholesale & Bulk',
        onSale: true,
        shop: 'Pickup',
        type:'Bulk Item'
    },

    {
        image: NewImg7,
        price: 21.33,
        orgPrice: 22.33,
        id: 226,
        title: 'First Cut Pork Chop',
        category: 'Pork',
        shop: 'Delivery',
        type:'Packed Item'
    },

    {
        image: NewImg8,
        price: 17.33,
        orgPrice: 14.33,
        id: 227,
        title: 'Fresh Turkey Wings',
        category: 'Turkey',
        shop: 'Pickup',
        type:'Bulk Item'
    },

    {
        image: avgImg,
        price: 20.33,
        orgPrice: 21.33,
        id: 228,
        title: 'Beef Shank',
        category: 'Beef',
        onSale: true,
        shop: 'Delivery',
        type:'Packed Item'
    },

    {
        image: FoodImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 229,
        title: 'Chuck Steak',
        category: 'Chicken',
        shop: 'Pickup',
        type:'Bulk Item'
    },

    {
        image: NewImg3,
        price: 21.33,
        orgPrice: 22.33,
        id: 230,
        title: 'Skirt Steak',
        category: 'Wholesale & Bulk',
        shop: 'Delivery',
        type:'Packed Item'
    },

    {
        image: NewImg7,
        price: 19.33,
        orgPrice: 20.33,
        id: 231,
        title: 'First Cut Pork Chop',
        category: 'Homemade Sausage',
        onSale: true,
        shop: 'Pickup',
        type:'Bulk Item'
    },

    {
        image: NewImg8,
        price: 23.33,
        orgPrice: 24.33,
        id: 232,
        title: 'Fresh Turkey Wings',
        category: 'Turkey',
        shop: 'Delivery',
        type:'Packed Item'
    },

    {
        image: FoodImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 233,
        title: 'Chicken Wings Case',
        category: 'Chicken',
        shop: 'Pickup',
        type:'Bulk Item'
    },

    {
        image: NewImg8,
        price: 18.33,
        orgPrice: 19.33,
        id: 234,
        title: 'Ground Chicken Gizzards',
        category: 'Fruits & Vegetables',
        onSale: true,
        shop: 'Delivery',
        type:'Packed Item'
    },
    {
        image: NewImg7,
        price: 13.33,
        orgPrice: 14.33,
        id: 235,
        title: 'First Cut Pork Chop',
        category: 'Pork',
        shop: 'Pickup',
        type:'Bulk Item'
    },

    {
        image: NewImg3,
        price: 19.33,
        orgPrice: 20.33,
        id: 236,
        title: 'Fresh Turkey Wings',
        category: 'Turkey',
        shop: 'Delivery',
        type:'Packed Item'
    },

    {
        image: avgImg,
        price: 26.33,
        orgPrice: 27.33,
        id: 237,
        title: 'Beef Shank',
        category: 'Pantry',
        onSale: true,
        shop: 'Pickup',
        type:'Bulk Item'
    },

    {
        image: FoodImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 238,
        title: 'Chicken Wings Case',
        category: 'Pork',
        shop: 'Delivery',
        type:'Packed Item'
    },

    {
        image: NewImg8,
        price: 17.33,
        orgPrice: 18.33,
        id: 239,
        title: 'Ground Chicken Gizzards',
        category: 'Fruits & Vegetables',
        onSale: true,
        shop: 'Pickup',
        type:'Bulk Item'
    },

    {
        image: NewImg7,
        price: 19.33,
        orgPrice: 20.33,
        id: 240,
        title: 'First Cut Pork Chop',
        category: 'Pork',
        shop: 'Delivery',
        type:'Packed Item'
    },

    {
        image: NewImg3,
        price: 13.33,
        orgPrice: 14.33,
        id: 241,
        title: 'Fresh Turkey Wings',
        category: 'Turkey',
        shop: 'Pickup',
        type:'Bulk Item'
    },

    {
        image: avgImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 242,
        title: 'Beef Shank',
        category: 'Pork',
        onSale: true,
        shop: 'Delivery',
        type:'Packed Item'
    },

    {
        image: FoodImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 243,
        title: 'Chicken Wings Case',
        category: 'Deli',
        shop: 'Pickup',
        type:'Bulk Item'
    },

    {
        image: NewImg8,
        price: 16.33,
        orgPrice: 17.33,
        id: 244,
        title: 'Ground Chicken Gizzards',
        category: 'Fruits & Vegetables',
        shop: 'Delivery',
        type:'Packed Item'
    },

    {
        image: NewImg7,
        price: 13.33,
        orgPrice: 14.33,
        id: 245,
        title: 'First Cut Pork Chop',
        category: 'Wholesale & Bulk',
        onSale: true,
        shop: 'Pickup',
        type:'Bulk Item'
    },

    {
        image: NewImg3,
        price: 12.33,
        orgPrice: 13.33,
        id: 246,
        title: 'Fresh Turkey Wings',
        category: 'Turkey',
        shop: 'Delivery',
        type:'Packed Item'
    },

    {
        image: FoodImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 247,
        title: 'Boneless Chicken Breast',
        category: 'Lamb & Goat',
        onSale: true,
        shop: 'Pickup',
        type:'Bulk Item'
    },

    {
        image: avgImg,
        price: 26.33,
        orgPrice: 27.33,
        id: 248,
        title: 'Leg Quarters',
        category: 'Fruits & Vegetables',
        shop: 'Delivery',
        type:'Packed Item'
    },
    {
        image: NewImg3,
        price: 19.33,
        orgPrice: 20.33,
        id: 249,
        title: 'First Cut Pork Chop',
        category: 'Deli',
        shop: 'Pickup',
        type:'Bulk Item'
    },

    {
        image: NewImg7,
        price: 22.33,
        orgPrice: 23.33,
        id: 250,
        title: 'Fresh Turkey Wings',
        category: 'Turkey',
        onSale: true,
        shop: 'Delivery',
        type:'Packed Item'
    },

    {
        image: NewImg8,
        price: 14.33,
        orgPrice: 15.33,
        id: 251,
        title: 'Beef Shank',
        category: 'Deli',
        shop: 'Pickup',
        type:'Bulk Item'
    },

    {
        image: FoodImg,
        price: 12.33,
        orgPrice: 13.33,
        id: 252,
        title: 'Boneless Chicken Breast',
        category: 'Lamb & Goat',
        onSale: true,
        shop: 'Delivery',
        type:'Packed Item'
    },

    {
        image: avgImg,
        price: 13.33,
        orgPrice: 14.33,
        id: 253,
        title: 'Leg Quarters',
        category: 'Fruits & Vegetables',
        shop: 'Pickup',
        type:'Bulk Item'
    },

    {
        image: NewImg3,
        price: 28.33,
        orgPrice: 29.33,
        id: 254,
        title: 'First Cut Pork Chop',
        category: 'Pantry',
        onSale: true,
        shop: 'Delivery',
        type:'Packed Item'
    },

    {
        image: NewImg7,
        price: 19.33,
        orgPrice: 20.33,
        id: 255,
        title: 'Fresh Turkey Wings',
        category: 'Turkey',
        shop: 'Pickup',
        type:'Bulk Item'
    },

    {
        image: NewImg8,
        price: 13.33,
        orgPrice: 14.33,
        id: 256,
        title: 'Beef Shank',
        category: 'Homemade Sausage',
        shop: 'Delivery',
        type:'Packed Item'
    },

    {
        image: FoodImg,
        price: 17.33,
        orgPrice: 18.33,
        id: 257,
        title: 'Boneless Chicken Breast',
        category: 'Lamb & Goat',
        onSale: true,
        shop: 'Delivery',
        type:'Bulk Item'
    },

    {
        image: avgImg,
        price: 12.33,
        orgPrice: 13.33,
        id: 258,
        title: 'Leg Quarters',
        category: 'Turkey',
        shop: 'Pickup',
        type:'Packed Item'
    },

    {
        image: NewImg3,
        price: 31.33,
        orgPrice: 32.33,
        id: 259,
        title: 'First Cut Pork Chop',
        category: 'Fruits & Vegetables',
        onSale: true,
        shop: 'Delivery',
        type:'Bulk Item'
    },

    {
        image: NewImg7,
        price: 12.33,
        orgPrice: 13.33,
        id: 260,
        title: 'Fresh Turkey Wings',
        category: 'Pantry',
        shop: 'Pickup',
        type:'Packed Item'
    },

    {
        image: FoodImg,
        price: 19.33,
        orgPrice: 20.33,
        id: 261,
        title: 'Boneless Chicken Breast',
        category: 'Deli',
        onSale: true,
        shop: 'Delivery',
        type:'Bulk Item'
    },

    {
        image: NewImg8,
        price: 23.33,
        orgPrice: 24.33,
        id: 262,
        title: 'Leg Quarters',
        category: 'Turkey',
        shop: 'Pickup',
        type:'Packed Item'
    },
    {
        image: NewImg7,
        price: 24.33,
        orgPrice: 25.33,
        id: 263,
        title: 'First Cut Pork Chop',
        category: 'Lamb & Goat',
        shop: 'Delivery',
        type:'Bulk Item'
    },

    {
        image: NewImg3,
        price: 29.33,
        orgPrice: 30.33,
        id: 264,
        title: 'Fresh Turkey Wings',
        category: 'Turkey',
        onSale: true,
        shop: 'Pickup',
        type:'Packed Item'
    },

    {
        image: avgImg,
        price: 17.33,
        orgPrice: 18.33,
        id: 265,
        title: 'Beef Shank',
        category: 'Wholesale & Bulk',
        shop: 'Delivery',
        type:'Bulk Item'
    },

    {
        image: FoodImg,
        price: 12.33,
        orgPrice: 13.33,
        id: 266,
        title: 'Boneless Chicken Breast',
        category: 'Homemade Sausage',
        onSale: true,
        shop: 'Pickup',
        type:'Packed Item'
    },

    {
        image: NewImg8,
        price: 17.33,
        orgPrice: 18.33,
        id: 267,
        title: 'Leg Quarters',
        category: 'Pantry',
        shop: 'Delivery',
        type:'Bulk Item'
    },

    {
        image: NewImg7,
        price: 11.33,
        orgPrice: 12.33,
        id: 268,
        title: 'First Cut Pork Chop',
        category: 'Wholesale & Bulk',
        shop: 'Pickup',
        type:'Packed Item'
    },

    {
        image: NewImg3,
        price: 27.33,
        orgPrice: 28.33,
        id: 269,
        title: 'Fresh Turkey Wings',
        category: 'Turkey',
        onSale: true,
        shop: 'Delivery',
        type:'Bulk Item'
    },

    {
        image: avgImg,
        price: 26.33,
        orgPrice: 27.33,
        id: 270,
        title: 'Beef Shank',
        category: 'Homemade Sausage',
        shop: 'Pickup',
        type:'Packed Item'
    },

    {
        image: FoodImg,
        price: 22.33,
        orgPrice: 23.33,
        id: 271,
        title: 'Boneless Chicken Breast',
        category: 'Fruits & Vegetables',
        shop: 'Delivery',
        type:'Bulk Item'
    },

    {
        image: NewImg8,
        price: 11.33,
        orgPrice: 12.33,
        id: 272,
        title: 'Leg Quarters',
        category: 'Pantry',
        onSale: true,
        shop: 'Pickup',
        type:'Packed Item'
    },

    {
        image: NewImg7,
        price: 19.33,
        orgPrice: 20.33,
        id: 273,
        title: 'First Cut Pork Chop',
        category: 'Fruits & Vegetables',
        shop: 'Delivery',
        type:'Bulk Item'
    },

    {
        image: NewImg3,
        price: 13.33,
        orgPrice: 14.33,
        id: 274,
        title: 'Fresh Turkey Wings',
        category: 'Turkey',
        shop: 'Pickup',
        type:'Packed Item'
    },

    {
        image: NewImg8,
        price: 19.33,
        orgPrice: 20.33,
        id: 275,
        title: 'Leg Quarters',
        category: 'Pantry',
        onSale: true,
        shop: 'Delivery',
        type:'Bulk Item'
    },
]

const checkboxData = [{}];
export default CategoryData;
