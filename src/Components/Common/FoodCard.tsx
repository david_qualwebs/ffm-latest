import React, { useState, useContext, useEffect } from 'react';
import { images } from '../Constant/Constant';
import { stateContext } from '../../GlobalState/Context';
import { NavLink, useLocation } from 'react-router-dom';
import Notifications, { notify } from 'react-notify-toast';
import Tippy from '@tippyjs/react';
import 'tippy.js/dist/tippy.css';
import Popover from '@material-ui/core/Popover';

interface props {
    val: {
        image: string
        price: number
        orgPrice: number
        title: string
        category: string
        id: number
        onSale?: boolean
    }
    change?: string
};

const FoodCard: React.FC<props> = ({ val, change }) => {
    const { state, dispatch } = useContext(stateContext);
    const [mod, setMod] = useState(0);
    const [weight, setWeight] = useState(2);
    const [priceLb, setPriceLb] = useState(2);
    //const [checked, setChecked] = useState(false);
    const location = useLocation();

    const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);

    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;

    useEffect(() => {
        localStorage.setItem('items', JSON.stringify(state.cartItems));
        localStorage.setItem('price', JSON.stringify(state.price));
        localStorage.setItem('counter', JSON.stringify(state.counter));
        localStorage.setItem('Link', JSON.stringify(state.link));
    }, [state.cartItems, state.counter, state.link]);

    const handleButton = (val: any) => { //Add to Cart function
        dispatch({ type: 'ADD_ITEM', payload: val });

        if (val.category === 'Meat') {
            dispatch({ type: 'itemMeat' });
        }

        if (val.category === 'Turkey') {
            dispatch({ type: 'itemTurkey' });
        }

        if (val.category === 'Beef') {
            dispatch({ type: 'itemBeef' });
        }

        if (val.category === 'Chicken') {
            dispatch({ type: 'itemChicken' });
        }

        if (val.category === 'Pork') {
            dispatch({ type: 'itemPork' });
        }

        if (val.category === 'Lamb & Goat') {
            dispatch({ type: 'itemLambGoat' });
        }

        if (val.category === 'Deli') {
            dispatch({ type: 'itemDeli' });
        }

        if (val.category === 'Homemade Sausage') {
            dispatch({ type: 'itemHomemadeSausage' });
        }

        if (val.category === 'Pantry') {
            dispatch({ type: 'itemPantry' });
        }

        if (val.category === 'Fruits & Vegetables') {
            dispatch({ type: 'itemFruitsVegetables' });
        }

        if (val.category === 'Wholesale & Bulk') {
            dispatch({ type: 'itemWholesaleBulk' });
        }
    }

    const decrement = (val: any) => {   // Qty. decrement
        dispatch({ type: 'DECREASE', payload: val });
    };

    const increment = (val: any) => {  // Qty. increment
        dispatch({ type: 'INCREASE', payload: val });
    };

    const handleList = (val: object) => { // Add to List
       // notify.show('Added to List!', 'success');
        setMod(1);
        dispatch({ type: 'isAdded', payload: 1 });
        dispatch({ type: 'ADD_TO_LIST', payload: val });
        dispatch({ type: 'isEmpty', payload: 1 });
    };

    const handleProduct = (val: any) => { // Single Product
        dispatch({ type: 'link', payload: val.title });
        dispatch({ type: 'SINGLE_PRODUCT', payload: val });
        dispatch({ type: 'prevPath', payload: location.pathname });
    };

    const handleRemove = (val: any) => { // Remove Item
        dispatch({ type: 'REMOVE_STATE', payload: val });
        if (val.category === 'Turkey') {
            dispatch({ type: 'itemTurkeyDecrease' });
        }

        if (val.category === 'Beef') {
            dispatch({ type: 'itemBeefDecerease' });
        }

        if (val.category === 'Chicken') {
            dispatch({ type: 'itemChickenDecerease' });
        }

        if (val.category === 'Pork') {
            dispatch({ type: 'itemPorkDecerease' });
        }

        if (val.category === 'Lamb & Goat') {
            dispatch({ type: 'itemLambGoatDecerease' });
        }

        if (val.category === 'Deli') {
            dispatch({ type: 'itemDeliDecerease' });
        }

        if (val.category === 'Homemade Sausage') {
            dispatch({ type: 'itemHomemadeSausageDecerease' });
        }

        if (val.category === 'Pantry') {
            dispatch({ type: 'itemPantryDecerease' });
        }

        if (val.category === 'Fruits & Vegetables') {
            dispatch({ type: 'itemFruitsVegetablesDecerease' });
        }

        if (val.category === 'Wholesale & Bulk') {
            dispatch({ type: 'itemWholesaleBulkDecerease' });
        }
    };

    return (
        <React.Fragment>
            <div
                className="food-column"
                data-aos="zoom-in"
                data-aos-easing="linear"
            >
                <div className="product-details">
                    {val.onSale ? (
                        <div className="onsale-details">
                            <span>On sale</span>
                        </div>
                    ) : null}
                    <div className="top-img">
                        <Tippy content="Prime item">
                            <img
                                className="p"
                                src={images.imgP.default}
                                alt="Not Found"
                            />
                        </Tippy>
                        <Tippy content="Bulk item">
                            <img
                                className="b"
                                src={images.imgB.default}
                                alt="Not Found"
                            />
                        </Tippy>
                    </div>
                    <NavLink
                        exact
                        to={`/singleproduct/${val.title}`}
                        onClick={() => {
                            handleProduct(val);
                        }}
                    >
                    <div className="food-img">
                        <img
                            src={val.image}
                            className="img-fluid"
                            alt="Not Found"
                        />
                    </div>

                    <div className="food-content">
                        <h6>{val.title}</h6>
                         {/* </NavLink>* */}

                        {change ? null : (
                            <p className="weight">
                                Wt: {weight}lb <span>${priceLb}/lb</span>
                            </p>
                        )}
                        <p className="price">
                            <sup></sup>
                            <span>${Math.trunc(val.price)}.</span>
                            <sup>{val.price.toString().split('.')[1]} </sup>
                            each
                            <del>${val.orgPrice}</del>
                        </p>
                    </div>
                    </NavLink>
                    <div className="buttons">
                        {state.cartItems.find(
                            (item: { id: any }) => item.id === val.id
                        ) ? (
                            <button
                                className={`btn btn-m cart-button ${state.toggled}`}
                            >
                                <>
                                    {state.cartItems
                                        .filter(
                                            (item: { id: any }) =>
                                                item.id === val.id
                                        )
                                        .map((val: any) => {
                                            return (
                                                <div
                                                    className="added"
                                                    key={val.id}
                                                >
                                                        <span
                                                            className="minus"
                                                            onClick={() => {
                                                                decrement(val)
                                                            }}
                                                        >
                                                            <i className="fa fa-minus"></i>
                                                        </span>
                                                    <span>
                                                            {val.quantity}
                                                        &nbsp;added
                                                        </span>
                                                    <span
                                                        className="plus"
                                                        onClick={() => {
                                                            increment(val)
                                                        }}
                                                    >
                                                            <i className="fa fa-plus"></i>
                                                        </span>
                                                    {val.quantity === 0
                                                        ? handleRemove(val)
                                                        : null}
                                                </div>
                                            )
                                        })}
                                </>
                            </button>
                        ) : (
                            <button
                                className="btn btn-m cart-button menu-toggle"
                                onClick={() => handleButton(val)}
                            >
                                    <span className="add-to-cart">
                                        Add to cart
                                    </span>
                            </button>
                        )}
                    </div>
                    {change ? null : (
                        <div>
                            <button
                                className="btn btn-m btn-blank"
                                onClick={(e) =>
                                    handleClick(e)
                                }
                                aria-describedby={id}
                            >
                                {mod === 0 || state.isAdded === 0
                                    ? 'Add to list'
                                    : 'Added'}
                            </button>
                            <Popover
                                id={id}
                                open={open}
                                anchorEl={anchorEl}
                                onClose={handleClose}
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'center',
                                }}
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'center',
                                }}
                            >
                                <ul>
                                    <li>My List</li>
                                    <li onClick={() => handleList(val)}>Wish List</li>
                                    <li>Manage My List</li>
                                </ul>
                            </Popover>
                        </div>
                    )}
                    <p className="sub-order">Add to cart</p>
                </div>
            </div>
        </React.Fragment>
    )
}

export default FoodCard
