import React, { useState, useEffect } from 'react';
import GoogleLogin from 'react-google-login';
import { NavLink } from 'react-router-dom';
import { images } from '../Constant/Constant';
import { Helmet } from 'react-helmet'; 
import API from '../../API/api';
import Notifications, { notify } from 'react-notify-toast';

const api = new API();

const SignUp: React.FC = () => {
    const clientID =
        '282384162289-gp4ve43bm00n3hgu09keva40i6n44au6.apps.googleusercontent.com';

    useEffect(() => {
        $('#wrapper').removeClass('toggled');
        $('.container1').removeClass('open');
    }, []);

    const [data, setData] = useState({
        first_name: '',
        last_name: '',
        email: '',
        contact: '',
        password: '',
        confirmPassword: '',
    });
    const [ error, setError ] = useState('');
    /// Onchange function 
    const InputEvent = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;

        setData((prevVal) => {
            return {
                ...prevVal,
                [name]: value,
            }
        });
    };
   
    /// Sign Up click
    const submitForm = (e: React.FormEvent) => {
        e.preventDefault();
        
        if(data.first_name===''){
            setError('Enter First Name');
        }
        
        if(data.last_name===''){
            setError('Enter Last Name');
        }

        if(data.email===''){
            setError('Enter Email');
        }
       
        if(data.contact === ''||data.contact.length!=10){
            setError('Enter Contact No.');
        }
        
        if(data.password === ''){
            setError('Enter Password');
        }

        if(data.confirmPassword===''){
            setError('Enter Confirm Password');
        }
        if(data.password!==data.confirmPassword){
            setError('Password and Confirm Password not match');
        }

        if( error === '' ){
           api.register(data).then((res)=>{
              notify.show('Your account has been created successfully','success'); 
              console.log(res); 
           }).catch((error)=>{
               notify.show(`${error.response.data.message}`,'error');
               //console.log('error',);
           }); 
        }
        /*alert(`First Name = ${data.firname}
               Last Name = ${data.lname}
               Email = ${data.email}
               Contact = ${data.contact}
               Password = ${data.password}
               ConfirmPassword = ${data.confirmPassword} 
        `);*/
    };
    
    // Google Success Response
    const responseGoogleSuccess = (res: any) => {
        console.log(res.profileObj);
    };
    
    // Google Failure Response
    const responseGoogleFailure = (res: any) => {
        console.log(res);
    };

    return (
        <React.Fragment>
            <Notifications  options={{zIndex: 200, top: '90px'}}/>
            <Helmet>
                <title>
                    Sign Up - Farmer&#039;s Fresh Meat: Houston Meat Market
                    &amp; Butcher Shop
                </title> 
            </Helmet>
            <header className="header1">
                <div className="bg-white header">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                                <NavLink exact to="/">
                                    <div className="logo-col mx-auto">
                                        <img
                                            src={images.logoImg.default}
                                            className="img-fluid"
                                            alt="logo"
                                        />
                                    </div>
                                </NavLink>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <section className="main-height">
                <div className="container">
                    <div className="row">
                        <div className="col-12 col-md-4 col-lg-4 col-xl-5 login-column mx-auto">
                            <div className="signin-info text-center">
                                <h2>Create account</h2>
                                <p>Welcome to farmer’s fresh meat & grocery.</p>
                            </div>
                            <div className="bg-white main-section p-4">
                                <form>
                                    <div className="form-group">
                                        <label>First name</label>
                                        <input
                                            type="text"
                                            className="form-control info-control"
                                            id="first_name"
                                            placeholder="Enter here .."
                                            name="first_name"
                                            value={data.first_name}
                                            onChange={InputEvent}
                                        />
                                    </div>

                                    <div className="form-group">
                                        <label>Last name</label>
                                        <input
                                            type="text"
                                            className="form-control info-control"
                                            id="last_name"
                                            placeholder="Enter here .."
                                            name="last_name"
                                            value={data.last_name}
                                            onChange={InputEvent}
                                        />
                                    </div>

                                    <div className="form-group">
                                        <label>Contact number (optional)</label>
                                        <input
                                            type="tel"
                                            className="form-control info-control"
                                            id="contact"
                                            placeholder="Enter here .."
                                            name="contact"
                                            value={data.contact}
                                            onChange={InputEvent}
                                        />
                                    </div>

                                    <div className="form-group">
                                        <label>Email address</label>
                                        <input
                                            type="email"
                                            className="form-control info-control"
                                            id="email"
                                            placeholder="Enter here .."
                                            name="email"
                                            value={data.email}
                                            onChange={InputEvent}
                                        />
                                    </div>

                                    <div className="form-group">
                                        <label>Password</label>
                                        <input
                                            type="password"
                                            className="form-control info-control"
                                            id="password"
                                            placeholder="Enter here .."
                                            name="password"
                                            value={data.password}
                                            onChange={InputEvent}
                                        />
                                        <small>
                                            8 characters{' '}
                                            <span className="ml-3">
                                                No spaces
                                            </span>
                                        </small>
                                    </div> 

                                    <div className="form-group">
                                        <label>Confirm password</label>
                                        <input
                                            type="password"
                                            className="form-control info-control"
                                            id="confirm-password"
                                            placeholder="Enter here .."
                                            name="confirmPassword"
                                            value={data.confirmPassword}
                                            onChange={InputEvent}
                                        />
                                    </div>

                                    <div className="custom-check login-check mt-4">
                                        <label className="check ">
                                            <input
                                                type="checkbox"
                                                name="is_name1"
                                            />
                                            <span className="checkmark"></span>
                                            Send me emails for coupon, deals and
                                            more
                                        </label>
                                    </div>

                                    <div className="custom-check login-check">
                                        <label className="check ">
                                            <input 
                                                type="checkbox"
                                                name="is_name1"
                                            />
                                            <span className="checkmark"></span>I
                                            agree to the Farmer’s fresh meat
                                            <a href="#">Terms & Conditions </a>
                                            and
                                            <a href="#">Privacy Policy</a>
                                        </label>
                                    </div>

                                    <div
                                        className="update-btn"
                                        onClick={submitForm}
                                    >
                                        <a
                                            href="#"
                                            className="btn btn-cart w-100"
                                        >
                                            Create account
                                        </a>
                                    </div>

                                    <div className="create-account text-center">
                                        <p>
                                            Already have an account?
                                            <NavLink exact to="/login">
                                                Sign in
                                            </NavLink>
                                        </p>
                                    </div>

                                    <div className="row kpx_row-sm-offset-3 kpx_loginOr">
                                        <div className="col-xs-12 col-xl-12">
                                            <hr className="kpx_hrOr" />
                                            <span className="kpx_spanOr">
                                                or
                                            </span>
                                        </div>
                                    </div>

                                    <div className="google-sign text-center">
                                        <h6 className="login-title">
                                            Continue with
                                        </h6>

                                        <GoogleLogin
                                            clientId={clientID}
                                            render={(renderProps) => (
                                                <div
                                                    className="google-button d-flex justify-content-center"
                                                    onClick={
                                                        renderProps.onClick
                                                    }
                                                >
                                                    <div className="google-icon">
                                                        <img
                                                            src={
                                                                images
                                                                    .googleIcon
                                                                    .default
                                                            }
                                                            alt="Google"
                                                        />
                                                    </div>

                                                    <div className="google-content">
                                                        <h6>
                                                            Sign in with google
                                                        </h6>
                                                    </div>
                                                </div>
                                            )}
                                            onSuccess={responseGoogleSuccess}
                                            onFailure={responseGoogleFailure}
                                            cookiePolicy={'single_host_origin'}
                                        />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <hr className="mb-0" />
        </React.Fragment>
    );
};

export default SignUp;
