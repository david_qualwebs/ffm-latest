import React, { useContext, useState, useEffect } from 'react';
import API from '../../API/index';
import PlacesAutocomplete, {
    geocodeByAddress,
    getLatLng,
} from 'react-places-autocomplete';
import { formatDistance } from '../Helpers/utils';
import { stateContext } from '../../GlobalState/Context';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';

const api = new API();

const Modals: React.FC = () => {
    const destination = new google.maps.LatLng(
        29.85250139999999,
        -95.25277410000001
    );
    const destination1 = new google.maps.LatLng(29.6672782, -95.35597050000001);
    const { state, dispatch } = useContext(stateContext);
    const [distance, setDistance] = useState(0);
    const [distance1, setDistance1] = useState(0);
    const [address, setAddress] = useState('');
    const [show, setShow] = useState(0);
    const [coordinates, setCoordinates] = useState({
        lat: 0,
        lng: 0,
    });
    const [deliveryAddress, setDeliveryAddress] = useState('');
    const [deliveryArea, setDeliveryArea] = useState('');
    const [latitude, setLatitude] = useState(0);
    const [longitude, setLongitude] = useState(0);

    const handleActive = (val: any) => {
        if (val === 'Delivery') $('#home-tab').addClass('active')
        else {
            $('#home-tab').removeClass('active');
        }

        if (val === 'Pickup') $('#profile-tab').addClass('active')
        else {
            $('#profile-tab').removeClass('active');
        }
    }

    const handleSelect = async (value: any) => {
        const results = await geocodeByAddress(value);
        const latLng: any = await getLatLng(results[0]);
        setAddress(value);
        setCoordinates(latLng);
    };

    const handleSearch = () => {
        if (coordinates.lat !== 0) {
            setShow(1);
            const origin = new google.maps.LatLng(
                coordinates.lat,
                coordinates.lng
            );
            //console.log(origin)
            const dis = google.maps.geometry.spherical.computeDistanceBetween(
                origin,
                destination
            );
            const dis1 = google.maps.geometry.spherical.computeDistanceBetween(
                origin,
                destination1
            );
            setDistance((dis / 1000) * 0.62137);
            setDistance1((dis1 / 1000) * 0.62137);
        }
    }

    const handleCullen = () => {
        //dispatch({ type: "delivery_Price", payload: distance1 });
        //dispatch({type:"pickUp_Address",payload:"Cullen"})
        dispatch({ type: 'defaultPickUp', payload: 'Cullen' });
    }

    const handleMesa = () => {
        //dispatch({ type: "delivery_Price", payload: distance });
        // dispatch({type:"pickUp_Address",payload:"Mesa"});
        dispatch({ type: 'defaultPickUp', payload: 'Mesa' });
    }

    const handleDelivery = () => {
        api.getLocation(deliveryAddress).then((res: any) => {
            if (res) {
                setLatitude(res.data.results[0].geometry.location.lat);
                setLongitude(res.data.results[0].geometry.location.lng);
                const origin = new google.maps.LatLng(latitude, longitude);
                if (state.defaultPickUp === 'Mesa') {
                    const dis =
                        google.maps.geometry.spherical.computeDistanceBetween(
                            origin,
                            destination
                        );
                    dispatch({ type: 'deliveryDistance', payload: dis });
                } else {
                    const dis =
                        google.maps.geometry.spherical.computeDistanceBetween(
                            origin,
                            destination1
                        );
                    dispatch({ type: 'deliveryDistance', payload: dis });
                }
            }
        })
    };

    return (
        <React.Fragment>
            <div className="drop-modal">
                <div className="order-prefrence">
                    <h4>Order Preference</h4>
                    <p>
                        We need to know your order preference to show you the
                        current range of products.
                    </p>
                </div>
                {/* <div className="delivery-tabs"> */}
                <Tabs className="delivery-tabs">
                    <h6>Preference</h6>

                    {/* <ul className="nav nav-tabs border-0 food-details" id="myTab" role="tablist"> */}
                    <TabList className="nav nav-tabs border-0 food-details">
                        <Tab
                            className=" nav-item "
                            onClick={() => handleActive('Pickup')}
                        >
                            <p className="nav-link active" id="profile-tab">
                                Pickup
                            </p>
                        </Tab>
                        <Tab
                            className="nav-item "
                            onClick={() => handleActive('Delivery')}
                        >
                            <p className="nav-link " id="home-tab">
                                Delivery
                            </p>
                        </Tab>

                    </TabList>
                    <TabPanel>
                        <div
                            className="tab-pane fade show active"
                            id="home"
                            role="tabpanel"
                            aria-labelledby="home-tab"
                        >
                            {/**<form className="input-form"> **/}
                            <form className="input-form">
                                <div className="form-group">
                                    <label htmlFor="exampleInputEmail1">
                                        Search address
                                    </label>

                                    <PlacesAutocomplete
                                        value={address}
                                        onChange={setAddress}
                                        onSelect={handleSelect}
                                    >
                                        {({
                                              getInputProps,
                                              suggestions,
                                              getSuggestionItemProps,
                                              loading,
                                          }) => (
                                            <div>
                                                <div
                                                    className="position-relative"
                                                    style={{ display: 'flex' }}
                                                >
                                                    <input
                                                        className="form-control"
                                                        id="exampleInputEmail1"
                                                        aria-describedby="emailHelp"
                                                        {...getInputProps({
                                                            placeholder:
                                                                'Type address',
                                                        })}
                                                    />
                                                    <span
                                                        className="location-icon"
                                                        onClick={() =>
                                                            handleSearch()
                                                        }
                                                    >
                                                        {/*<i className="fas fa-directions"></i>*/}
                                                        Find store
                                                    </span>
                                                </div>
                                                <div className="location-suggestions">
                                                    {loading ? (
                                                        <div>...loading</div>
                                                    ) : null}

                                                    {suggestions.map(
                                                        (suggestion) => {
                                                            const style = {
                                                                backgroundColor:
                                                                    suggestion.active
                                                                        ? '#41b6e6'
                                                                        : '#fff',
                                                            }

                                                            return (
                                                                <div
                                                                    className="suggestion-items"
                                                                    {...getSuggestionItemProps(
                                                                        suggestion,
                                                                        {
                                                                            style,
                                                                        }
                                                                    )}
                                                                >
                                                                    {
                                                                        suggestion.description
                                                                    }
                                                                </div>
                                                            )
                                                        }
                                                    )}
                                                </div>
                                            </div>
                                        )}
                                    </PlacesAutocomplete>
                                </div>
                            </form>

                            {show === 1 ? (
                                <div className="card-location col-12 p-0">
                                    <div className="card-body p-0">
                                        {distance1 < distance ? (
                                            <div className="store-select">
                                                <div className="options">
                                                    <p className="card-title">
                                                        Farmer's fresh meat at
                                                        Cullen
                                                    </p>
                                                    <p className="distance">
                                                        Distance:{' '}
                                                        {formatDistance(
                                                            distance1
                                                        )}{' '}
                                                        miles
                                                    </p>
                                                    <button
                                                        onClick={() => {
                                                            handleCullen()
                                                        }}
                                                        className="cart-button"
                                                    >
                                                        Select
                                                    </button>
                                                </div>
                                                <div className="options">
                                                    <p className="card-title">
                                                        Farmer's fresh meat at
                                                        Mesa
                                                    </p>
                                                    <p className="distance">
                                                        Distance:{' '}
                                                        {formatDistance(
                                                            distance
                                                        )}{' '}
                                                        miles
                                                    </p>
                                                    <button
                                                        onClick={() => {
                                                            handleMesa()
                                                        }}
                                                        className="cart-button"
                                                    >
                                                        Select
                                                    </button>
                                                </div>
                                            </div>
                                        ) : null}
                                        {distance < distance1 ? (
                                            <div className="store-select">
                                                <div className="options">
                                                    <p className="card-title">
                                                        Farmer's fresh meat at
                                                        Mesa
                                                    </p>
                                                    <p className="distance">
                                                        distance:{' '}
                                                        {formatDistance(
                                                            distance
                                                        )}{' '}
                                                        miles
                                                    </p>
                                                    <button
                                                        onClick={() => {
                                                            handleMesa()
                                                        }}
                                                        className="cart-button"
                                                    >
                                                        Select
                                                    </button>
                                                </div>
                                                <div className="options">
                                                    <p className="card-title">
                                                        Farmer's fresh meat at
                                                        Cullen
                                                    </p>
                                                    <p className="distance">
                                                        distance:{' '}
                                                        {formatDistance(
                                                            distance1
                                                        )}{' '}
                                                        miles
                                                    </p>
                                                    <button
                                                        onClick={() => {
                                                            handleCullen()
                                                        }}
                                                        className="cart-button"
                                                    >
                                                        Select
                                                    </button>
                                                </div>
                                            </div>
                                        ) : null}
                                    </div>
                                </div>
                            ) : null}
                            {/*****</form>****/}
                        </div>
                    </TabPanel>
                    <TabPanel>
                        {/* <form className="input-form"> */}
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Zipcode</label>
                            <input
                                type="text"
                                className="form-control"
                                aria-describedby="emailHelp"
                                placeholder="Enter Zip Code/Address"
                                onChange={(e) =>
                                    setDeliveryAddress(e.target.value)
                                }
                            />
                        </div>
                        <h2></h2>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">
                                Delivery area
                            </label>
                            <input
                                type="text"
                                className="form-control"
                                aria-describedby="emailHelp"
                                placeholder="903, Market St., Houston, TX"
                                onChange={(e) =>
                                    setDeliveryArea(e.target.value)
                                }
                            />
                        </div>
                        <button
                            className="btn btn-m cart-button p-0"
                            onClick={() => {
                                handleDelivery()
                            }}
                        >
                          Save
                        </button>
                        {/* </form> */}
                    </TabPanel>
                    {/* </ul> */}
                </Tabs>

                {/* </div> */}
                <div className="map">
                    <p>Select a point on the map</p>
                </div>

                <div className="start-btn">
                    {/*<button className="btn">Start Shopping</button>*/}
                </div>

                <div className="exit-details text-center">
                    <p
                        /*href="#"*/ data-dismiss="modal"
                        onClick={() => {
                            dispatch({ type: 'MODAL', payload: false })
                        }}
                    >
                        <i className="fa fa-times"></i>
                    </p>
                </div>
            </div>
            {/*<!--------------------------/header dropdown modal-------------------------->*/}
        </React.Fragment>
    );
};

export default Modals;
