import React, { useContext, useEffect, useState } from 'react';
import { formatNumber } from '../Helpers/utils';
import $ from 'jquery';
import { NavLink, useLocation } from 'react-router-dom';
import { stateContext } from '../../GlobalState/Context';


const CartSideBar: React.FC = () => {
    const { state, dispatch } = useContext(stateContext);
    const location = useLocation();
    const [ update, setUpdate ] = useState(false);

    useEffect(() => {
        /*<!------------for toggle and expand minisidebar js------>*/
        if (state.counter === 0) {
            $('#wrapper').removeClass('toggled');
        } else {
            $('#wrapper').addClass('toggled');
        }

        /*<!--------------minisidebar open close js-------------------> */

        if (state.counter === 0) {
            $('.container1').removeClass('open');
        }

        if (location.pathname === '/cart') {
            $('#wrapper').removeClass('toggled');
            $('.container1').removeClass('open');
        }

        if (location.pathname === '/checkout') {
            $('#wrapper').removeClass('toggled');
            $('.container1').removeClass('open');
        }
        
        localStorage.setItem('itemTurkey', JSON.stringify(state.itemTurkey));
        localStorage.setItem('itemBeef', JSON.stringify(state.itemBeef));
        localStorage.setItem('itemChicken', JSON.stringify(state.itemChicken));
        localStorage.setItem('itemPork', JSON.stringify(state.itemPork));
        localStorage.setItem('itemLambGoat', JSON.stringify(state.itemLambGoat));
        localStorage.setItem('itemDeli', JSON.stringify(state.itemDeli));
        localStorage.setItem('itemHomemadeSausage',JSON.stringify(state.itemHomemadeSausage));
        localStorage.setItem('itemPantry', JSON.stringify(state.itemPantry));
        localStorage.setItem('itemFruitsVegetables',JSON.stringify(state.itemFruitsVegetables));
        localStorage.setItem('itemWholesaleBulk',JSON.stringify(state.itemWholesaleBulk));
    }, [state.counter, state.showCart,state.itemTurkey,
        state.itemBeef,
        state.itemChicken,
        state.itemPork,
        state.itemLambGoat,
        state.itemDeli,
        state.itemHomemadeSausage,
        state.itemPantry,
        state.itemFruitsVegetables,])

    const handleExpand = () => {
        $('.container1').toggleClass('open');
    };

    const handleClose = () => {
        $('#wrapper').removeClass('toggled');
        $('.container1').removeClass('open');
        dispatch({ type: 'showCart', payload: 1 });
    }
   
    const decrement = (val: any) => {
        dispatch({ type: 'DECREASE', payload: val });
    };

    const increment = (val: any) => {
        dispatch({ type: 'INCREASE', payload: val });
    };
    
    const handleRemove = (val: any) => {

        if (val.quantity === 1) {
            dispatch({ type: 'REMOVE_ITEM', payload: val })
        } else {
            val.price = val.price * val.quantity
            val.orgPrice = val.orgPrice * val.quantity
            dispatch({ type: 'REMOVE_ITEM', payload: val })
        }

        if (val.category === 'Turkey') {
            dispatch({ type: 'itemTurkeyDecrease' })
        }

        if (val.category === 'Beef') {
            dispatch({ type: 'itemBeefDecerease' })
        }

        if (val.category === 'Chicken') {
            dispatch({ type: 'itemChickenDecerease' })
        }

        if (val.category === 'Pork') {
            dispatch({ type: 'itemPorkDecerease' })
        }

        if (val.category === 'Lamb & Goat') {
            dispatch({ type: 'itemLambGoatDecerease' })
        }

        if (val.category === 'Deli') {
            dispatch({ type: 'itemDeliDecerease' })
        }

        if (val.category === 'Homemade Sausage') {
            dispatch({ type: 'itemHomemadeSausageDecerease' })
        }

        if (val.category === 'Pantry') {
            dispatch({ type: 'itemPantryDecerease' })
        }

        if (val.category === 'Fruits & Vegetables') {
            dispatch({ type: 'itemFruitsVegetablesDecerease' })
        }

        if (val.category === 'Wholesale & Bulk') {
            dispatch({ type: 'itemWholesaleBulkDecerease' })
        }
    }

    return (
        <React.Fragment>
            <div id="sidebar-wrapper">
                <div className="container1">
                    <div className="background">
                        <div
                            className="closeburger"
                            onClick={() => {
                                handleExpand()
                            }} /*onClick={()=>{setMod(true)}} onDoubleClick={()=>{setMod(false)}}*/
                        ></div>
                    </div>

                    <div className="sidebar-front">
                        <div className="row">
                            <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                                {state.counter
                                    ? state.cartItems.map((val: any) => {
                                          return (
                                              <div
                                                  className="food-images"
                                                  key={val.id}
                                              >
                                                  <img
                                                      src={val.image}
                                                      alt="Not Found"
                                                  />
                                              </div>
                                          )
                                      })
                                    : null}
                            </div>
                        </div>
                    </div>

                    <div className="main-cart">
                        <div className="cart-padding">
                            <div className="cart-title">
                                <h6>Cart</h6>
                            </div>

                            {
                                //dispatch({type:'showCart',payload:1})
                                state.counter ? (
                                    <div className="row">
                                        <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                                            <div className="estimate-order">
                                                <h6>
                                                    ${Math.trunc(state.price)}.
                                                    <span>
                                                        {
                                                            formatNumber(
                                                                state.price
                                                            )
                                                                .toString()
                                                                .split('.')[1]
                                                        }
                                                    </span>
                                                </h6>
                                                <p>Estimated order value</p>
                                            </div>
                                            <div
                                                className="proceed-button"
                                                onClick={() => handleClose()}
                                            >
                                                <NavLink exact to="/cart">
                                                    
                                                    <button className="btn btn-proceed">
                                                        Begin checkout
                                                    </button>
                                                </NavLink>
                                            </div>
                                        </div>
                                    </div>
                                ) : (
                                    <h1>Your Cart is Empty</h1>
                                )
                            }
                        </div>

                        <div className="row">
                            <div className="col-xl-12">
                                <div className="cart-food-details mb-0">
                                    <h6>
                                        Turkey
                                        <span>2</span>
                                    </h6>
                                </div>
                                <div className="cart-padding side-cart-list">
                                    {/* <!-------------food1------------------------->*/}

                                    {state.counter
                                        ? state.cartItems.map((val: any) => {
                                              return (
                                                  <>
                                                      <div
                                                          className="food-details p-0"
                                                          key={val.id}
                                                      >
                                                          <div
                                                              className="col-12 col-md-8 col-xl-2 offset-xl-2 close-btn"
                                                              onClick={() => {
                                                                  handleRemove(
                                                                      val
                                                                  )
                                                              }}
                                                          >
                                                              <h6>
                                                                  <i className="fa fa-times"></i>
                                                              </h6>
                                                          </div>
                                                          <div className="imgCart">
                                                              <img
                                                                  src={
                                                                      val.image
                                                                  }
                                                                  className="img-fluid"
                                                                  alt="food"
                                                              />
                                                          </div>
                                                          <div className="cart-inner-details">
                                                              <h6>
                                                                  {val.title}
                                                              </h6>
                                                              <p className="weight">Wt: 2lb</p>
                                                              <p className="price">
                                                                  <sup>

                                                                  </sup>
                                                                  <span>
                                                                                          ${Math.trunc(
                                                                      val.price
                                                                  )}.
                                                                                      </span>
                                                                  <sup>
                                                                      {
                                                                          val.price
                                                                              .toString()
                                                                              .split(
                                                                                  '.'
                                                                              )[1]
                                                                      }
                                                                  </sup>
                                                              </p>
                                                              <div className="row">
                                                                  <div className="col-12 p-0">
                                                                      <div className="food-details mb-sm">
                                                                          <div className="cart-table qty-button">
                                                                              <div className="product-quantity-cart m-0">
                                                                                  <div className="num-block skin-5">
                                                                                      <div className="num-in">
                                                                                          <span
                                                                                              className="minus dis decrement"
                                                                                              onClick={() => {
                                                                                                  decrement(
                                                                                                      val
                                                                                                  )
                                                                                              }}
                                                                                          >
                                                                                              <i className="fa fa-minus"></i>
                                                                                          </span>
                                                                                          <input
                                                                                              type="text"
                                                                                              className="in-num"
                                                                                              value={
                                                                                                  val.quantity
                                                                                              }
                                                                                              name="qty"
                                                                                          />
                                                                                          <span
                                                                                              className="plus increment"
                                                                                              onClick={() => {
                                                                                                  increment(
                                                                                                      val
                                                                                                  )
                                                                                              }}
                                                                                          >
                                                                                              <i className="fa fa-plus"></i>
                                                                                          </span>
                                                                                      </div>
                                                                                      {/*val.quantity ===
                                                                                        0
                                                                                          ? RemoveProduct(
                                                                                                val
                                                                                            )
                                                                                          : null*/}
                                                                                  </div>
                                                                              </div>
                                                                          </div>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <hr />
                                                  </>
                                              )
                                          })
                                        : null}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default CartSideBar;
