import React, { useContext, useState } from 'react'
import { images } from '../Constant/Constant'
import { NavLink, useHistory } from 'react-router-dom'
import { stateContext } from '../../GlobalState/Context'
import '../../assets/css/List.css'
import CartSideBar from './CartSideBar'
import AddToList from '../Home/childComponents/AddToList'
import { Dropdown, Modal } from 'react-bootstrap'
import Modals from './Modals'
import '../../assets/css/List.css'
import Auth from '../../Auth/index'
import CategoryData from '../Common/CategoryData'

const auth = new Auth()

const Header1: React.FC = () => {
    const { state, dispatch } = useContext(stateContext)
    const [update, setUpdate] = useState(false)
    const [searchText, setSearchText] = useState('')
    const [selectValue, setSelectValue] = useState('')
    const [suggest, setSuggest] = useState<any>()
    const history = useHistory()

    const updateSearch = (e: any) => {
        setSearchText(e.target.value)
        setSelectValue(e.target.value)
        let suggestion = CategoryData.filter((item: any) => {
            if (
                item.title.toLowerCase().includes(e.target.value.toLowerCase())
            ) {
                return item
            }
        })
        setSuggest(suggestion)
        dispatch({ type: 'SEARCH', payload: e.target.value })
    }

    const selected = (val: any) => {
        setSelectValue(val)
        dispatch({ type: 'SEARCH', payload: val })
        setSuggest([])
    }

    const logOut = () => {
        auth.remove('token')
        setUpdate(true)
        history.push('/login')
    }

    return (
        <React.Fragment>
            <header id="header" className="header1 bg-white"
            >
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12 mid-head-details">
                            <NavLink
                                exact
                                to="/"
                                onClick={() =>{
                                    dispatch({
                                        type: 'showCart',
                                        payload: false,
                                    });
                                    dispatch({
                                        type: 'prevPath',
                                        payload: '',
                                    })
                                }
                                }
                            >
                                <div className="logo-col">
                                    <img
                                        src={images.logoImg.default}
                                        alt="Not Found"
                                        className="img-fluid"
                                    />
                                </div>
                            </NavLink>
                            <div className="flex-col">
                                <div className="head-content">
                                    <div className="search-bar-col position-relative">
                                        <div className="search-bar">
                                            <NavLink
                                                exact
                                                to={`/${state.search}`}
                                                style={{
                                                    textDecoration:
                                                        'none',
                                                }}
                                            >
                                                <form className="input-search">
                                                    <input
                                                        className="form-control"
                                                        onChange={
                                                            updateSearch
                                                        }
                                                        type="text"
                                                        placeholder="Search"
                                                        id="example-search-input"
                                                        autoComplete="off"
                                                        value={
                                                            selectValue
                                                        }
                                                    />
                                                    <i className="fa fa-search"></i>
                                                </form>
                                            </NavLink>
                                        </div>
                                        <ul className="search-list">
                                            {searchText.length
                                                ? suggest.map(
                                                    (item: any) => {
                                                        return (
                                                            <>
                                                                <li
                                                                    key={
                                                                        item.id
                                                                    }
                                                                    onClick={() =>
                                                                        selected(
                                                                            item.title
                                                                        )
                                                                    }
                                                                    style={{
                                                                        listStyle:
                                                                            'none',
                                                                        display:
                                                                            'inline-block',
                                                                        cursor: 'pointer',
                                                                    }}
                                                                >
                                                                    {
                                                                        item.title
                                                                    }
                                                                </li>

                                                                <br />
                                                            </>
                                                        )
                                                    }
                                                )
                                                : null}
                                        </ul>
                                    </div>
                                    <div className="drop-col">
                                        <div className="pickup-selection form-group">
                                            <div className="dropdown">
                                                <button
                                                    className="btn dropdown-toggle"
                                                    type="button"
                                                    data-toggle="modal"
                                                    onClick={() => {
                                                        dispatch({
                                                            type: 'MODAL',
                                                            payload:
                                                                true,
                                                        })
                                                    }}
                                                    data-target="#exampleModal"
                                                    aria-haspopup="true"
                                                    aria-expanded="false"
                                                >
                                                    <img
                                                        src={
                                                            images
                                                                .homeImg
                                                                .default
                                                        }
                                                        alt="Not Found"
                                                    />
                                                    <span>
                                                                Pickup from -{' '}
                                                            </span>
                                                    <span className="pickup">
                                                                {' '}
                                                        Farmer's
                                                                Fresh Meat on{' '}
                                                        {
                                                            state.defaultPickUp
                                                        }
                                                            </span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="menu-col top-header">
                                        <ul className="list-group list-group-horizontal content-right">
                                            <li className="menu-toggle">
                                                <img
                                                    src={
                                                        images.cartIcon
                                                            .default
                                                    }
                                                    alt="Not Found"
                                                />
                                                <a href="#">
                                                    <b>Cart</b>
                                                </a>
                                                {state.counter ? (
                                                    <span className="badge badge-light ">
                                                                {state.counter}
                                                            </span>
                                                ) : (
                                                    <span />
                                                )}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <div className="header-nav">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12 col-md-8 col-lg-12 col-xl-12 p-0">
                            <nav className="navbar navbar-expand-lg">
                                <button
                                    className="navbar-toggler"
                                    type="button"
                                    data-toggle="collapse"
                                    data-target="#navbarNav"
                                    aria-controls="navbarNav"
                                    aria-expanded="false"
                                    aria-label="Toggle navigation"
                                >
                                    <span className="fa fa-bars"></span>
                                </button>
                                <div
                                    className="collapse navbar-collapse"
                                    id="navbarNav"
                                >
                                    <ul className="navbar-nav menu-details">
                                        <li className="nav-item">
                                            <a className="nav-link" href="#">
                                                Shop
                                            </a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link" href="#">
                                                Farmer’s fresh kitchen
                                            </a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link" href="#">
                                                Farmer’s fresh catering
                                            </a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link" href="#">
                                                Location & hours
                                            </a>
                                        </li>
                                    </ul>
                                    <ul className="navbar-nav menu-details ml-auto">
                                        {auth.getValue('token') ? (
                                            <li>
                                                <Dropdown>
                                                    <Dropdown.Toggle
                                                        variant=""
                                                        id="dropdown-basic"
                                                    >
                                                        Hi, Abdallah
                                                    </Dropdown.Toggle>

                                                    <Dropdown.Menu>
                                                        <Dropdown.Item>
                                                            <NavLink
                                                                exact
                                                                to="/PersonalInformation"
                                                            >
                                                                Personal
                                                                Information
                                                            </NavLink>
                                                        </Dropdown.Item>
                                                        <Dropdown.Item
                                                            onClick={() =>
                                                                logOut()
                                                            }
                                                        >
                                                            Log Out
                                                        </Dropdown.Item>
                                                    </Dropdown.Menu>
                                                </Dropdown>
                                            </li>
                                        ) : (
                                            <li>
                                                {/*<img*/}
                                                {/*    src={*/}
                                                {/*        images*/}
                                                {/*            .userIcon*/}
                                                {/*            .default*/}
                                                {/*    }*/}
                                                {/*    alt="Not Found"*/}
                                                {/*/>*/}
                                                <NavLink
                                                    exact
                                                    to="/"
                                                >
                                                    <b>Login</b>
                                                </NavLink>{' '}
                                                or
                                                <NavLink
                                                    exact
                                                    to="/"
                                                >
                                                    <b>SignUp</b>
                                                </NavLink>
                                            </li>
                                        )}
                                        <li className="nav-item">
                                            <a className="nav-link" href="#">
                                                Lists
                                            </a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link" href="#">
                                                Help
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <CartSideBar />
            {/* <SearchItem/>  */}

            {state.list === 0 ? null : (
                <Modal show={state.list} className="modalContent">
                    <Modal.Body>
                        <AddToList />
                    </Modal.Body>
                </Modal>
            )}

            {/*<Modals/>*/}
            <Modal show={state.modalShow}>
                <Modal.Body>
                    <Modals />
                </Modal.Body>
            </Modal>
        </React.Fragment>
    )
}

export default Header1
