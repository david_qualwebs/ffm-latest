import React from 'react'
import { useLocation } from 'react-router-dom'
import Header from '../Header/Header'
import Footer from '../Footer/Footer'
import { excludeHeader, excludeFooter } from '../Constant/Constant'
import { Helmet } from 'react-helmet'

const Layout: React.FC = (props) => {
    const location = useLocation()
 
    return (
        <React.Fragment>
            <Helmet>
                <title>
                    Farmer&#039;s Fresh Meat - Voted Best Meat Market/Butcher
                    Shop in Houston
                </title>
                <meta
                    name="description"
                    content="Farmer&#039;s Fresh Meat is proud to be the best butcher 
                 shop and meat market in Houston, Texas. Buy wholesale beef, goat meat, chicken, and more - on sale now!"
                />
            </Helmet>

            <div id="wrapper" className="">
                {excludeHeader.find(
                    (item) => location.pathname === item
                ) ? null : (
                    <Header />
                )}

                <div className="content-padding" id="page-content-wrapper">
                    {props.children}

                    {excludeFooter.find(
                        (item) => location.pathname === item
                    ) ? null : (
                        <Footer />
                    )}
                </div>
            </div>
        </React.Fragment>
    )
}

export default Layout
