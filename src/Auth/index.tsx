export default class Auth {
    getValue(key: string) {
        const data = localStorage.getItem(key)
        if (data) return JSON.parse(data)
    }

    setValue(key: string, value: object) {
        return localStorage.setItem(key, JSON.stringify(value))
    }

    remove(key: string) {
        localStorage.removeItem(key)
    }

    hasValue(key: string) {
        return localStorage.getItem(key) !== null
    }
}
