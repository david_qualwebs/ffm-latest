import React, { useContext } from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import Home from '../Components/Home/Home'
import { stateContext } from '../GlobalState/Context'
import Auth from '../Auth/index'
import SearchItem from '../Components/searchComponents/SearchItem'
import Cart from '../Components/Checkout/Cart'
import Checkout from '../Components/Checkout/childComponents/Checkout'
import SingleProduct from '../Components/SingleProduct'
import Login from '../Components/Login/Login'
import PersonalInformation from '../Components/Login/AfterLoginComponents/PersonalInformation'
import CommunicationPreferences from '../Components/Login/AfterLoginComponents/CommunicationPreferences'
import SecuritySettings from '../Components/Login/AfterLoginComponents/SecuritySettings'
import AddressDetails from '../Components/Login/AfterLoginComponents/AddressDetails'
import PaymentDetails from '../Components/Login/AfterLoginComponents/PaymentDetails'
import PickUpOrders from '../Components/Login/AfterLoginComponents/PickUpOrders'
import DeliveryOrders from '../Components/Login/AfterLoginComponents/DeliveryOrders'
import DigitalCoupons from '../Components/Login/AfterLoginComponents/DigitalCoupons'
import ItemList from '../Components/Login/AfterLoginComponents/ItemList'
import TaxExemption from '../Components/Login/AfterLoginComponents/TaxExemption'
import SignUp from '../Components/SignUp/SignUp'
import AllProduct from '../Components/Common/AllProduct'
import CategoryPage from '../Components/CategoryPage/CategoryPage'

const auth = new Auth()

const PrivateRoute = ({ component: Component, ...rest }: any) => {
    const authenticated = auth.hasValue('token')

    return (
        <Route
            {...rest}
            render={(props) =>
                authenticated === true ? (
                    <Component {...props} />
                ) : (
                    <Redirect to="/login" />
                )
            }
        />
    )
}

const Routes: React.FC = () => {
    const { state } = useContext(stateContext)

    return (
        <React.Fragment>
            <Switch>
                <Route exact path="/" component={Home} />
                {state.search ? (
                    <Route
                        exact
                        path={`/${state.search}`}
                        component={SearchItem}
                    />
                ) : null}

                <Route exact path="/cart" component={Cart} />
                <Route exact path="/checkout" component={Checkout} />
                <Route
                    exact
                    path={`/singleproduct/${state.link}`}
                    component={SingleProduct}
                />
                <Route exact path="/Beef" component={CategoryPage} />
                <Route exact path="/Pork" component={CategoryPage} />
                <Route exact path="/Turkey" component={CategoryPage} />
                <Route exact path="/Deli" component={CategoryPage} />
                <Route exact path="/Chicken" component={CategoryPage} />
                <Route exact path="/WholeSale & Bulk" component={CategoryPage} />
                <Route exact path="/Homemade Sausage" component={CategoryPage} />
                <Route exact path="/Lamb & Goat" component={CategoryPage} />
                <Route exact path="/Pantry" component={CategoryPage} />
                <Route
                    exact
                    path="/Fruits & Vegetables"
                    component={CategoryPage}
                />
                <Route exact path="/login" component={Login} />
                <Route exact path="/signUp" component={SignUp} />
                <Route exact path="/AllProduct" component={AllProduct} />
                <PrivateRoute
                    exact
                    path="/PersonalInformation"
                    component={PersonalInformation}
                />
                <PrivateRoute
                    exact
                    path="/CommunicationPrefrences"
                    component={CommunicationPreferences}
                />
                <PrivateRoute
                    exact
                    path="/SecuritySetting"
                    component={SecuritySettings}
                />
                <PrivateRoute
                    exact
                    path="/AddressDetails"
                    component={AddressDetails}
                />
                <PrivateRoute
                    exact
                    path="/PaymentDetails"
                    component={PaymentDetails}
                />
                <PrivateRoute
                    exact
                    path="/PickUpOrder"
                    component={PickUpOrders}
                />
                <PrivateRoute
                    exact
                    path="/DeliveryOrders"
                    component={DeliveryOrders}
                />
                <PrivateRoute
                    exact
                    path="/DigitalCoupons"
                    component={DigitalCoupons}
                />
                <PrivateRoute exact path="/ItemList" component={ItemList} />
                <PrivateRoute
                    exact
                    path="/TaxExemption"
                    component={TaxExemption}
                />
                <Redirect to="/" />
            </Switch>
        </React.Fragment>
    )
}

export default Routes
