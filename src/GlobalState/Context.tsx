import React, { createContext, useReducer, useState } from 'react'
import reducer, { IState } from '../Reducer/Reducer'
import CategoryData from '../Components/Common/CategoryData';

import {
    getLocalData,
    getPrice,
    getCount,
    getSingleProductLink,
    getSingleProductData,
    getItemTurkey,
    getModalShow,
    getDefaultPickUp,
    getDeliveryDistance,
    getSelectedTime,
    getSelectedDate,
    getShowTime,
    getItemBeef,
    getItemChicken,
    getItemPork,
    getItemLambGoat,
    getItemDeli,
    getItemHomemadeSausage,
    getItemPantry,
    getItemFruitsVegetables,
    getItemWholesaleBulk,
    getProductCategory,
} from './Storage'
 
const initialState = {
    cartItems: getLocalData(),
    listItems: [],
    singleProduct: getSingleProductData(),
    price: getPrice(),
    orgPrice: 0,
    counter: getCount(),
    search: '',
    list: 0,
    isEmpty: 0,
    modalShow: false,
    isAdded: 0,
    link: getSingleProductLink(),
    isFooter: true,
    //itemMeat:getItemMeat(),
    itemTurkey: getItemTurkey(),
    itemBeef: getItemBeef(),
    itemChicken: getItemChicken(),
    itemPork: getItemPork(),
    itemLambGoat: getItemLambGoat(),
    itemDeli: getItemDeli(),
    itemHomemadeSausage: getItemHomemadeSausage(),
    itemPantry: getItemPantry(),
    itemFruitsVegetables: getItemFruitsVegetables(),
    itemWholesaleBulk: getItemWholesaleBulk(),
    showTime: getShowTime(),
    //deliveryPrice:getDeliveryPrice(),
    pickUpAddress: '',
    selectedDate: getSelectedDate(),
    selectedTime: getSelectedTime(),
    defaultPickUp: getDefaultPickUp(),
    modShow: getModalShow(),
    miniCart: '',
    statetime: false,
    showCart: 0,
    category: getProductCategory(),
    deliveryDistance: getDeliveryDistance(),
    prevPath: '',
    showValue:0
}

interface IIState {
    storeItem: any
    setStoreItem: Function
}

/*interface IIDataState {
    data: any
    setData: Function
}*/

const itemContext = createContext<IIState>({
    storeItem: 0,
    setStoreItem: () => {},
});

/*const dataContext = createContext<IIDataState>({
    data: 0,
    setData: () => {},
});*/

const stateContext = createContext<{ state: IState; dispatch: Function } | any>(
    {
        state: initialState,
        dispatch: () => 0,
    }
)

const Context: React.FC = (props) => {
    const [state, dispatch] = useReducer<any>(reducer, initialState);
    const [storeItem, setStoreItem] = useState<any>(CategoryData);
    //const [data, setData] = useState<any>(CategoryData);

    return (
        <React.Fragment>
            <stateContext.Provider value={{ state, dispatch }}>
                <itemContext.Provider value={{ storeItem, setStoreItem}}>
                    {props.children}
                </itemContext.Provider>
            </stateContext.Provider>
        </React.Fragment>
    )
}

export default Context
export { stateContext, itemContext }
