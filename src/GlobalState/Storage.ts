export const getLocalData = () => {
    let data = localStorage.getItem('items')
    if (data) {
        return JSON.parse(data)
    } else {
        return []
    }
}

export const getPrice = () => {
    let data = localStorage.getItem('price')
    if (data) {
        return JSON.parse(data)
    } else {
        return 0
    }
}

export const getCount = () => {
    let data = localStorage.getItem('counter')
    if (data) {
        return JSON.parse(data)
    } else {
        return 0
    }
}

export const getSingleProductLink = () => {
    let data = localStorage.getItem('Link')
    if (data) {
        return JSON.parse(data)
    } else {
        return ''
    }
}

export const getSingleProductData = () => {
    let data = localStorage.getItem('singleProduct')
    if (data) {
        return JSON.parse(data)
    } else {
        return []
    }
}

export const getModalShow = () => {
    let data = localStorage.getItem('Modal')
    if (data) {
        return JSON.parse(data)
    } else {
        return 0
    }
}

export const getDefaultPickUp = () => {
    let data = localStorage.getItem('PickUp')
    if (data) {
        return JSON.parse(data)
    } else {
        return ''
    }
}

export const getDeliveryDistance = () => {
    let data = localStorage.getItem('deliveryDistance')
    if (data) {
        return JSON.parse(data)
    } else {
        return 0
    }
}

/*export const getItemMeat = () =>{
    let data = localStorage.getItem('itemMeat');
    if(data){
        return JSON.parse(data);
    }
    else{
        return 0;
    }
};*/

export const getItemTurkey = () => {
    let data = localStorage.getItem('itemTurkey')
    if (data) {
        return JSON.parse(data)
    } else {
        return 0
    }
}

export const getItemBeef = () => {
    let data = localStorage.getItem('itemTurkey')
    if (data) {
        return JSON.parse(data)
    } else {
        return 0
    }
}

export const getItemChicken = () => {
    let data = localStorage.getItem('itemChicken')
    if (data) {
        return JSON.parse(data)
    } else {
        return 0
    }
}

export const getItemPork = () => {
    let data = localStorage.getItem('ItemPork')
    if (data) {
        return JSON.parse(data)
    } else {
        return 0
    }
}

export const getItemLambGoat = () => {
    let data = localStorage.getItem('itemLambGoat')
    if (data) {
        return JSON.parse(data)
    } else {
        return 0
    }
}

export const getItemDeli = () => {
    let data = localStorage.getItem('itemDeli')
    if (data) {
        return JSON.parse(data)
    } else {
        return 0
    }
}

export const getItemHomemadeSausage = () => {
    let data = localStorage.getItem('itemHomemadeSausage')
    if (data) {
        return JSON.parse(data)
    } else {
        return 0
    }
}

export const getItemPantry = () => {
    let data = localStorage.getItem('itemPantry')
    if (data) {
        return JSON.parse(data)
    } else {
        return 0
    }
}

export const getItemFruitsVegetables = () => {
    let data = localStorage.getItem('itemFruitsVegetables')
    if (data) {
        return JSON.parse(data)
    } else {
        return 0
    }
}

export const getItemWholesaleBulk = () => {
    let data = localStorage.getItem('itemWholesaleBulkss')
    if (data) {
        return JSON.parse(data)
    } else {
        return 0
    }
}

export const getSelectedTime = () => {
    let data = localStorage.getItem('PickUpTime')
    if (data) {
        return JSON.parse(data)
    } else {
        return ''
    }
}

export const getSelectedDate = () => {
    let data = localStorage.getItem('PickUpDate')
    if (data) {
        return JSON.parse(data)
    } else {
        return ''
    }
}

export const getShowTime = () => {
    let data = localStorage.getItem('showTime')
    if (data) {
        return JSON.parse(data)
    } else {
        return false
    }
}

export const getTimeId = () => {
    let data = localStorage.getItem('timeId')
    if (data) {
        return JSON.parse(data)
    } else {
        return ''
    }
}

export const getProductCategory = () => {
    let data = localStorage.getItem('category')
    if (data) {
        return JSON.parse(data)
    } else {
        return ''
    }
}
