import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import Layout from './Components/Layout/Layout';
import Routes from './Routes/Routes';
import Context from './GlobalState/Context';
import './assets/css/home.css';
import './assets/css/home-media.css';

function App() {
    return (
        <React.Fragment>
            <Context>
                <BrowserRouter>
                    <Layout>
                        <Routes />
                    </Layout>
                </BrowserRouter>
            </Context>
        </React.Fragment>
    );
};

export default App;
