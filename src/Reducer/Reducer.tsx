export interface ICart_type {
    price: number
    orgPrice: number
    id: number
    image: string
    quantity: number
}

export interface ISingleProduct {
    price: number
    id: number
    image: string
}

export interface IStorage {
    id: number
}

export interface IState {
    cartItems: Array<ICart_type>
    listItems: Array<ICart_type>
    singleProduct: Array<ISingleProduct>
    storeId: Array<IStorage>
    price: number
    counter: number
    orgPrice: number
    search: string
    list: number
    isEmpty: number
    modalShow: boolean
    isAdded: number
    link: string
    isFooter: boolean
    //itemMeat:number
    itemBeef: number
    itemTurkey: number
    itemChicken: number
    itemPork: number
    itemLambGoat: number
    itemDeli: number
    itemHomemadeSausage: number
    itemPantry: number
    itemFruitsVegetables: number
    itemWholesaleBulk: number
    showTime: boolean
    deliveryPrice: number
    pickUpAddress: string
    selectedDate: string
    selectedTime: string
    defaultPickUp: string
    defaultDistance: number
    modShow: number
    miniCart: string
    statetime: boolean
    showCart: number
    category: string
    deliveryDistance: number
    prevPath: string
    showValue:number
}

const reducer = (
    state: IState,
    action: {
        type: string
        payload: number | string | object | Array<ICart_type> | any
    }
) => {
    const { type, payload } = action

    switch (type) {
        case 'ADD_ITEM':
            if (!state.cartItems.find((item) => item.id === payload.id)) {
                state.cartItems.push({
                    ...payload,
                    quantity: 1,
                    price: payload.price,
                    orgPrice: payload.orgPrice,
                })
            }

            return {
                ...state,
                cartItems: [...state.cartItems],
                //  storeId:[...state.storeId],
                counter: state.counter + 1,
                price: state.price + payload.price,
                orgPrice: state.orgPrice + payload.orgPrice,
            }

        case 'REMOVE_ITEM':
            return {
                ...state,
                cartItems: [
                    ...state.cartItems.filter(
                        (item) => item.id !== action.payload.id
                    ),
                ],
                counter:
                    state.counter -
                    state.cartItems[
                        state.cartItems.findIndex(
                            (item) => item.id === payload.id
                        )
                    ].quantity,
                price: state.price - payload.price,
                orgPrice: state.orgPrice - payload.orgPrice,
            }

        case 'REMOVE_STATE':
            return {
                ...state,
                cartItems: [
                    ...state.cartItems.filter(
                        (item) => item.id !== action.payload.id
                    ),
                ],
            }

        case 'INCREASE':
            state.cartItems[
                state.cartItems.findIndex((item) => item.id === payload.id)
            ].quantity++

            return {
                ...state,
                cartItems: [...state.cartItems],
                counter: state.counter + 1,
                price: state.price + payload.price,
                orgPrice: state.orgPrice + payload.orgPrice,
            }

        case 'DECREASE':
            state.cartItems[
                state.cartItems.findIndex((item) => item.id === payload.id)
            ].quantity--

            return {
                ...state,
                cartItems: [...state.cartItems],
                counter: state.counter - 1,
                price: state.price - payload.price,
                orgPrice: state.orgPrice - payload.orgPrice,
            }

        case 'SEARCH':
            return {
                ...state,
                search: payload,
            }

        case 'ADD_TO_LIST':
            if (!state.listItems.find((item) => item.id === payload.id)) {
                state.listItems.push({
                    ...payload,
                    quantity: 1,
                })
            }

            return {
                ...state,
                listItems: [...state.listItems],
            }

        case 'REMOVE_LIST':
            return {
                ...state,
                listItems: [
                    ...state.listItems.filter(
                        (item) => item.id !== action.payload.id
                    ),
                ],
            }

        case 'listOpen':
            return {
                ...state,
                list: payload,
            }
        case 'isEmpty':
            return {
                ...state,
                isEmpty: payload,
            }

        case 'MODAL':
            return {
                ...state,
                modalShow: payload,
            }

        case 'SINGLE_PRODUCT':
            return {
                ...state,
                singleProduct: [payload],
            }

        case 'isAdded':
            return {
                ...state,
                isAdded: payload,
            }
        case 'link':
            return {
                ...state,
                link: payload,
            }
        case 'show_Footer':
            return {
                ...state,
                isFooter: payload,
            }
        /*case "itemMeat":
            return{
                    ...state,
                    itemMeat:state.itemMeat+1
           }
        
        case "itemMeatDecerease":
            return{
                ...state,
                itemMeat:state.itemMeat - 1
            }*/
        case 'itemTurkey':
            return {
                ...state,
                itemTurkey: state.itemTurkey + 1,
            }

        case 'itemTurkeyDecrease':
            return {
                ...state,
                itemTurkey: state.itemTurkey - 1,
            }

        case 'itemBeef':
            return {
                ...state,
                itemBeef: state.itemBeef + 1,
            }

        case 'itemBeefDecerease':
            return {
                ...state,
                itemBeef: state.itemBeef - 1,
            }

        case 'itemChicken':
            return {
                ...state,
                itemChicken: state.itemChicken + 1,
            }

        case 'itemChickenDecerease':
            return {
                ...state,
                itemChicken: state.itemChicken - 1,
            }
        //
        case 'itemPork':
            return {
                ...state,
                itemPork: state.itemPork + 1,
            }

        case 'itemPorkDecerease':
            return {
                ...state,
                itemPork: state.itemPork - 1,
            }

        case 'itemLambGoat':
            return {
                ...state,
                itemLambGoat: state.itemLambGoat + 1,
            }

        case 'itemLambGoatDecerease':
            return {
                ...state,
                itemLambGoat: state.itemLambGoat - 1,
            }

        case 'itemDeli':
            return {
                ...state,
                itemDeli: state.itemDeli + 1,
            }

        case 'itemDeliDecerease':
            return {
                ...state,
                itemDeli: state.itemDeli - 1,
            }

        case 'itemHomemadeSausage':
            return {
                ...state,
                itemHomemadeSausage: state.itemHomemadeSausage + 1,
            }

        case 'itemHomemadeSausageDecerease':
            return {
                ...state,
                itemHomemadeSausage: state.itemHomemadeSausage - 1,
            }

        case 'itemPantry':
            return {
                ...state,
                itemPantry: state.itemPantry + 1,
            }

        case 'itemPantryDecerease':
            return {
                ...state,
                itemPantry: state.itemPantry - 1,
            }

        case 'itemFruitsVegetables':
            return {
                ...state,
                itemFruitsVegetables: state.itemFruitsVegetables + 1,
            }

        case 'itemFruitsVegetablesDecerease':
            return {
                ...state,
                itemFruitsVegetables: state.itemFruitsVegetables - 1,
            }

        case 'itemWholesaleBulk':
            return {
                ...state,
                itemWholesaleBulk: state.itemWholesaleBulk + 1,
            }

        case 'itemWholesaleBulkDecerease':
            return {
                ...state,
                itemWholesaleBulk: state.itemWholesaleBulk - 1,
            }

        case 'CLEAR_CART':
            return {
                ...state,
                cartItems: [
                    ...state.cartItems.filter(
                        (item) => item.id !== action.payload.id
                    ),
                ],
                counter: 0,
                price: 0,
            }
        case 'show_Time':
            return {
                ...state,
                showTime: payload,
            }

        /*case "delivery_Price":
            return {
                ...state,
                deliveryPrice: payload * 5
            };*/

        case 'pickUp_Address':
            return {
                ...state,
                pickUpAddress: payload,
            }

        case 'selected_Date':
            return {
                ...state,
                selectedDate: payload,
            }

        case 'selected_Time':
            return {
                ...state,
                selectedTime: payload,
            }

        /*case "onRefresh":
            return{
                ...state,
                cartItems:[...state.cartItems,payload],
            }*/
        case 'defaultPickUp':
            return {
                ...state,
                defaultPickUp: payload,
            }

        case 'deliveryDistance':
            return {
                ...state,
                deliveryDistance: payload,
            }

        case 'modShow':
            return {
                ...state,
                modShow: state.modShow + 1,
            }

        case 'miniCart':
            return {
                ...state,
                miniCart: payload,
            }

        case 'statetime':
            return {
                ...state,
                statetime: payload,
            }

        case 'showCart':
            return {
                ...state,
                showCart: payload,
            }

        case 'category':
            return {
                ...state,
                category: payload,
            }
        case 'prevPath':
            return {
                ...state,
                prevPath: payload,
            }
        case 'showValue':
            return {
                ...state,
                showValue: payload,
            }    
        default:
            return state
    }
}

export default reducer
