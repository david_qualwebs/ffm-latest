import axios from 'axios'

const baseURL = `https://maps.googleapis.com/maps/api/`

const headers = () => {
    const headers: any = {
        'Content-Type': 'application/json',
    }
    return headers
}

const request: Function = (method: string, path: string) => {
    const url = `${baseURL}${path}`
    const options: object = { method, url, headers: headers() }

    return axios(options)
}

export default class API {
    getLocation(address: any) {
        return request(
            'GET',
            `geocode/json?address=${address}&key=AIzaSyCsRpdZNGNZ2jY-_YNVKx5A8uY_b1Y4pFw`
        )
    }
}
