import axios from 'axios';
import Auth from '../Auth/index';

const baseURL = 'https://fcadmin77.falafelcorner.us/api/';

const headers = () => {
    const headers:any = {
        'Content-Type': 'application/json',
    }

    const auth = new Auth();
    const token = auth.getValue('auth_token');

    if (token) {
        headers['Authorization'] = `Bearer ${token}`
    }

    return headers;
}

const request = async (method: string, path: string, body?: any) => {
    const url = `${baseURL}${path}`;
    const options:any = { method, url, headers: headers() }
    if (body) {
        options.data = body;
    }
    return await axios(options);
}

export default class API {
    login(inputData: object) {
        return request('POST', 'login', inputData);
    };
  
    register(inputData:object){
        return request('POST','register',inputData);
    }; 
}
